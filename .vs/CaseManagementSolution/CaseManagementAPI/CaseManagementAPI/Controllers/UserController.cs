﻿using AutoMapper;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CaseManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IUserRepo _userRepo;
        private readonly IMapper _mapper;
        private readonly IAuthRepo _authRepo;
        private readonly ILogger<UserController> _logger;
        private readonly IRepo _repo;

        //private readonly IHttpContextAccessor _contextAccessor;
        public UserController(IRepo repo, IAuthRepo authRepo, IUserRepo userRepo, IMapper mapper, ILogger<UserController> logger)
        {
            _userRepo = userRepo;
            _mapper = mapper;
            _authRepo = authRepo;
            this._logger = logger;
            _repo = repo;
        }
      
        [HttpGet]
        //[Route("[action]")]
        public IActionResult GetUserRole()
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole == 0)
                {
                    //ModelState.AddModelError("", $"User not found. No Role Assigned! You cant access this portal");
                    //return StatusCode(400, ModelState);
                    // return new JsonResult(new { status = StatusCodes.Status400BadRequest, success = false, message = $"User not found. No Role Assigned! You cant access this portal" });
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"User not found. No Role Assigned! You cant access this portal" });
                    //return BadRequest("User not fouNo UserRole Assigned! You cant access this portal");
                }
                UserRoleDTO userRoleDTO = new UserRoleDTO();
                userRoleDTO.UserRole = userDetails.UserRole.ToString();

               
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful Userrole retrieval", output = userRoleDTO });

               // return Ok(userRoleDTO);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while getting user Role");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }

        }
        
        //create get user details
        [HttpPost]
       // [Route("[action]")]
        public async Task<IActionResult> CreateUserProfile([FromBody] UserProfileDTO userDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                //var currentUserFullName = _userRepo.GetCurrentUserFullName(currentUserDetails);

                //var loggedInUserName = name.Where(x => x.Name.Contains(".com"));

                // var user = UserManager.FindByNameAsync(name);
                //This gets the current person that is logged into the windows system
                //string opl = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                if (userDetails.UserRole == Role.Affiliate || userDetails.UserRole == Role.Regional || userDetails.UserRole == Role.Unknown)
                {
                    //ModelState.AddModelError("", $"Access Denied! Your role isnt permitted to create a user.");
                    //return StatusCode(400, ModelState);
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"Access Denied! Your role isnt permitted to create a user." });
                    // return new JsonResult(new { status = StatusCodes.Status400BadRequest, success = false, message = $"Access Denied! Your role isnt permitted to create a user." });

                    //return BadRequest(ModelState);
                }
                else if (userDTO == null)
                {
                   // ModelState.AddModelError("", $"Access Denied! Your role isnt permitted to create a user.");
                    //return StatusCode(400, ModelState);
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"Access Denied! Your role isnt permitted to create a user." });

                    //return BadRequest(ModelState);
                }
                else if (_userRepo.UserExists(userDTO.EmailAddress))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "User Already Exists!" });

                   // ModelState.AddModelError("", "User Already Exists!");
                    //return StatusCode(404, ModelState);
                }
                var userObj = _mapper.Map<UserProfile>(userDTO);

                //userObj.UserId = userId;
                userObj.UserName = userDTO.EmailAddress.Split('@')[0];
                userObj.AddedBy = userDetails.FullName;
                userObj.AddedBy_Email = userDetails.EmailAddress;
                userObj.DateAdded = DateTime.Now.Date;
                //userObj.IsDeleted = false;


                if (!_userRepo.CreateUser(userObj))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when saving the record {userObj.EmailAddress}" });

                   /// ModelState.AddModelError("", $"Something went wrong when saving the record {userObj.EmailAddress}");
                    //return StatusCode(500, ModelState);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_USER, $"Created USER:  {userDetails.EmailAddress}");
                return StatusCode(StatusCodes.Status201Created, new { success = true, message = $"User successfully created" });

                //return StatusCode(StatusCodes.Status201Created);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while creating user Role");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }
        }


        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole == Role.Affiliate || userDetails.UserRole == Role.Regional || userDetails.UserRole == Role.Unknown)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to Users Module" });
                }
                var objList = _userRepo.GetAllUsers();
                if (objList.Count == 0)
                {
                    return Ok(objList);
                }
                var objDto = new List<GetAll_UserProfileDTO>();
                foreach (var obj in objList)
                {
                    var dtx = _mapper.Map<GetAll_UserProfileDTO>(obj);
                    objDto.Add(dtx);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_USERS, $"Retrieved users");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful User Retrieval...", output = objDto });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while getting users");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateUserProfile(int id, [FromBody] UserProfileDTO userProfileDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole == Role.Affiliate || userDetails.UserRole == Role.Regional || userDetails.UserRole == Role.Unknown)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to Users Module" });

                    //ModelState.AddModelError("", "You are not authorized to Users Module");
                    //return StatusCode(403, ModelState);
                }
                var entity = _userRepo.GetUserById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"No record found against the id {id}" });

                    //ModelState.AddModelError("", $"No record found against the id {id}.");
                    //return StatusCode(404, ModelState);
                    //return new JsonResult(new { status = StatusCodes.Status404NotFound, success = false, message = $"No record found against the id {id}." });

                }
                var previousName = entity.EmailAddress;
                if (!_userRepo.UpdateUserProfile(userDetails, entity, userProfileDTO))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when updating the note for {entity.Id}" });

                    //ModelState.AddModelError("", $"Something went wrong when updating the note for {entity.Id}");
                    //return StatusCode(500, ModelState);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_USER, $"Updated user {previousName}");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Updated Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while updating user profile");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }


        }

        [HttpPost("{Id}")]
        public async Task<IActionResult> DeleteUser(int Id)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole == Role.Affiliate || userDetails.UserRole == Role.Regional || userDetails.UserRole == Role.Unknown)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to Users Module" });

                    //ModelState.AddModelError("", "You are not authorized to Users Module");
                    //return StatusCode(403, ModelState);
                }
                if (!_userRepo.UserExists(Id))
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"No record found against the id {Id}." });

                    //ModelState.AddModelError("", $"No record found against the id {Id}.");
                    //return StatusCode(404, ModelState);
                    //return new JsonResult(new { status = StatusCodes.Status404NotFound, success = false, message = $"No record found against the id {Id}." });
                    //return NotFound();
                }

                var userObj = _userRepo.GetUserById(Id);
                if (!_userRepo.DeleteUser(userObj))
                {
                    ModelState.AddModelError("", $"Something went wrong when deleting the record {userObj.EmailAddress}");
                    return StatusCode(500, ModelState);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.DELETED_USER, $"Deleted user {userObj.EmailAddress}");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"User Successfully deleted" });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while deleting user with id- {Id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }
        }

        [HttpGet("{userName}")]
        public async Task<IActionResult> GetUsersForReassignment(string userName)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userName == null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"Usename is null" });
                }
                else if (userDetails.UserRole == Role.UserManager)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to reassign a case" });
                }
                var users = _userRepo.GetUsersToReassign(userDetails, userName);
                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_USERS_FORREASSIGNMENT, $"Retrieved users for reassignment");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful Retrieval of Users...", output = users });

               // return Ok(users);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while getting users");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

    }
}
