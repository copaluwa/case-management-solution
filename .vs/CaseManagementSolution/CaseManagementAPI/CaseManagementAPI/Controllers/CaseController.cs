﻿using AutoMapper;
using CaseManagementAPI.Data;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
//using System.Diagnostics;
//using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CaseManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    //[Authorize(Roles = "Affiliate,Region,Group")]
    [Authorize]
    public class CaseController : ControllerBase
    {
        private readonly ICaseRepo _caseRepo;
        private readonly IMapper _mapper;
        private readonly IUserRepo _userRepo;
        private readonly IAuthRepo _authRepo;
        private readonly IRepo _repo;
        private readonly ILogger<CaseController> _logger;
        private readonly ApplicationDbContext _db;
        private readonly IConfiguration _config;
        private readonly IWebHostEnvironment _webEnvironment;

        public CaseController(
            IRepo repo, ICaseRepo caseRepo,
            IMapper mapper, IUserRepo userRepo,
            IAuthRepo authRepo,
            ILogger<CaseController> logger,
            ApplicationDbContext db,
            IConfiguration config,
            IWebHostEnvironment webHostEnvironment
        )
        {
            _caseRepo = caseRepo;
            _mapper = mapper;
            _userRepo = userRepo;
            _authRepo = authRepo;
            _logger = logger;
            _repo = repo;
            _db = db;
            _config = config;
            _webEnvironment = webHostEnvironment;
        }

        [HttpPost]
        public async Task<IActionResult> CreateCase([FromForm] CreateCaseDTO caseDto)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (caseDto == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "You are not authorized to create a case" }); ;
                }
                if (_caseRepo.CaseExists(caseDto.WhistleblowerNo))
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "Case Already Exists!" });
                }
                else if (caseDto.NetLoss < 0)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "NetLoss cannot be a negative value." });
                }
                else if (caseDto.UsdExchangeRate <= 0)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "UsdExchangeRate must be greater than 0." });
                }
                else if (caseDto.GrossOnSlaught < caseDto.AmountRecovered)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "Recovered Amount cannot be greater than the Gross Onslaught" });
                }
                else if (caseDto.DateOfIncident > caseDto.DateReported)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "Incident Date cannot be greater than the Date the Incident was reported" });
                }

                if (caseDto.Files?.Any() == true && !_caseRepo.ValidateCaseFiles(caseDto.Files))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "One or more case files are invalid" });
                }

                var executionStrategy = _db.Database.CreateExecutionStrategy();
                var result = await executionStrategy.ExecuteAsync(async () =>
                {
                    using (var transaction = await _db.Database.BeginTransactionAsync())
                    {
                        try
                        {
                            var caseObj = _mapper.Map<Case>(caseDto);
                            var caseNo = _caseRepo.GetCaseNumber(caseObj.Region);
                            caseObj.CaseNo = caseNo;
                            //caseObj.NetLoss = caseDto.GrossOnSlaught - caseDto.AmountRecovered;
                            caseObj.DateAdded = DateTime.Now;
                            caseObj.AddedBy_Affiliate = userDetails.Affiliate;
                            caseObj.AddedBy_Email = userDetails.EmailAddress;
                            caseObj.AddedBy_Name = userDetails.FullName;
                            caseObj.AddedBy_Region = userDetails.Region;
                            caseObj.AddedBy_Role = userDetails.UserRole;
                            caseObj.UserId = userDetails.UserId;
                            caseObj.AssignedTo_Email = userDetails.EmailAddress;
                            caseObj.AssignedTo_Affiliate = userDetails.Affiliate;
                            caseObj.AssignedTo_Name = userDetails.FullName;
                            caseObj.AssignedTo_Region = userDetails.Region;
                            caseObj.AssignedTo_Role = userDetails.UserRole;
                            caseObj.Status = CaseStatus.Open;
                            if (!await _caseRepo.CreateCase(caseObj))
                            {
                                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when saving the record {caseObj.CaseNo}" });
                            }
                            if (!string.IsNullOrEmpty(caseDto.Note))
                            {
                                var saveNote = _caseRepo.CreateNote(caseObj, userDetails, caseDto.Note);
                                if (!await saveNote)
                                {
                                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when saving the case note for {caseObj.CaseNo}, Case not saved as well" });
                                }
                            }

                            if (caseDto.Files?.Any() == true)
                                await _caseRepo.CreateCaseFile(caseDto.Files, caseNo, caseObj.Id);

                            await transaction.CommitAsync();

                            await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_CASE, $"Created new Case, with CaseNo: '{caseNo}'");
                            return StatusCode(StatusCodes.Status201Created, new { success = true, message = $"Case Created Successfully..." });
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            _logger.LogError(ex, $"An error was encountered while commiting transaction");
                            return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"An error occured while trying to create case." });
                        }
                    }
                });
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error was encountered while creating a Case and or the Case Note");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateCase(int id, [FromBody] UpdateCaseDTO caseDto)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole == Role.Unknown)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to update a case" });
                }
                else if (caseDto == null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Case to be created is null" });
                }
                else if (caseDto.NetLoss < 0)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "NetLoss cannot be a negative value." });
                }
                else if (caseDto.GrossOnSlaught < caseDto.AmountRecovered)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "Gross Onslaught cannot be greater than the recovered amount" });
                }
                //string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value
                var entity = _caseRepo.GetCaseById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"No record found against the id {id}." });
                }
                else if (!string.IsNullOrEmpty(entity.AssignedUsers))
                {
                    if (!(entity.AssignedUsers.Contains(userDetails.EmailAddress)) && (userDetails.EmailAddress != entity.AssignedTo_Email) && (userDetails.UserRole != Role.Regional))
                    {
                        return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Sorry you can't update this record..." });
                    }
                }
                else if (userDetails.EmailAddress != entity.AssignedTo_Email && (userDetails.UserRole == Role.Affiliate))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Sorry you can't update this record..." });
                }
                entity.AmountRecovered = caseDto.AmountRecovered;
                entity.AmountRefuted = caseDto.AmountRefuted;
                entity.AmountPrevented = caseDto.AmountPrevented;
                entity.NetLoss = caseDto.NetLoss;
                entity.DescriptionOfIncident = caseDto.DescriptionOfIncident;
                entity.GrossOnSlaught = caseDto.GrossOnSlaught;
                entity.IncidentCategory = caseDto.IncidentCategory;
                entity.IncidentSubCategory = caseDto.IncidentSubCategory;
                entity.IncidentSub1Type = caseDto.IncidentSub1Type;
                entity.IncidentSub2Type = caseDto.IncidentSub2Type;
                //entity.UsdEquivalent = caseDto.UsdEquivalent;
                entity.Status = caseDto.Status;
                entity.DateModified = DateTime.Now.Date;
                entity.ModifiedBy_Name = userDetails.FullName;
                entity.ModifiedBy_Email = userDetails.EmailAddress;
                entity.ModifiedBy_Role = userDetails.UserRole;
                //entity.WhistleblowerNo = caseDto.WhistleblowerNo;
                //entity.CaseOutcome = caseDto.CaseOutcome;
                //entity.Currency = caseDto.Currency;
                // entity.DateOfIncident = caseDto.DateOfIncident;
                //entity.DateReported = caseDto.DateReported;
                //entity.DIR = caseDto.DIR;
                // entity.DRF = caseDto.DRF;
                // entity.Month = caseDto.Month;
                //entity.Quarter = caseDto.Quarter;
                //entity.Region = caseDto.Region;
                //entity.Source = caseDto.Source;
                //entity.StaffInvolvement = caseDto.StaffInvolvement;
                //entity.Affiliate = caseDto.Affiliate;
                //entity.Year = caseDto.Year;
                //caseObj.Id = entity.Id;
                //caseObj.CaseNo = entity.CaseNo;
                //caseObj.DateAdded = entity.DateAdded;
                //caseObj.AddedBy_Affiliate = entity.AddedBy_Affiliate;
                //caseObj.AddedBy_Email = entity.AddedBy_Email;
                //caseObj.AddedBy_Name = entity.AddedBy_Name;
                //caseObj.AddedBy_Region = entity.AddedBy_Region;
                //caseObj.AddedBy_Role = entity.AddedBy_Role;
                //caseObj.UserId = entity.UserId;
                //caseObj.AssignedTo = caseDto.AssignedTo;
                //caseObj.NetLoss = caseDto.GrossOnSlaught - caseDto.AmountRecovered;
                //caseObj.
                if (entity.Status == CaseStatus.Closed)
                {
                    if (string.IsNullOrEmpty(caseDto.CaseOutcome) || caseDto.DateFinalised == null || string.IsNullOrEmpty(caseDto.CorrectiveAction) || string.IsNullOrEmpty(caseDto.DRF))
                    {
                        return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"CaseOutcome, DateFinalised,CorrectiveAction,DRF cannot be empty" });
                    }
                    entity.CaseOutcome = caseDto.CaseOutcome;
                    entity.DateFinalised = caseDto.DateFinalised;
                    entity.CorrectiveAction = caseDto.CorrectiveAction;
                    //entity.DRF = caseDto.DRF;
                    entity.DateClosed = DateTime.Now.Date;
                    entity.ClosedBy_Name = userDetails.FullName;
                    entity.ClosedBy_Email = userDetails.EmailAddress;
                    entity.ClosedBy_Region = userDetails.Region;
                    entity.ClosedBy_Affiliate = userDetails.Affiliate;
                    // entity.ClosedBy_Role = userDetails.UserRole;
                }
                if (!await _caseRepo.UpdateCase(entity))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when updating the record {entity.CaseNo}" });

                }
                //if (!string.IsNullOrEmpty(caseDto.Note))
                //{
                //    var saveNote = _caseRepo.CreateNote(entity, userDetails, caseDto.Note);

                //    if (!saveNote)
                //    {
                //        ModelState.AddModelError("", $"Something went wrong when saving the case note for {entity.CaseNo}, Case not saved as well");
                //        return StatusCode(500, ModelState);
                //    }
                //}
                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_CASE, $"Updated Case: '{entity.CaseNo}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Record for CaseNo:{entity.CaseNo} Updated Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Updating a Case");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }
        [HttpPost("{id}")]
        public async Task<IActionResult> CloseCase(int id, [FromForm] CloseCaseDTO caseDto)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (caseDto == null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"The items to be added cannot be null" });
                }
                else if (caseDto.Status != CaseStatus.Closed)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "Case status was not set to 'Close'" });
                }

                if (caseDto.Files?.Any() == true && !_caseRepo.ValidateCaseFiles(caseDto.Files))
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "One or more case files are invalid" });

                //string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
                var executionStrategy = _db.Database.CreateExecutionStrategy();
                var result = await executionStrategy.ExecuteAsync(async () =>
                {
                    using (var transaction = await _db.Database.BeginTransactionAsync())
                    {
                        try
                        {
                            var entity = _caseRepo.GetCaseById(id);
                            if (entity == null)
                            {
                                return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"No record found against the id {id}." });
                            }
                            else if (!string.IsNullOrEmpty(entity.AssignedUsers))
                            {
                                if (!(entity.AssignedUsers.Contains(userDetails.EmailAddress)) && (userDetails.EmailAddress != entity.AssignedTo_Email) && (userDetails.UserRole != Role.Regional))
                                {
                                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Sorry you can't update this record..." });
                                }
                            }
                            else if (userDetails.EmailAddress != entity.AssignedTo_Email && userDetails.UserRole != Role.Regional/*userDetails.UserId != entity.UserId || */)
                            {
                                return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Sorry you can't update this record..." });
                            }
                            entity.Status = caseDto.Status;
                            entity.CaseOutcome = caseDto.CaseOutcome;
                            entity.DateFinalised = caseDto.DateFinalised;
                            entity.CorrectiveAction = caseDto.CorrectiveAction;
                            entity.RefutedOrSubstantiated = caseDto.RefutedOrSubstantiated;
                            entity.DateClosed = DateTime.Now.Date;
                            entity.ClosedBy_Name = userDetails.FullName;
                            entity.ClosedBy_Email = userDetails.EmailAddress;
                            entity.ClosedBy_Region = userDetails.Region;
                            entity.ClosedBy_Affiliate = userDetails.Affiliate;
                            //.ClosedBy_Role = userDetails.UserRole;
                            if (!await _caseRepo.UpdateCase(entity))
                            {
                                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when closing the record {entity.CaseNo}" });
                            }

                            if (caseDto.Files?.Any() == true)
                                await _caseRepo.CreateCaseFile(caseDto.Files, entity.CaseNo, entity.Id);

                            await transaction.CommitAsync();

                            await _repo.AuditLogger(userDetails, ActivityActionType.CLOSED_CASE, $"Closed Case with Case Number: '{entity.CaseNo}'");
                            return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Case:{entity.CaseNo} Closed Successfully..." });
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            _logger.LogError(ex, $"An error was encountered while commiting transaction");
                            return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"An error occured while trying to close case." });
                        }
                    }
                });
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Closing CaseId- {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetCaseByCaseNumber(string caseNo)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (caseNo == null)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "CaseNo Cannot be null!" });
                }
                else if (!_caseRepo.CaseExistsByCaseNo(caseNo))
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"CaseNo {caseNo} does not exist " });
                }
                var obj = _caseRepo.GetCaseByCaseNo(caseNo, userDetails);
                if (obj == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"You are not authorized to access CaseNo {caseNo}" });
                }
                var objDto = _mapper.Map<CaseDTO>(obj);
                objDto.StatusText = objDto.Status.GetDescription();
                var baseUrl = _config.GetSection("AppSettings:BaseUrl").Value.ToString();
                foreach (var file in objDto.CaseFiles)
                    file.FilePath = $"{baseUrl}/{file.FilePath}";

                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_CASEBYCASENUMBER, $"Retrieved Case: {caseNo}");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful", output = objDto });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred Getting Case with Case number - {caseNo}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetCaseByRegion(string region)
        {
            //TO DO: Input roles for Group, role and Affiliate users
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Affiliate && userDetails.UserRole != Role.Regional && userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to retrieve cases" });
                }
                var objList = _caseRepo.GetCasesByRegion(userDetails, region);
                if (objList.Count == 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful retrieved Case by Regions", output = objList });
                }
                var objDto = new List<CaseDTO>();
                foreach (var obj in objList)
                {
                    var objDtof = _mapper.Map<CaseDTO>(obj);
                    var dtx = _mapper.Map<CaseDTO>(obj);
                    dtx.StatusText = dtx.Status.GetDescription();
                    objDto.Add(dtx);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_CASESBYREGION, $"Retrieved Cases for the Region : {region}");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful", output = objDto });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred Getting Case by Region: - {region}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetCaseBySource(string source)
        {
            //TO DO: Input roles for Group, role and Affiliate users
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Affiliate && userDetails.UserRole != Role.Regional && userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to retrieve cases" });
                }
                var objList = _caseRepo.GetCasesBySource(source);
                if (objList.Count == 0)
                {
                    await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_CASESBYSOURCE, $"Retrieved Cases for the Source : {source}");
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful retrieved Case by Source", output = objList });
                }
                var objDto = new List<CaseDTO>();
                foreach (var obj in objList)
                {
                    var objDtof = _mapper.Map<CaseDTO>(obj);
                    var dtx = _mapper.Map<CaseDTO>(obj);
                    dtx.StatusText = dtx.Status.GetDescription();
                    objDto.Add(dtx);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_CASESBYSOURCE, $"Retrieved Cases for the Source : {source}");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful", output = objDto });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred Getting Case by Source: - {source}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetCaseByAffiliate(string affiliate)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Affiliate && userDetails.UserRole != Role.Regional && userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to retrieve cases" });
                }
                var objList = _caseRepo.GetCasesByAffiliate(affiliate);
                if (objList.Count == 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful retrieved Case by Affiliate", output = objList });
                }
                var objDto = new List<CaseDTO>();
                foreach (var obj in objList)
                {
                    var objDtof = _mapper.Map<CaseDTO>(obj);
                    var dtx = _mapper.Map<CaseDTO>(obj);
                    dtx.StatusText = dtx.Status.GetDescription();
                    objDto.Add(dtx);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_CASESBYAFFILIATE, $"Retrieved Cases for the Affiliate : {affiliate}");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful", output = objDto });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred Getting Case by Affiliate: - {affiliate}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCasebyCaseId(int id)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                //var obj = _caseRepo.GetCaseById(id);
                var obj = _db.Cases.Include(x => x.CaseNotes).Include(x => x.CaseFiles).Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
                if (obj == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "Case Not found!" });
                }

                var objDto = _mapper.Map<CaseDTO>(obj);
                objDto.StatusText = objDto.Status.GetDescription();
                var baseUrl = _config.GetSection("AppSettings:BaseUrl").Value.ToString();
                foreach (var file in objDto.CaseFiles)
                    file.FilePath = $"{baseUrl}/{file.FilePath}";

                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_CASEBYCASEID, $"Retrieved Cases for the Id : {id}");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful", output = objDto });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred Getting Case by Id: - {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetCases()
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Affiliate && userDetails.UserRole != Role.Regional && userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to retrieve cases" });
                }
                var objList = _caseRepo.GetAllCases(userDetails);
                if (objList.Count == 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful retrieval of Cases.", output = objList });
                }
                var objDto = new List<CaseDTO>();
                foreach (var obj in objList)
                {
                    var dtx = _mapper.Map<CaseDTO>(obj);
                    dtx.StatusText = dtx.Status.GetDescription();
                    objDto.Add(dtx);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_CASES, $"Retrieved Cases");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful", output = objDto });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Getting Cases");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddNewNoteToCase([FromBody] CaseNoteDTO noteDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (noteDTO == null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Note Cannot be null" });
                }
                else if (!_caseRepo.CaseExistsByCaseIdAndCaseNo(noteDTO.CaseId, noteDTO.CaseNo))
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "The Case does not exist!" });
                }
                var noteObj = _mapper.Map<CaseNote>(noteDTO);
                //noteObj.DateAdded = DateTime.Now.Date;
                //noteObj.AddedBy_Affiliate = userDetails.Affiliate;
                //noteObj.AddedBy_Email = userDetails.EmailAddress;
                //noteObj.AddedBy_Name = userDetails.FullName;
                //noteObj.AddedBy_Region = userDetails.Region;
                //noteObj.AddedBy_Role = userDetails.UserRole;
                if (!await _caseRepo.CreateNote(noteObj, userDetails))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when saving the Note for {noteDTO.CaseNo}" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_CASENOTE, $"Added new note with Id '{noteObj.Id}' to CaseNumber: '{noteDTO.CaseNo}'");
                return StatusCode(StatusCodes.Status201Created, new { success = true, message = $"Note for CaseNo:{noteDTO.CaseId} created Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while adding note to caseNumber {noteDTO.CaseNo}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateCaseNote(int id, [FromBody] UpdateCaseNoteDTO newNote)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (string.IsNullOrEmpty(newNote.Note))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"Note is empty" });
                }
                //string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
                var entity = _caseRepo.GetCaseNoteById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"No record found against the id {id}." });
                }
                if (!await _caseRepo.UpdateNote(userDetails, entity, newNote.Note))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when updating the note for {entity.CaseNo}" });
                }
                //var saveNote = _caseRepo.UpdateNote(entity.CaseNo, caseObj.Id, userDetails.FullName, userDetails.EmailAddress, userDetails.UserRole, caseDto.Note);
                //if (!saveNote)
                //{
                //    ModelState.AddModelError("", $"Something went wrong when saving the case note for {entity.CaseNo}, Case not saved as well");
                //    return StatusCode(500, ModelState);
                //}
                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_CASENOTE, $"Updated Case note with Id '{id}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Note for CaseNo:{entity.CaseNo} Updated Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while update CaseNote: {newNote}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        //[HttpPost("{id}")]
        //public IActionResult ReassignCase(int id, string reassigned)
        //{
        //    var currentUserDetails = HttpContext.User;
        //    var currentUserEmail = _userRepo.GetCurrentUserEmail(currentUserDetails);
        //    var userDetails = _userRepo.GetUserDetails(currentUserEmail, currentUserDetails);
        //    var entity = _caseRepo.GetCaseById(id);
        //    return Ok();

        //}
        [HttpPost("{id}")]
        public async Task<IActionResult> ReassignCase(int id, AssignmentDTO reassignmentDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole == Role.UserManager)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to reassign cases" });
                }
                var entity = _caseRepo.GetCaseByIdWithoutCaseNote(userDetails, id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"You do not have rights to reassign this case to " });
                }
                var reassignedUserDetails = _userRepo.GetUserByEmailAddress(reassignmentDTO.AssignToEmail);
                if (reassignedUserDetails == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"'{reassignmentDTO.AssignToEmail}' is not an existing user" });
                }
                else if (reassignedUserDetails.UserRole == Role.UserManager)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"'You cannot reassign a case to the Role - {reassignedUserDetails.UserRole}" });
                }
                else if (!string.IsNullOrEmpty(entity.AssignedUsers))
                {
                    if ((entity.AssignedUsers.Contains(reassignedUserDetails.EmailAddress)) || (entity.AssignedTo_Email == reassignedUserDetails.EmailAddress))
                    {
                        return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"'{reassignmentDTO.AssignToEmail}' is  an existing user" });
                    }
                }
                else if (entity.AssignedTo_Email == reassignedUserDetails.EmailAddress)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"'{reassignmentDTO.AssignToEmail}' is  an existing user" });
                }
                if (!await _caseRepo.ReassignCase(userDetails, entity, reassignedUserDetails))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong while reassigning the Case for Id: '{entity.Id}'" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.REASSIGNED_CASE, $"Reassign case");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Reassignment Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Reassigning CaseID: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> AssignCaseToUsers(int id, AssignmentDTO assignmentDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole == Role.UserManager || userDetails.UserRole == Role.Affiliate)
                {
                    //an affiliate user cannot assign a case
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to assign cases" });
                }
                var entity = _caseRepo.GetCaseByIdWithoutCaseNote(userDetails, id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"You do not have rights to reassign this case" });
                }
                string[] stringSeparators = new string[] { "," };
                string[] emailArray = (assignmentDTO.AssignToEmail).Split(stringSeparators, StringSplitOptions.None);
                List<string> unSavedAssignment = new List<string>();
                List<string> savedAssignment = new List<string>();
                List<string> savedAssignment_Name = new List<string>();
                foreach (string item in emailArray)
                {
                    var email = item.ToLower().Trim();
                    if (entity.AssignedTo_Email.ToLower().Contains(email))
                    {
                        unSavedAssignment.Add(email);
                    }
                    else if (!(string.IsNullOrEmpty(entity.AssignedUsers)))
                    {
                        if (entity.AssignedUsers.Contains(email.ToLower()))
                        {
                            unSavedAssignment.Add(email);
                        }
                        else
                        {
                            var assignedUserDetails = _userRepo.GetUserByEmailAddress(email);
                            if (assignedUserDetails == null)
                            {
                                unSavedAssignment.Add(email);
                            }
                            //else if (assignedUserDetails.UserRole == Role.UserManager || assignedUserDetails.UserRole == Role.Group)
                            //for the commented else if statement, At first, they requested that a group user should not be assigned a case but they changed their minds.
                            //a group user can now be assigned a case
                            else if (assignedUserDetails.UserRole == Role.UserManager || assignedUserDetails.UserRole == Role.Group)
                            {
                                unSavedAssignment.Add(email);
                            }
                            else
                            {
                                var name = assignedUserDetails.FullName == "" ? email : assignedUserDetails.FullName;
                                //entity.AssignedUsers.Add(email);
                                savedAssignment.Add(email);
                                savedAssignment_Name.Add(name);
                            }
                        }
                    }
                    else
                    {
                        var assignedUserDetails = _userRepo.GetUserByEmailAddress(email);
                        if (assignedUserDetails == null)
                        {
                            unSavedAssignment.Add(email);
                        }
                        else if (assignedUserDetails.UserRole == Role.UserManager || assignedUserDetails.UserRole == Role.Group)
                        {
                            unSavedAssignment.Add(email);
                        }
                        else
                        {
                            var name = assignedUserDetails.FullName == "" ? email : assignedUserDetails.FullName;
                            //entity.AssignedUsers.Add(email);
                            savedAssignment.Add(email);
                            savedAssignment_Name.Add(name);
                        }
                    }
                }
                string unSaved = String.Join(",", unSavedAssignment);
                string saved = String.Join(",", savedAssignment);
                string savedName = String.Join(",", savedAssignment_Name);

                if (string.IsNullOrEmpty(entity.AssignedUsers))
                {
                    entity.AssignedUsers_Name = savedName;
                    entity.AssignedUsers = saved;
                }
                else
                {
                    var emails = entity.AssignedUsers;
                    var names = entity.AssignedUsers_Name;
                    entity.AssignedUsers = emails + "|" + saved;
                    entity.AssignedUsers_Name = names + "|" + savedName;
                }
                if (string.IsNullOrEmpty(saved))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"Case was not assigned to the user(s): {unSaved}. User(s) is/are already assigned to the case {entity.CaseNo} or User(s) is/are not authorized to be assigned a case" });
                }

                if (string.IsNullOrEmpty(entity.AssignedUsers_AddedByName))
                {
                    DateTime dt = DateTime.Now;
                    entity.AssignedUsers_DateAdded = (dt.AddSeconds(-dt.Second)).ToString();
                    entity.AssignedUsers_AddedByName = userDetails.FullName;
                    entity.AssignedUsers_AddedByEmail = userDetails.EmailAddress;
                }
                else
                {
                    DateTime dt = DateTime.Now;
                    var dateAdded = entity.AssignedUsers_DateAdded;
                    var assignedUsersName = entity.AssignedUsers_AddedByName;
                    var assignedUsersEmail = entity.AssignedUsers_AddedByEmail;
                    entity.AssignedUsers_DateAdded = dateAdded + "|" + (dt.AddSeconds(-dt.Second)).ToString();
                    entity.AssignedUsers_AddedByName = assignedUsersName + "|" + userDetails.FullName;
                    entity.AssignedUsers_AddedByEmail = assignedUsersEmail + "|" + userDetails.EmailAddress;

                }
                //entity.DateUsersAssigned = DateTime.Now.Date;
                //entity.AssignedUsers_AddedByName = userDetails.FullName;
                //entity.AssignedUsers_AddedByEmail = userDetails.EmailAddress;
                //entity.DateModified = DateTime.Now.Date;
                //entity.ModifiedBy_Name = userDetails.FullName;
                //entity.ModifiedBy_Email = userDetails.EmailAddress;
                //entity.ModifiedBy_Role = userDetails.UserRole;
                if (!await _caseRepo.UpdateCase(entity))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong while assigning the Case for Id: '{entity.Id}'" });
                }

                if (unSaved.Count() != 0)
                {
                    await _repo.AuditLogger(userDetails, ActivityActionType.ASSIGNCASE_TOUSERS, $"Assigned case to users");
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Users assigned successfully but the user(s) '{unSaved}' was/were not added." });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.ASSIGNCASE_TOUSERS, $"Assigned case to users");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Users assigned successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Assigning Case to Users");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> SearchCases(SearchCaseDTO searchCaseDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Affiliate && userDetails.UserRole != Role.Regional && userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to retrieve cases" });
                }
                var objList = _caseRepo.SearchAllCases(userDetails, searchCaseDTO);
                if (objList.Count == 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful retrieval of Cases.", output = objList });
                }

                var objDto = new List<SearchCaseDto>();
                foreach (var obj in objList)
                {
                    var dtx = _mapper.Map<SearchCaseDto>(obj);
                    dtx.CaseStatus = obj.Status.ToString();
                    objDto.Add(dtx);
                }

                await _repo.AuditLogger(userDetails, ActivityActionType.SEARCH_CASES, $"Retrieved Cases via search");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful", output = objDto });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Searching Cases");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Upload files to a case
        /// </summary>
        /// <param name="id">Case Id</param>
        /// <param name="Files">File(s) to upload</param>
        /// <returns>A list of upload file paths</returns>
        [HttpPost("~/api/[controller]/{id}/UploadFiles")]
        public async Task<IActionResult> UploadFiles([FromRoute] int id, [FromForm] IList<IFormFile> files)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                    return StatusCode(validateResult.StatusCode, new { success = validateResult.Success, message = validateResult.Message });

                var userDetails = validateResult.UserProfile;

                if (!files.Any())
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Kindly select file(s) to upload" });

                if (!_caseRepo.ValidateCaseFiles(files))
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "One or more case files are invalid" });

                if (userDetails.UserRole == Role.Unknown || userDetails.UserRole == Role.UserManager)
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to carry out this operation" });

                var _case = await _db.Cases.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
                if (null == _case)
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Case not found" });

                if (userDetails.UserRole == Role.Regional && !userDetails.Region.ToLower().Equals(_case.Region.ToLower()))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You cannot add files to cases in other regions" });

                if (userDetails.UserRole == Role.Affiliate && !userDetails.Affiliate.ToLower().Equals(_case.Affiliate?.ToLower()))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You cannot add files to cases in other affiliates" });

                var caseFileNames = await _caseRepo.CreateCaseFile(files, _case.CaseNo, _case.Id);

                await _repo.AuditLogger(userDetails, ActivityActionType.UPLOAD_CASE_FILES, $"Added files to Case, with CaseNo: '{_case.CaseNo}'");
                return StatusCode(StatusCodes.Status201Created, new { success = true, message = $"Case files uploaded successfully.", output = caseFileNames });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while uploading case files");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = "A error occured while uploading case files" });
            }
        }

        /// <summary>
        /// Delete case file(s)
        /// </summary>
        /// <param name="id">Case Id</param>
        /// <param name="fileIds">Ids or files to be deleted</param>
        /// <returns>A list of upload file paths</returns>
        [HttpDelete("~/api/[controller]/{id}/DeleteFiles")]
        public async Task<IActionResult> DeleteCaseFiles([FromRoute] int id, [FromQuery] IEnumerable<int> fileIds)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                    return StatusCode(validateResult.StatusCode, new { success = validateResult.Success, message = validateResult.Message });

                var userDetails = validateResult.UserProfile;

                if (!fileIds.Any())
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Kindly specify the file(s) to be deleted" });

                if (userDetails.UserRole == Role.Unknown || userDetails.UserRole == Role.UserManager)
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to carry out this operation" });

                var _case = await _db.Cases.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
                if (null == _case)
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Case not found" });

                if (userDetails.UserRole == Role.Regional && !userDetails.Region.ToLower().Equals(_case.Region.ToLower()))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You cannot delete files from cases in other regions" });

                if (userDetails.UserRole == Role.Affiliate && !userDetails.Affiliate.ToLower().Equals(_case.Affiliate?.ToLower()))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You cannot delete files from cases in other affiliates" });

                var files = await _db.CaseFiles.Where(x => fileIds.Contains(x.Id)).ToListAsync();
                string webRootPath = _webEnvironment.WebRootPath;
                foreach (var file in files)
                {
                    var fullPath = System.IO.Path.Combine(webRootPath, file.FilePath);
                    _logger.LogInformation($"DELETE_CASE_FILE => File path - {fullPath}");
                    //fullPath = fullPath.Replace("/\\", "/");
                    //_logger.LogInformation(fullPath);
                    ////Debug.WriteLine(fullPath);
                    //fullPath = fullPath.Replace("\\", "/");
                    //_logger.LogInformation(fullPath);
                    //Debug.WriteLine(fullPath);
                    if (System.IO.File.Exists(fullPath))
                    {
                        _logger.LogInformation($"DELETE_CASE_FILE => File path exists");
                        System.IO.File.Delete(fullPath);
                    }
                    else _logger.LogInformation($"DELETE_CASE_FILE => File path does not exist");
                }
                _db.CaseFiles.RemoveRange(files);
                await _db.SaveChangesAsync();

                await _repo.AuditLogger(userDetails, ActivityActionType.DELETE_CASE_FILES, $"Deleted files from Case, with CaseNo: '{_case.CaseNo}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Case files deleted successfully." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while deleting case files");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = "A error occured while deleting case files" });
            }
        }
    }
}
