﻿using AutoMapper;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    //[Authorize(Roles = "Affiliate,Region,Group")]
    [Authorize]
    public class RegionController : ControllerBase
    {
        private readonly ICaseRepo _caseRepo;
        private readonly IRegionRepo _regionRepo;
        private readonly IAffiliateRepo _affiliateRepo;
        private readonly IMapper _mapper;
        private readonly IUserRepo _userRepo;
        private readonly IAuthRepo _authRepo;
        private readonly ILogger<RegionController> _logger;
        private readonly IRepo _repo;

        public RegionController(IRepo repo, ICaseRepo caseRepo, IAffiliateRepo affiliateRepo,IRegionRepo regionRepo, IMapper mapper, IUserRepo userRepo, IAuthRepo authRepo, ILogger<RegionController> logger)
        {
            _regionRepo = regionRepo;
            _mapper = mapper;
            _userRepo = userRepo;
            _authRepo = authRepo;
            _logger = logger;
            _affiliateRepo = affiliateRepo;
            _caseRepo = caseRepo;
            _repo = repo;
        }
        [HttpPost]
        public async Task<IActionResult> CreateRegion([FromBody] CreateRegionDTO createRegionDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to create a new region" });
                }
                if (createRegionDTO == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "Region cannot be null" });
                }
                if (_regionRepo.RegionExists(createRegionDTO.Region))
                {
                    return StatusCode(StatusCodes.Status409Conflict, new { success = false, message = "Incident Category Already Exists!" });

                }
                var regionObj = new Region();


                regionObj.Region_Name = createRegionDTO.Region;
                regionObj.DateAdded = DateTime.Now.Date;
                regionObj.AddedBy_Affiliate = userDetails.Affiliate;
                regionObj.AddedBy_Email = userDetails.EmailAddress;
                regionObj.AddedBy_Name = userDetails.FullName;
                regionObj.AddedBy_Role = userDetails.UserRole;


                if (! await _regionRepo.CreateRegion(regionObj))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something wrong while creating Regions" });
                }


                if (!string.IsNullOrEmpty(createRegionDTO.Affiliates))
                {
                    string[] stringSeparators = new string[] { "," };
                    string[] affiliateArray = (createRegionDTO.Affiliates).Split(stringSeparators, StringSplitOptions.None);
                    List<string> unsavedAffiliate = new List<string>();
                    //List<string> savedSubcategory = new List<string>();

                    foreach (var affiliate in affiliateArray)
                    {
                        if (_affiliateRepo.AffiliatesExists(affiliate))
                        {
                            unsavedAffiliate.Add(affiliate);
                        }
                        else
                        {
                            var affiliateObj = new Affiliate();

                            affiliateObj.RegionId = regionObj.Id;
                            affiliateObj.Affiliate_Name = affiliate.Trim();
                            affiliateObj.DateAdded = DateTime.Now.Date;
                            affiliateObj.AddedBy_Affiliate = userDetails.Affiliate;
                            affiliateObj.AddedBy_Email = userDetails.EmailAddress;
                            affiliateObj.AddedBy_Name = userDetails.FullName;
                            affiliateObj.AddedBy_Role = userDetails.UserRole;

                            var saveAffiliate = _affiliateRepo.CreateAffiliate(affiliateObj);

                            if (!await saveAffiliate)
                            {
                                unsavedAffiliate.Add(affiliate);
                            }
                        }

                        // savedSubcategory.Add(subCategory);
                    }
                    string unSaved = String.Join(",", unsavedAffiliate);
                    //string saved = String.Join(",", savedSubcategory);


                    if (unsavedAffiliate.Count() != 0)
                    {
                        await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_REGION,
                        $"Created new Region: '{createRegionDTO.Region}' and Affiliates '{createRegionDTO.Affiliates}'");

                        return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Region created successfully but '{unSaved}' affiliate was/were not created successfully." });
                    }
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_REGION,
                       $"Created new Region: '{createRegionDTO.Region}' and Affiliates '{createRegionDTO.Affiliates}'");

                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Region and affiliate created successfully" });

                //return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Region and affiliate created successfully" });

                // return new JsonResult(new { status = StatusCodes.Status200OK, success = true,  message = $"Region and affiliate created successfully" });

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Creating Region {createRegionDTO.Region}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }

        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateRegion(int id, [FromBody] UpdateRegionDTO updateRegionDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to access this module" });
                }
                else if (string.IsNullOrEmpty(updateRegionDTO.Region))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"Region to be updated is empty" });
                }
                var entity = _regionRepo.GetRegionById(id);
                var previousName = entity.Region_Name;
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"Region does not exist" });
                }
                if (! await _regionRepo.UpdateRegion(userDetails, entity, updateRegionDTO.Region))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when updating the Region for Id: {entity.Id}" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_REGION, $"Changed Region Name - from  '{previousName}' to '{updateRegionDTO.Region}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Region Updated Successfully..." });

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Updating Region {updateRegionDTO.Region}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }

        }

        [HttpPost("{id}")]
        public async Task<IActionResult> DeleteRegion(int id)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to Region Module" });
                }

                var entity = _regionRepo.GetRegionById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"Region does not exist" });
                    
                }
                else if (_caseRepo.GetCaseCountByRegion(entity.Region_Name) != 0)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = $"The Region: '{entity.Region_Name}' cannot be deleted because it has case(s) assigned to it" });
                }
                if (! await _regionRepo.DeleteRegion(userDetails, entity))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when Deleting the Region for ID: {entity.Id}" });
                }
                var affiliates = _affiliateRepo.GetAffiliatesByRegionId(entity.Id);
                if (affiliates.Count > 0)
                {
                    foreach (var affiliate in affiliates)
                    {
                        if (!await _affiliateRepo.DeleteAffiliate(userDetails, affiliate))
                        {
                            return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Region deleted successfully but something went wrong when Deleting the '{affiliate}' Affiliate for ID: {entity.Id}" });
                        }
                        //var subCategories = _regionRepo.DeleteIncidentSubCategory(userDetails,subCategory);
                    }
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.DELETED_REGION, $"Deleted Region '{entity.Region_Name}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Region Deleted Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Deleting Region  with Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetRegions()
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                var objList = _regionRepo.GetRegions(userDetails);
                if (objList.Count == 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful Regions Retrieval...", output = objList });
                }
                var objDto = new List<GetRegionsDTO>();
                foreach (var obj in objList)
                {
                    var dtx = _mapper.Map<GetRegionsDTO>(obj);
                    objDto.Add(dtx);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_REGIONS, $"Retrieved all Regions");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful Regions Retrieval...", output = objDto });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while getting Regions");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }

        }

    }
}
