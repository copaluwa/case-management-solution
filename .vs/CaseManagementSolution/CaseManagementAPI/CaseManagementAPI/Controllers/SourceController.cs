﻿using AutoMapper;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class SourceController : ControllerBase
    {
        private readonly ICaseRepo _caseRepo;
        private readonly ISourceRepo _sourceRepo;
        private readonly IMapper _mapper;
        private readonly IUserRepo _userRepo;
        private readonly IAuthRepo _authRepo;
        private readonly ILogger<SourceController> _logger;
        private readonly IRepo _repo;
        public SourceController(IRepo repo, ICaseRepo caseRepo, ISourceRepo sourceRepo, IMapper mapper, IUserRepo userRepo, IAuthRepo authRepo, ILogger<SourceController> logger)
        {
            _sourceRepo = sourceRepo;
            _mapper = mapper;
            _userRepo = userRepo;
            _authRepo = authRepo;
            this._logger = logger;
            _caseRepo = caseRepo;
            _repo = repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetSources()
        {

            try
            {
                _logger.LogInformation($"Get Sources");
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole == Role.UserManager || userDetails.UserRole == Role.Unknown)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to retrieve Sources" });
                }


                var objList = _sourceRepo.GetSources();
                if (objList.Count == 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful Sources Retrieval...", output = objList });
                }
                var objDto = new List<CaseSourceDTO>();
                foreach (var obj in objList)
                {

                    var dtx = _mapper.Map<CaseSourceDTO>(obj);
                    objDto.Add(dtx);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_SOURCES, $"Retrieved all Sources");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful Sources Retrieval...", output = objDto });

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Getting all Case Source");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }


            //[HttpGet]
            //public IActionResult GetSources()
            //{
            //    var currentUserDetails = HttpContext.User;
            //    var currentUserEmail = _userRepo.GetCurrentUserEmail(currentUserDetails);
            //    var userDetails = _userRepo.GetUserDetails(currentUserEmail, currentUserDetails);

            //    if (userDetails == null)
            //    {
            //        ModelState.AddModelError("", "Unable to retrieve Userdetails!");
            //        return StatusCode(404, ModelState);
            //    }
            //    else if (userDetails.UserRole == Role.UserManager || userDetails.UserRole == Role.Unknown)
            //    {
            //        //return new JsonResult(new { status = StatusCodes.Status403Forbidden, success = false,  message = $"You are not authorized to retrieve cases" });
            //        ModelState.AddModelError("", "You are not authorized to retrieve Sources");
            //        return StatusCode(403, ModelState);
            //    }
            //    var objList = _sourceRepo.GetSources();
            //    if (objList.Count == 0)
            //    {

            //        return Ok(objList);
            //    }
            //    var objDto = new List<CaseSourceDTO>();
            //    foreach (var obj in objList)
            //    {

            //        var dtx = _mapper.Map<CaseSourceDTO>(obj);
            //        objDto.Add(dtx);
            //    }
            //    return Ok(objDto);
            //}




        }


        [HttpPost]
        public async Task<IActionResult> CreateSource([FromBody] CaseSourceDTO sourceDto)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to create a Source" });
                }
                if (sourceDto == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "Source Value cannot be null" });
                }
                if (_sourceRepo.SourceExists(sourceDto.Source))
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "Source Already Exists!" });
                }

                var sourceObj = _mapper.Map<CaseSource>(sourceDto);

                sourceObj.DateAdded = DateTime.Now.Date;
                sourceObj.AddedBy_Affiliate = userDetails.Affiliate;
                sourceObj.AddedBy_Email = userDetails.EmailAddress;
                sourceObj.AddedBy_Name = userDetails.FullName;
                sourceObj.AddedBy_Region = userDetails.Region;
                sourceObj.AddedBy_Role = userDetails.UserRole;

               
                if (! await _sourceRepo.CreateSource(sourceObj))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong while creating source." });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_SOURCE,
                    $"Created new Source: '{sourceDto.Source}' with Id '{sourceObj.Id}'");
                return StatusCode(StatusCodes.Status201Created, new { success = true, message = $"Source created successfully" });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while creating Source: {sourceDto.Source}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }


        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateSource(int id, [FromBody] CaseSourceDTO sourceDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to access this module" });

                }
                else if (string.IsNullOrEmpty(sourceDTO.Source))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"Source to be updated is empty" });

                }
                var entity = _sourceRepo.GetSourceById(id);
                var previousName = entity.Source;
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"Source does not exist" });
                }

                if (! await _sourceRepo.UpdateSource(userDetails, entity, sourceDTO.Source))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when updating the Source for SourceID: {entity.Id}" });
                }

                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_SOURCE, $"Changed Source Name - from  '{previousName}' to '{sourceDTO.Source}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Source Updated Successfully..." });

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while updating Source: {sourceDTO.Source}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }
        }
        [HttpPost("{id}")]
        public async Task<IActionResult> DeleteSource(int id)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to Source Module" });
                }
                var entity = _sourceRepo.GetSourceById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"Source does not exist" });
                }
                else if (_caseRepo.GetCaseCountBySource(entity.Source) != 0)
                {

                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = $"The Source: '{entity.Source}' cannot be deleted because it has case(s) assigned to it" });
                }
                if (! await _sourceRepo.DeleteSource(userDetails, entity))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when Deleting the Source for SourceID: {entity.Id}" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.DELETED_SOURCE, $"Deleted Source '{entity.Source}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Source Deleted Successfully..." });

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while deleting Source with Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }

        }

    }
}
