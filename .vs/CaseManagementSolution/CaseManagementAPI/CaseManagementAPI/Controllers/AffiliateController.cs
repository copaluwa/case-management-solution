﻿using AutoMapper;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    //[Authorize(Roles = "Affiliate,Region,Group")]
    [Authorize]
    public class AffiliateController : ControllerBase
    {
            private readonly ICaseRepo _caseRepo;
            private readonly IAffiliateRepo _affiliateRepo;
            private readonly IMapper _mapper;
            private readonly IUserRepo _userRepo;
            private readonly IAuthRepo _authRepo;
            private readonly ILogger<AffiliateController> _logger;
            private readonly IRepo _repo;

        public AffiliateController(IRepo repo, ICaseRepo caseRepo,IAffiliateRepo affiliateRepo, IMapper mapper, IUserRepo userRepo, IAuthRepo authRepo, ILogger<AffiliateController> logger)
            {

                _affiliateRepo = affiliateRepo;
                _mapper = mapper;
                _userRepo = userRepo;
                _authRepo = authRepo;
                _logger = logger;
                _caseRepo = caseRepo;
                _repo = repo;
        }

        [HttpPost]
        public async Task<IActionResult> CreateAffiliate([FromBody] CreateAffiliateDTO createAffiliateDto)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to create an Affiliate" });
                }
                if (createAffiliateDto == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "Affiliate cannot be null" });
                }
                if (_affiliateRepo.AffiliatesExists(createAffiliateDto.Affiliate))
                {
                    return StatusCode(StatusCodes.Status409Conflict, new { success = false, message = "Affiliate Already Exists!" });
                }
                var affiliateObj = new Affiliate();

                affiliateObj.Affiliate_Name = createAffiliateDto.Affiliate;
                affiliateObj.RegionId = createAffiliateDto.RegionId;
                affiliateObj.DateAdded = DateTime.Now;
                affiliateObj.AddedBy_Affiliate = userDetails.Affiliate;
                affiliateObj.AddedBy_Email = userDetails.EmailAddress;
                affiliateObj.AddedBy_Name = userDetails.FullName;
                affiliateObj.AddedBy_Role = userDetails.UserRole;


                if (!await _affiliateRepo.CreateAffiliate(affiliateObj))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something wrong while creating Affiliate" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_AFFILIATE, $"Created affiliate:  {createAffiliateDto.Affiliate}");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Affiliate created successfully" });
            }
            catch (Exception ex)
            {

                _logger.LogError(ex, $"An error occurred while creating Affiliate: {createAffiliateDto.Affiliate}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateAffiliate(int id, [FromBody] UpdateAndDeleteAffiliateDTO updateAffiliateDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to access this module" });
                }
                else if (string.IsNullOrEmpty(updateAffiliateDTO.Affiliate))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"Affiliate to be updated is empty" });
                }
                var entity = _affiliateRepo.GetAffiliatesById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"Affiliate does not exist" });
                }
                var previousName = entity.Affiliate_Name;
                if (_affiliateRepo.AffiliatesExists(previousName))
                {
                    return StatusCode(StatusCodes.Status409Conflict, new { success = false, message = "Affiliate Already Exists!" });
                }
                if (! await _affiliateRepo.UpdateAffiliate(userDetails, entity, updateAffiliateDTO.Affiliate))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when updating the Affiliate for Id: {entity.Id}" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_AFFILIATE, $"Updated affililiate from previousName to {updateAffiliateDTO.Affiliate}.");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Affiliate Updated Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Updating an Affiliate with Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }

        }


        [HttpPost("{id}")]
        public async Task<IActionResult> DeleteAffiliate(int id)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to Affiliate Module" });
                }

                var entity = _affiliateRepo.GetAffiliatesById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"Affiliate does not exist" });
                }
                else if (_caseRepo.GetCaseCountByAffiliate(entity.Affiliate_Name) != 0)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = $"The Affiliate: '{entity.Affiliate_Name}' cannot be deleted because it has case(s) assigned to it" });

                }
                if (!await _affiliateRepo.DeleteAffiliate(entity))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when Deleting the Affiliate for ID: {entity.Id}" });

                    //ModelState.AddModelError("", $"Something went wrong when Deleting the Affiliate for ID: {entity.Id}");
                    //return StatusCode(500, ModelState);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_AFFILIATE, $"Deleted affililiate {entity.Affiliate_Name}.");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Affiliate Deleted Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Deleting an Affiliate with Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }
        }
    }
}
