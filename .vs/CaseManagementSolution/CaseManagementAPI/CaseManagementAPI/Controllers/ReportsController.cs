﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CaseManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReportsController : ControllerBase
    {
        private readonly ILogger<ReportsController> _logger;
        private readonly IRepo _repo;
        private readonly ICaseRepo _caseRepo;
        private readonly IUserRepo _userRepo;
        private readonly IAuthRepo _authRepo;
        private readonly IReportRepo _reportRepo;

        public ReportsController(
            IRepo repo,
            ICaseRepo caseRepo,
            IReportRepo reportRepo,
            IUserRepo userRepo,
            IAuthRepo authRepo,
            ILogger<ReportsController> logger
        )
        {
            _logger = logger;
            _userRepo = userRepo;
            _authRepo = authRepo;
            _caseRepo = caseRepo;
            _repo = repo;
            _reportRepo = reportRepo;
        }

        /// <summary>
        /// Get cases per sources
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("PerSources")]
        public async Task<IActionResult> GetReportsBySource([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetReportBySource(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_CASE_SOURCE, $"Retrieved all report by Source. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by Source", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_SOURCE => An error occurred while retrieving report by source");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get cases per quarter
        /// </summary>
        /// <param name="Status"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("PerQuarter")]
        public async Task<IActionResult> GetReportPerQuarter([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetReportPerQuarter(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_QUARTER, $"Retrieved all report per quarter.");

                return Ok(new { success = true, message = $"Successful retrieval of report per quarter", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_QUARTER => An error occurred while retrieving report per quarter");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get cases per incident categories
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("PerIncidentCategories")]
        public async Task<IActionResult> GetReportsByIncidetCategories([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetReportByIncidentCategories(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_INCIDENT_CATEGORY, $"Retrieved all report by incident category. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by incident categories", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_INCIDENT_CATEGORIES => An error occurred while retrieving report by incident categories");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get cases per incident type (open and closed)
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("PerIncidentTypes")]
        public async Task<IActionResult> GetTotalReportCases([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetReportByIncidentTypes(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_INCIDENT_TYPE, $"Retrieved all report by incident types. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by incident types", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_INCIDENT_TYPES => An error occurred while retrieving report by incident types");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get cases per staff involvement
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("PerStaffInvolvements")]
        public async Task<IActionResult> GetReportsByStaffInvolvement([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetReportByStaffInvolvement(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_STAFF_INVOLVEMENT, $"Retrieved all report by staff involvement. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by staff involvement", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_STAFF_INVOLVEMENT => An error occurred while retrieving report by staff involvement");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get cases per affiliates
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("PerAffiliates")]
        public async Task<IActionResult> GetReportsByAffiliates([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetReportByAffiliates(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_AFFILIATES, $"Retrieved all report by affiliates. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by affiliates", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_AFFILIATES => An error occurred while retrieving report by affiliates");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get sum of gross onslaught
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("SumGrossOnslaught")]
        public async Task<IActionResult> GetGrossOnslaughtReport([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetGrossOnslaughtReport(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_GROSS_ONSLAUGHT, $"Retrieved all report by gross onslaught. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by gross onslaught", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_GROSS_ONSLAUGHT => An error occurred while retrieving report by gross onslaught");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get sum of gross onslaught per affiliate
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("SumGrossOnslaughtPerAffiliate")]
        public async Task<IActionResult> GetGrossOnslaughtPerAffiliate([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.SumGrossOnslaughtPerAffiliateReport(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_AFFILIATE_GROSS_ONSLAUGHT, $"Retrieved all report by gross onslaught per affiliate. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by gross onslaught per affiliate", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_GROSS_ONSLAUGHT_SUM_PER_AFFILIATE => An error occurred while retrieving report by gross onslaught per affiliate");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get net loss per affiliate
        /// </summary>
        /// <param name="command"></param>
        /// <returns>A response string</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("SumNetLoss")]
        public async Task<IActionResult> GetNetLossReport([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetNetLossReport(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_NET_LOSS, $"Retrieved all report by net loss. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by net loss", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_NET_LOSS => An error occurred while retrieving report by net loss");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get sum of net loss per affiliate
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("SumNetLossPerAffiliate")]
        public async Task<IActionResult> SumNetLossPerAffiliate([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.SumNetLossPerAffiliateReport(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_NET_LOSS_SUM_PER_AFFILIATE, $"Retrieved all report by net loss sum per affiliate. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by net loss sum per affiliate", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_NET_LOSS_SUM_PER_AFFILIATE => An error occurred while retrieving report by net loss sum per affiliate");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get sum of net loss per incident categories
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("SumNetLossPerIncidentCategories")]
        public async Task<IActionResult> SumNetLossPerIncidentCategories([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.SumNetLossPerIncidentCategoryReport(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_NET_LOSS_SUM_PER_INCIDENT_CATEGORY, $"Retrieved all report by net loss sum per incident categories. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by net loss sum per incident categories", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_NET_LOSS_SUM_PER_INCIDENT_CATEGORIES => An error occurred while retrieving report by net loss sum per incident categories");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get sum of net loss per staff involvement
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("SumNetLossPerStaffInvolvement")]
        public async Task<IActionResult> SumNetLossPerStaffInvolvement([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.SumNetLossPerStaffInvolvementReport(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_NET_LOSS_SUM_PER_STAFF_INVOLVEMENT, $"Retrieved all report by net loss sum per staff involvement. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by net loss sum per staff involvement", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_NET_LOSS_SUM_PER_INCIDENT_CATEGORIES => An error occurred while retrieving report by net loss sum per staff involvement");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get the average days per affiliate from incident date to report date
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("AverageDaysPerAffiliate")]
        public async Task<IActionResult> GetAverageDaysPerAffiliate([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetAverageDaysPerAffiliate(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_AVERAGE_REPORT_DAYS_PER_AFFILIATE, $"Retrieved all report by average days per affiliate. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by average days per affiliate", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_AVERAGE_DAYS_PER_AFFILIATE => An error occurred while retrieving report by average days per affiliate");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get the average days per source from incident date to report date
        /// </summary>
        /// <param name="request"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("AverageDaysPerSource")]
        public async Task<IActionResult> GetAverageDaysPerSource([FromQuery] ReportRequest request)
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetAverageDaysPerSource(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    EndDate = request.EndDate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole,
                    StartDate = request.StartDate,
                    Status = request.Status
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_AVERAGE_REPORT_DAYS_PER_SOURCE, $"Retrieved all report by average days per source. Report request - {JsonSerializer.Serialize(request)}");

                return Ok(new { success = true, message = $"Successful retrieval of report by average days per source", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_AVERAGE_DAYS_PER_SOURCE => An error occurred while retrieving report by average days per source");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Get finalized cases
        /// </summary>
        /// <param name="Status"></param>
        /// <returns>A response object</returns>
        /// <response code="200">A response object</response>
        /// <response code="500">If Application encounters exception</response>
        [Produces("application/json")]
        [HttpGet("FinalizedCases")]
        public async Task<IActionResult> GetFinalizedCases()
        {
            try
            {
                var userDetailsResult = GetUserDetails(out UserProfile userDetails);
                if (null == userDetails) return userDetailsResult;

                if (!ValidateUserRole(userDetails.UserRole, out string message))
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = message });

                var response = await _reportRepo.GetFinalizedCases(new ReportRequestDTO
                {
                    Affiliate = userDetails.Affiliate,
                    Region = userDetails.Region,
                    Role = userDetails.UserRole
                });

                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_FINALIZED_CASES, $"Retrieved all report by finalized cases.");

                return Ok(new { success = true, message = $"Successful retrieval of report by finalized cases", output = response });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"GET_REPORT_BY_FINALIZED_CASES => An error occurred while retrieving report by finalized cases");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        #region private methods
        private IActionResult GetUserDetails(out UserProfile userDetails)
        {
            var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
            if (!validateResult.Success)
            {
                userDetails = null;
                return StatusCode(validateResult.StatusCode, new
                {
                    success = validateResult.Success,
                    message = validateResult.Message
                });
            }
            userDetails = validateResult.UserProfile;
            return NoContent();
        }

        /// <summary>
        /// Validates that the user viewing report is one of Group, Region or Affiliate
        /// </summary>
        /// <param name="role"></param>
        /// <param name="message"></param>
        /// <returns>A Boolean value</returns>
        private bool ValidateUserRole(Role role, out string message)
        {
            message = "";
            if (!(role == Role.Group || role == Role.Regional || role == Role.Affiliate))
            {
                message = "You're not authorized to view reports";
                return false;
            }
            return true;
        }
        #endregion
    }
}

