﻿using AutoMapper;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    //[Authorize(Roles = "Affiliate,Region,Group")]
    [Authorize]
    public class IncidentCategoryController : ControllerBase
    {
        private readonly ICaseRepo _caseRepo;
        private readonly IIncidentCategoryRepo _incidentCategoryRepo;
        private readonly IMapper _mapper;
        private readonly IUserRepo _userRepo;
        private readonly IAuthRepo _authRepo;
        private readonly ILogger<IncidentCategoryController> _logger;
        private readonly IRepo _repo;

        public IncidentCategoryController(IRepo repo, ICaseRepo caseRepo, IIncidentCategoryRepo incidentCategoryRepo, IMapper mapper, IUserRepo userRepo, IAuthRepo authRepo, ILogger<IncidentCategoryController> logger)
        {
            _incidentCategoryRepo = incidentCategoryRepo;
            _mapper = mapper;
            _userRepo = userRepo;
            _authRepo = authRepo;
            _logger = logger;
            _caseRepo = caseRepo;
            _repo = repo;
        }

        [HttpPost]
        public async Task<IActionResult> CreateIncidentCategory([FromBody] CreateIncidentCategoryDTO createIncidentCategoryDto)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to create an IncidentCategory" });
                }
                if (createIncidentCategoryDto == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "Incident Category cannot be null" });
                }
                if (_incidentCategoryRepo.IncidentCategoryExists(createIncidentCategoryDto.IncidentCategory))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = "Incident Category Already Exists!" });
                }
                var incidentCategoryObj = new IncidentCategory();

                incidentCategoryObj.IncidentCategoryName = createIncidentCategoryDto.IncidentCategory;
                incidentCategoryObj.DateAdded = DateTime.Now.Date;
                incidentCategoryObj.AddedBy_Affiliate = userDetails.Affiliate;
                incidentCategoryObj.AddedBy_Email = userDetails.EmailAddress;
                incidentCategoryObj.AddedBy_Name = userDetails.FullName;
                incidentCategoryObj.AddedBy_Role = userDetails.UserRole;
                if (! await _incidentCategoryRepo.CreateIncidentCategory(incidentCategoryObj))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something wrong while creating Incident Category" });
                }
                if (!string.IsNullOrWhiteSpace(createIncidentCategoryDto.IncidentSubCategories))
                {
                    string[] stringSeparators = new string[] { "," };
                    string[] incidentSubcategoryArray = (createIncidentCategoryDto.IncidentSubCategories).Split(stringSeparators, StringSplitOptions.None);
                    List<string> unSavedSubcategory = new List<string>();
                    //List<string> savedSubcategory = new List<string>();
                    foreach (var subCategory in incidentSubcategoryArray)
                    {
                        if (_incidentCategoryRepo.IncidentSubCategoryExists(subCategory, incidentCategoryObj.Id))
                        {
                            unSavedSubcategory.Add(subCategory);
                        }
                        else
                        {
                            var subCatgoryObj = new IncidentSubCategory();

                            subCatgoryObj.IncidentCategoryId = incidentCategoryObj.Id;
                            subCatgoryObj.IncidentSubcategory_Name = subCategory.Trim();
                            subCatgoryObj.DateAdded = DateTime.Now.Date;
                            subCatgoryObj.AddedBy_Affiliate = userDetails.Affiliate;
                            subCatgoryObj.AddedBy_Email = userDetails.EmailAddress;
                            subCatgoryObj.AddedBy_Name = userDetails.FullName;
                            subCatgoryObj.AddedBy_Role = userDetails.UserRole;

                            var saveSubCategory = _incidentCategoryRepo.CreateSubcategory(subCatgoryObj);

                            if (! await saveSubCategory)
                            {
                                unSavedSubcategory.Add(subCategory);
                            }
                        }
                        // savedSubcategory.Add(subCategory);
                    }
                    string unSaved = String.Join(",", unSavedSubcategory);
                    //string saved = String.Join(",", savedSubcategory);
                    if (unSavedSubcategory.Count() != 0)
                    {
                        await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_INCIDENTCATEGORY,
                       $"Created new IncidentCategory: '{createIncidentCategoryDto.IncidentCategory}' and Subcategories '{createIncidentCategoryDto.IncidentSubCategories}'");
                        return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Incident Category created successfully but '{unSaved}' subcategories was/were not created successfully." });
                    }
                    await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_INCIDENTCATEGORY,
                       $"Created new IncidentCategory: '{createIncidentCategoryDto.IncidentCategory}' and Subcategories '{createIncidentCategoryDto.IncidentSubCategories}'");
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Incident Category and Subcategories created successfully" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_INCIDENTCATEGORY,
                       $"Created new IncidentCategory: '{createIncidentCategoryDto.IncidentCategory}' and Subcategories '{createIncidentCategoryDto.IncidentSubCategories}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Incident Category created successfully" });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Creating Incident Category for: {createIncidentCategoryDto.IncidentCategory}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }
        }
        [HttpGet]
        public async Task<IActionResult> GetIncidentCategories()
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole == Role.UserManager)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to retrieve Incident Categories" });
                }
                var objList = _incidentCategoryRepo.GetIncidentCategories();
                if (objList.Count == 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful retrieval of Incident Categories.", output = objList });
                }
                var objDto = new List<GetIncidentCategoriesDTO>();
                foreach (var obj in objList)
                {
                    var dtx = _mapper.Map<GetIncidentCategoriesDTO>(obj);
                    objDto.Add(dtx);
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_INCIDENTCATEGORIES, $"Retrieved all IncidentCategories");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful Incident Categories retrieval", output = objDto });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Getting Incident Categories");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateIncidentCategory(int id, [FromBody] UpdateIncidentCategoryDTO updateIncidentCategoryDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to access this module" });
                }
                else if (string.IsNullOrEmpty(updateIncidentCategoryDTO.IncidentCategory))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"IncidentCategory to be updated is empty" });
                }
                var entity = _incidentCategoryRepo.GetIncidentCategoryById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"IncidentCategory does not exist" });
                }
                var previousName = entity.IncidentCategoryName;
                if (! await _incidentCategoryRepo.UpdateIncidentCategory(userDetails, entity, updateIncidentCategoryDTO.IncidentCategory))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when updating the IncidentCategory for Id: {entity.Id}" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_INCIDENTCATEGORY, $"Changed Incident Category Name - from  '{previousName}' to '{updateIncidentCategoryDTO.IncidentCategory}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"IncidentCategory Updated Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Updating Incident Category with id - {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }
        [HttpPost("{id}")]
        public async Task<IActionResult> DeleteIncidentCategory(int id)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to IncidentCategory Module" });
                }
                var entity = _incidentCategoryRepo.GetIncidentCategoryById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"IncidentCategory does not exist" });
                }
                else if (_caseRepo.GetCaseCountByIncidentCategory(entity.IncidentCategoryName) != 0)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = $"The IncidentCategory: '{entity.IncidentCategoryName}' cannot be deleted because it has case(s) assigned to it" });
                }

                if (! await _incidentCategoryRepo.DeleteIncidentCategory(userDetails, entity))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when Deleting the IncidentCategory for ID: {entity.Id}" });
                }
                var subCategories = _incidentCategoryRepo.GetIncidentSubCategoriesByIncidentCategoryId(entity.Id);
                if (subCategories.Count > 0)
                {
                    foreach (var subCategory in subCategories)
                    {
                        if (! await _incidentCategoryRepo.DeleteIncidentSubCategory(userDetails, subCategory))
                        {
                            return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"IncidentCategory deleted successfully but something went wrong when Deleting the '{subCategory}' Subcategory for ID: {entity.Id}" });
                        }
                        //var subCategories = _incidentCategoryRepo.DeleteIncidentSubCategory(userDetails,subCategory);
                    }
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.DELETED_INCIDENTCATEGORY, $"Deleted IncidentCategory '{entity.IncidentCategoryName}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Incident Category Deleted Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Deleting Incident Category with Id- {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateIncidentSubCategory(int id, [FromBody] UpdateIncidentSubCategoryDTO updateIncidentSubCategoryDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to access this module" });
                }
                else if (string.IsNullOrWhiteSpace(updateIncidentSubCategoryDTO.IncidentSubCategory))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"IncidentSubCategory to be updated is empty" });
                }
                var entity = _incidentCategoryRepo.GetIncidentSubCategoryById(id, updateIncidentSubCategoryDTO.IncidentCategoryId);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"IncidentSubCategory does not exist" });
                }
                var previousName = entity.IncidentSubcategory_Name;
                if (! await _incidentCategoryRepo.UpdateSubIncidentCategory(userDetails, entity, updateIncidentSubCategoryDTO.IncidentSubCategory))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when updating the IncidentSubCategory for Id: {entity.Id}" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_INCIDENTSUBCATEGORY, $"Changed Incident SubCategory Name - from  '{previousName}' to '{updateIncidentSubCategoryDTO.IncidentSubCategory}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"IncidentSubCategory Updated Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Updating IncidentSubCsategory with Id- {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> DeleteIncidentSubCategory(int id)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to access the IncidentSubCategory Module" });
                }
                var entity = _incidentCategoryRepo.GetIncidentSubCategoryById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"IncidentCategory does not exist" });
                }
                else if (_caseRepo.GetCaseCountByIncidentSubCategory(entity.IncidentSubcategory_Name) != 0)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = $"The IncidentSubcategory: '{entity.IncidentSubcategory_Name}' cannot be deleted because it has case(s) assigned to it" });
                }
                if (! await _incidentCategoryRepo.DeleteIncidentSubCategory(entity))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when Deleting the IncidentSubSCategory for ID: {entity.Id}" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.DELETED_INCIDENTSUBCATEGORY, $"Deleted IncidentSubcategory '{entity.IncidentSubcategory_Name}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"IncidentSubCategory Deleted Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Deleting IncidentSubCategory with Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateIncidentSubCategory([FromBody] CreateIncidentSubCategoryDTO createIncidentSubCategoryDto)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to create an IncidentSubCategory" });
                }
                if (createIncidentSubCategoryDto == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "Incident SubCategory cannot be null" });
                }
                if (_incidentCategoryRepo.IncidentSubCategoryExists(createIncidentSubCategoryDto.IncidentSubCategory, createIncidentSubCategoryDto.IncidentCategoryId))
                {
                    return StatusCode(StatusCodes.Status409Conflict, new { success = false, message = "Incident SubCategory Already Exists!" });
                }
                var incidentSubCategoryObj = new IncidentSubCategory();
                incidentSubCategoryObj.IncidentSubcategory_Name = createIncidentSubCategoryDto.IncidentSubCategory;
                incidentSubCategoryObj.IncidentCategoryId = createIncidentSubCategoryDto.IncidentCategoryId;
                incidentSubCategoryObj.DateAdded = DateTime.Now.Date;
                incidentSubCategoryObj.AddedBy_Affiliate = userDetails.Affiliate;
                incidentSubCategoryObj.AddedBy_Email = userDetails.EmailAddress;
                incidentSubCategoryObj.AddedBy_Name = userDetails.FullName;
                incidentSubCategoryObj.AddedBy_Role = userDetails.UserRole;
                if (! await _incidentCategoryRepo.CreateSubcategory(incidentSubCategoryObj))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something wrong while creating Incident SubCategory" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_INCIDENTSUBCATEGORY,
                  $"Created new Incident SubCategory: '{createIncidentSubCategoryDto.IncidentSubCategory}' with Id '{incidentSubCategoryObj.Id}'");

                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Subcategory '{createIncidentSubCategoryDto.IncidentSubCategory}' created successfully" });

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Creating IncidentSubCategory");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }
        }
    }
}
