﻿//using CaseManagementAPI.Data.Dtos;
//using CaseManagementAPI.Enums;
//using CaseManagementAPI.Repository.IRepository;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace CaseManagementAPI.Controllers
//{
//    [Route("api/[controller]/[action]")]
//    [ApiController]
//    [Authorize]
//    public class ReportController : ControllerBase
//    {
//        private readonly ILogger<ReportController> _logger;
//        private readonly IRepo _repo;
//        private readonly ICaseRepo _caseRepo;
//        private readonly IUserRepo _userRepo;
//        private readonly IAuthRepo _authRepo;
//        private readonly IReportRepo _reportRepo;

//        public ReportController(IRepo repo, ICaseRepo caseRepo, IReportRepo reportRepo, IUserRepo userRepo, IAuthRepo authRepo, ILogger<ReportController> logger)
//        {
//            _logger = logger;
//            _userRepo = userRepo;
//            _authRepo = authRepo;
//            _caseRepo = caseRepo;
//            _repo = repo;
//            _reportRepo = reportRepo;
//        }

//        [HttpPost]
//        public async Task<IActionResult> GetReportByStatus(StatusReportDto statusReportDto)
//        {
//            try
//            {
//                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
//                if (!validateResult.Success)
//                {
//                    return StatusCode(validateResult.StatusCode, new
//                    {
//                        success = validateResult.Success,
//                        message = validateResult.Message
//                    });
//                }
//                var userDetails = validateResult.UserProfile;

//                var objList = _reportRepo.GetReportByStatus(userDetails, statusReportDto);
//                if (objList.Count == 0)
//                {
//                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful retrieval of Cases per Status", output = objList });
//                }
//                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_CASE_STATUS, $"Retrieved all Cases per Status" +
//                    $" {statusReportDto.CaseStatus} for the StartDate {statusReportDto.StartDate.Date} and EndDate: {statusReportDto.EndDate.Date}");

//                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful retrieval of Cases per Status", output = objList });
//            }
//            catch (Exception ex)
//            {
//                _logger.LogError(ex, $"An error occurred while Retrieving Cases per Status");
//                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
//            }
//        }

//        [HttpGet]
//        public async Task<IActionResult> GetReportBySource(StatusReportDto statusReportDto)
//        {
//            try
//            {
//                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
//                if (!validateResult.Success)
//                {
//                    return StatusCode(validateResult.StatusCode, new
//                    {
//                        success = validateResult.Success,
//                        message = validateResult.Message
//                    });
//                }
//                var userDetails = validateResult.UserProfile;

//                var objList = _reportRepo.GetReportBySource(userDetails, statusReportDto);
//                if (objList.Count == 0)
//                {
//                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful Retrieval of Cases per Source", output = objList });
//                }
//                await _repo.AuditLogger(userDetails, ActivityActionType.GET_REPORT_BY_CASE_SOURCE, $"Retrieved all Cases per Source per Status" +
//                    $" {statusReportDto.CaseStatus} for the StartDate {statusReportDto.StartDate.Date} and EndDate: {statusReportDto.EndDate.Date}");

//                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful retrieval of Cases per Source", output = objList });
//            }
//            catch (Exception ex)
//            {
//                _logger.LogError(ex, $"An error occurred while Retrieving Cases per Source");
//                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
//            }
//        }
//    }
//}
