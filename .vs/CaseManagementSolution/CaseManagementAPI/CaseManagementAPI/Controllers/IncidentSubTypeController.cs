﻿using AutoMapper;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class IncidentSubTypeController : ControllerBase
    {
        private readonly ICaseRepo _caseRepo;
        private readonly IIncidentSubTypeRepo _incidentSubTypeRepo;
        private readonly IMapper _mapper;
        private readonly IUserRepo _userRepo;
        private readonly IAuthRepo _authRepo;
        private readonly ILogger<IncidentSubTypeController> _logger;
        private readonly IRepo _repo;

        public IncidentSubTypeController(IRepo repo, ICaseRepo caseRepo, IIncidentSubTypeRepo incidentSubTypeRepo, IMapper mapper, IUserRepo userRepo, IAuthRepo authRepo, ILogger<IncidentSubTypeController> logger)
        {
            _incidentSubTypeRepo = incidentSubTypeRepo;
            _mapper = mapper;
            _userRepo = userRepo;
            _authRepo = authRepo;
            _logger = logger;
            _caseRepo = caseRepo;
            _repo = repo;
        }
        [HttpPost]
        public async Task<IActionResult> CreateIncidentSub1Type([FromBody] CreateIncidentSubTypeDTO createIncidentSubTypeDto)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to create an IncidentSub1Type" });
                }
                if (createIncidentSubTypeDto == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "Incident Sub1Type cannot be null" });
                }
                if (_incidentSubTypeRepo.IncidentSubType1Exists(createIncidentSubTypeDto.IncidentSub1Type, createIncidentSubTypeDto.IncidentSubCategoryId))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"Incident Sub1Type {createIncidentSubTypeDto.IncidentSub1Type} Already Exists!" });
                }
                var incidentSub1TypeObj = new IncidentSub1Type();

                incidentSub1TypeObj.IncidentSubCategoryId = createIncidentSubTypeDto.IncidentSubCategoryId;
                incidentSub1TypeObj.IncidentSub1Type_Name = createIncidentSubTypeDto.IncidentSub1Type;
                incidentSub1TypeObj.DateAdded = DateTime.Now.Date;
                incidentSub1TypeObj.AddedBy_Affiliate = userDetails.Affiliate;
                incidentSub1TypeObj.AddedBy_Email = userDetails.EmailAddress;
                incidentSub1TypeObj.AddedBy_Name = userDetails.FullName;
                incidentSub1TypeObj.AddedBy_Role = userDetails.UserRole;
                if (!await _incidentSubTypeRepo.CreateSub1Type(incidentSub1TypeObj))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something wrong while creating Incident Sub1Type {incidentSub1TypeObj.IncidentSub1Type_Name}" });
                }
                if (!string.IsNullOrWhiteSpace((createIncidentSubTypeDto.IncidentSub2Type)))
                {
                    string[] stringSeparators = new string[] { "," };
                    string[] incidentSub2TypeArray = (createIncidentSubTypeDto.IncidentSub2Type).Split(stringSeparators, StringSplitOptions.None);
                    List<string> unSavedSub2Type = new List<string>();
                    List<string> savedSub2Type = new List<string>();
                    foreach (var incidentSub2Type in incidentSub2TypeArray)
                    {
                        if (_incidentSubTypeRepo.IncidentSubType2Exists(incidentSub2Type, incidentSub1TypeObj.Id))
                        {
                            unSavedSub2Type.Add(incidentSub2Type);
                        }
                        else
                        {
                            var sub2TypeObj = new IncidentSub2Type();

                            sub2TypeObj.IncidentSub1TypeId = incidentSub1TypeObj.Id;
                            sub2TypeObj.IncidentSub2Type_Name = incidentSub2Type.Trim();
                            sub2TypeObj.DateAdded = DateTime.Now.Date;
                            sub2TypeObj.AddedBy_Affiliate = userDetails.Affiliate;
                            sub2TypeObj.AddedBy_Email = userDetails.EmailAddress;
                            sub2TypeObj.AddedBy_Name = userDetails.FullName;
                            sub2TypeObj.AddedBy_Role = userDetails.UserRole;

                            var saveIncidentSub2Type = _incidentSubTypeRepo.CreateIncidentSub2Type(sub2TypeObj);
                            if (!await saveIncidentSub2Type)
                            {
                                unSavedSub2Type.Add(incidentSub2Type);
                            }
                            savedSub2Type.Add(incidentSub2Type);
                        }
                        // savedSubcategory.Add(subCategory);
                    }
                    string unSaved = String.Join(",", unSavedSub2Type);
                    string saved = String.Join(",", savedSub2Type);
                    if (unSavedSub2Type.Count() != 0)
                    {
                        await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_INCIDENTSUB1TYPE,
                        $"Created new IncidentSub1Type: '{createIncidentSubTypeDto.IncidentSub1Type}' and Sub2Types '{saved}'");
                        return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Incident Sub1Type : '{createIncidentSubTypeDto.IncidentSub1Type}' created successfully but '{unSaved}' Sub2Types was/were not created successfully." });
                    }
                    await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_INCIDENTSUB1TYPE,
                       $"Created new IncidentSub1Type: '{createIncidentSubTypeDto.IncidentSub1Type}' and Sub2Types '{createIncidentSubTypeDto.IncidentSub2Type}'");
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Incident Sub1Type : '{createIncidentSubTypeDto.IncidentSub1Type}' and Sub2Type : '{createIncidentSubTypeDto.IncidentSub2Type}' created successfully" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_INCIDENTSUB1TYPE,
                       $"Created new IncidentSub1Type: '{createIncidentSubTypeDto.IncidentSub1Type}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Incident Sub1Type : '{createIncidentSubTypeDto.IncidentSub1Type}' created successfully" });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Creating IncidentSub2Type for: {createIncidentSubTypeDto.IncidentSub1Type}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetIncidentSub1Types()
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole == Role.UserManager)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to retrieve IncidentSub1Type" });
                }
                var objList = _incidentSubTypeRepo.GetIncidentSub1Types();
                if (objList.Count == 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful retrieval of IncidentSub1Types.", output = objList });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.RETRIEVED_INCIDENTSUB1TYPES, $"Retrieved all IncidentSub1Types");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"Successful IncidentSub1Type retrieval", output = objList });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Getting Incident Categories");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateIncidentSub1Type(int id, [FromBody] UpdateIncidentSub1TypeDTO updateIncidentSub1TypeDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to access this module" });
                }
                if (string.IsNullOrWhiteSpace(updateIncidentSub1TypeDTO.IncidentSub1Type))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"IncidentSub1Type to be updated is empty" });
                }
                else if (_incidentSubTypeRepo.IncidentSubType1Exists(updateIncidentSub1TypeDTO.IncidentSub1Type))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"Incident Sub1Type '{updateIncidentSub1TypeDTO.IncidentSub1Type}' Already Exists!" });
                }
                var entity = _incidentSubTypeRepo.GetIncidentSub1TypesById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"IncidentSub1Type does not exist" });
                }
                var previousName = entity.IncidentSub1Type_Name;
                if (!await _incidentSubTypeRepo.UpdateIncidentSub1Type(userDetails, entity, updateIncidentSub1TypeDTO.IncidentSub1Type))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when updating the IncidentSub1Type for Id: {entity.Id}" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_INCIDENTSUB1TYPE, $"Changed IncidentSub1Type Name - from  '{previousName}' to '{updateIncidentSub1TypeDTO.IncidentSub1Type}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"IncidentSub1Type Updated from  from  '{previousName}' to '{updateIncidentSub1TypeDTO.IncidentSub1Type}' Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Updating IncidentSub1Type with id - {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> DeleteIncidentSub1Type(int id)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to IncidentSub1Type Module" });
                }
                var entity = _incidentSubTypeRepo.GetIncidentSub1TypesById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"IncidentSub1Type does not exist" });
                }
                else if (_caseRepo.GetCaseCountByIncidentSub1Type(entity.IncidentSub1Type_Name) != 0)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = $"The IncidentSub1Type: '{entity.IncidentSub1Type_Name}' cannot be deleted because it has case(s) assigned to it" });
                }

                if (!await _incidentSubTypeRepo.DeleteIncidentSub1Type(userDetails, entity))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when Deleting the IncidentSub1Type for ID: {entity.Id}" });
                }
                var sub2Types = _incidentSubTypeRepo.GetIncidentSub2TypesByIncidentSub1TypeId(entity.Id);
                if (sub2Types.Count > 0)
                {
                    foreach (var sub2Type in sub2Types)
                    {
                        if (!await _incidentSubTypeRepo.DeleteIncidentSub2Type(userDetails, sub2Type))
                        {
                            return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"IncidentSub1Type deleted successfully but something went wrong when Deleting the '{sub2Type}' Sub2Type for ID: {entity.Id}" });
                        }
                    }
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.DELETED_INCIDENTSUB1TYPE, $"Deleted IncidentSub1Type '{entity.IncidentSub1Type_Name}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"IncidentSub1Type: '{entity.IncidentSub1Type_Name}' Deleted Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Deleting Incident Category with Id- {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateIncidentSub2Type(int id, [FromBody] UpdateIncidentSub2TypeDTO updateIncidentSub2TypeDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to access this module" });
                }
                else if (string.IsNullOrEmpty(updateIncidentSub2TypeDTO.IncidentSub2Type))
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { success = false, message = $"IncidentSub2Type to be updated is empty" });
                }
                //var entity = _incidentSubTypeRepo.GetIncidentSub2TypeById(id, updateIncidentSub2TypeDTO.IncidentSub1TypeId);
                var entity = _incidentSubTypeRepo.GetIncidentSub2TypeById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"IncidentSub2Type does not exist" });
                }
                var previousName = entity.IncidentSub2Type_Name;
                if (!await _incidentSubTypeRepo.UpdateIncidentSub2Type(userDetails, entity, updateIncidentSub2TypeDTO.IncidentSub2Type))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when updating the IncidentSub2Type for Id: {entity.Id}" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.UPDATED_INCIDENTSUB2TYPE, $"Changed IncidentSub2Type Name - from  '{previousName}' to '{updateIncidentSub2TypeDTO.IncidentSub2Type}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"IncidentSub2Type Updated Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Updating IncidentSub2Type with Id- {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> DeleteIncidentSub2Type(int id)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to access the IncidentSub2Type Module" });
                }
                var entity = _incidentSubTypeRepo.GetIncidentSub2TypeById(id);
                if (entity == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = $"IncidentSub2Type does not exist" });
                }
                else if (_caseRepo.GetCaseCountByIncidentSub2Type(entity.IncidentSub2Type_Name) != 0)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = $"The IncidentSub2Type: '{entity.IncidentSub2Type_Name}' cannot be deleted because it has case(s) assigned to it" });
                }
                if (!await _incidentSubTypeRepo.DeleteIncidentSub2Type(entity))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something went wrong when Deleting the IncidentSub2Type for ID: {entity.Id}" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.DELETED_INCIDENTSUB2TYPE, $"Deleted IncidentSub2Type '{entity.IncidentSub2Type_Name}'");
                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"IncidentSub2Type  '{entity.IncidentSub2Type_Name}' Deleted Successfully..." });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Deleting IncidentSub2Type with Id- {id}");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateIncidentSub2Type([FromBody] CreateIncidentSub2TypeDTO createIncidentSub2TypeDTO)
        {
            try
            {
                var validateResult = _authRepo.ValidateUserAuthorization(HttpContext.User);
                if (!validateResult.Success)
                {
                    return StatusCode(validateResult.StatusCode, new
                    {
                        success = validateResult.Success,
                        message = validateResult.Message
                    });
                }
                var userDetails = validateResult.UserProfile;

                if (userDetails.UserRole != Role.Group)
                {
                    return StatusCode(StatusCodes.Status403Forbidden, new { success = false, message = "You are not authorized to create an IncidentSub2Type" });
                }
                if (createIncidentSub2TypeDTO == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "IncidentSub2Type cannot be null" });
                }
                if (string.IsNullOrWhiteSpace(createIncidentSub2TypeDTO.IncidentSub2Type))
                {
                    return StatusCode(StatusCodes.Status404NotFound, new { success = false, message = "IncidentSub2Type cannot be empty" });
                }
                if (_incidentSubTypeRepo.IncidentSub2TypeExists(createIncidentSub2TypeDTO.IncidentSub2Type, createIncidentSub2TypeDTO.IncidentSub1TypeId))
                {
                    return StatusCode(StatusCodes.Status409Conflict, new { success = false, message = "IncidentSub2Type Already Exists!" });
                }
                var incidentSub2TypeObj = new IncidentSub2Type();
                incidentSub2TypeObj.IncidentSub2Type_Name = createIncidentSub2TypeDTO.IncidentSub2Type;
                incidentSub2TypeObj.IncidentSub1TypeId = createIncidentSub2TypeDTO.IncidentSub1TypeId;
                incidentSub2TypeObj.DateAdded = DateTime.Now.Date;
                incidentSub2TypeObj.AddedBy_Affiliate = userDetails.Affiliate;
                incidentSub2TypeObj.AddedBy_Email = userDetails.EmailAddress;
                incidentSub2TypeObj.AddedBy_Name = userDetails.FullName;
                incidentSub2TypeObj.AddedBy_Role = userDetails.UserRole;
                if (!await _incidentSubTypeRepo.CreateIncidentSub2Type(incidentSub2TypeObj))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Something wrong while creating IncidentSub2Type" });
                }
                await _repo.AuditLogger(userDetails, ActivityActionType.CREATED_INCIDENTSUB2TYPE,
                  $"Created new Source: '{createIncidentSub2TypeDTO.IncidentSub2Type}' with Id '{incidentSub2TypeObj.Id}'");

                return StatusCode(StatusCodes.Status200OK, new { success = true, message = $"IncidentSub2Type: '{createIncidentSub2TypeDTO.IncidentSub2Type}' created successfully" });

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"An error occurred while Creating IncidentSub2Type");
                return StatusCode(StatusCodes.Status500InternalServerError, new { success = false, message = $"Error: {ex.Message}" });

            }

        }

    }
}
