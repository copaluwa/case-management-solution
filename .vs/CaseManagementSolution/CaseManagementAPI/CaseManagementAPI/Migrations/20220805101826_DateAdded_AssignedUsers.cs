﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class DateAdded_AssignedUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AssignedUsers_AddedByEmail",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AssignedUsers_AddedByName",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AssignedUsers_DateAdded",
                table: "Cases",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignedUsers_AddedByEmail",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "AssignedUsers_AddedByName",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "AssignedUsers_DateAdded",
                table: "Cases");
        }
    }
}
