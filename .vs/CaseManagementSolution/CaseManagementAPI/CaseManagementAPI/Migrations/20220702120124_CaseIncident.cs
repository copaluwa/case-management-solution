﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace CaseManagementAPI.Migrations
{
    public partial class CaseIncident : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IncidentCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IncidentCategoryName = table.Column<string>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentCategories", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
