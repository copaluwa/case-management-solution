﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class NewMigration_ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CaseNo = table.Column<string>(nullable: true),
                    WhistleblowerNo = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: false),
                    Affiliate = table.Column<string>(nullable: true),
                    AffiliateCode = table.Column<string>(nullable: true),
                    Quarter = table.Column<string>(nullable: false),
                    Month = table.Column<string>(nullable: false),
                    Source = table.Column<string>(nullable: false),
                    Year = table.Column<string>(nullable: false),
                    DateOfIncident = table.Column<DateTime>(nullable: false),
                    DateReported = table.Column<DateTime>(nullable: false),
                    DIR = table.Column<string>(nullable: true),
                    DateFinalised = table.Column<DateTime>(nullable: false),
                    DRF = table.Column<string>(nullable: true),
                    GrossOnSlaught = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    AmountRefuted = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    AmountRecovered = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    NetLoss = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    Currency = table.Column<string>(maxLength: 1, nullable: false),
                    UsdEquivalent = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    IncidentCategory = table.Column<string>(nullable: false),
                    IncidentSubCategory = table.Column<string>(nullable: false),
                    CaseOutcome = table.Column<string>(nullable: true),
                    IncidentSubtype1 = table.Column<string>(nullable: true),
                    IncidentSubtype2 = table.Column<string>(nullable: true),
                    StaffInvolvement = table.Column<string>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    DescriptionOfIncident = table.Column<string>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Region = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    DateClosed = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cases", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserAudits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmailAddress = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(nullable: false),
                    UserRole = table.Column<int>(nullable: false),
                    DisplayName = table.Column<string>(nullable: false),
                    Department = table.Column<string>(nullable: true),
                    AffiliateCode = table.Column<string>(nullable: true),
                    Affiliate = table.Column<string>(maxLength: 4, nullable: false),
                    Region = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAudits", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserProfile",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmailAddress = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    UserRole = table.Column<int>(nullable: false),
                    Affiliate = table.Column<string>(nullable: false),
                    FullName = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfile", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CaseNotes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CaseId = table.Column<int>(nullable: false),
                    CaseNo = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy = table.Column<string>(nullable: true),
                    AddedBy_Email = table.Column<string>(nullable: true),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiededBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CaseNotes_Cases_CaseId",
                        column: x => x.CaseId,
                        principalTable: "Cases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CaseNotes_CaseId",
                table: "CaseNotes",
                column: "CaseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CaseNotes");

            migrationBuilder.DropTable(
                name: "UserAudits");

            migrationBuilder.DropTable(
                name: "UserProfile");

            migrationBuilder.DropTable(
                name: "Cases");
        }
    }
}
