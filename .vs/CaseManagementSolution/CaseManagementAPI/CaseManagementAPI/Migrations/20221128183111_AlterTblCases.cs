﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class AlterTblCases : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IncidentSubtype1",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "IncidentSubtype2",
                table: "Cases");

            migrationBuilder.AddColumn<string>(
                name: "IncidentSub1Type",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IncidentSub2Type",
                table: "Cases",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IncidentSub1Type",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "IncidentSub2Type",
                table: "Cases");

            migrationBuilder.AddColumn<string>(
                name: "IncidentSubtype1",
                table: "Cases",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IncidentSubtype2",
                table: "Cases",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
