﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class ChangedAuditLogData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Event",
                table: "AuditLogs");

            migrationBuilder.AddColumn<string>(
                name: "ActionBy_EmailAddress",
                table: "AuditLogs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ActionBy_Name",
                table: "AuditLogs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IpAddress2",
                table: "AuditLogs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PcName",
                table: "AuditLogs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActionBy_EmailAddress",
                table: "AuditLogs");

            migrationBuilder.DropColumn(
                name: "ActionBy_Name",
                table: "AuditLogs");

            migrationBuilder.DropColumn(
                name: "IpAddress2",
                table: "AuditLogs");

            migrationBuilder.DropColumn(
                name: "PcName",
                table: "AuditLogs");

            migrationBuilder.AddColumn<string>(
                name: "Event",
                table: "AuditLogs",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
