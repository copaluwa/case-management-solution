﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class AlteredTblCase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IncidentSub1Types",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IncidentSubCategoryId = table.Column<int>(nullable: false),
                    IncidentSub1Type_Name = table.Column<string>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSub1Types", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncidentSub1Types_IncidentSubCategories_IncidentSubCategoryId",
                        column: x => x.IncidentSubCategoryId,
                        principalTable: "IncidentSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IncidentSub2Types",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IncidentSub1TypeId = table.Column<int>(nullable: false),
                    IncidentSub2Type_Name = table.Column<string>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSub2Types", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncidentSub2Types_IncidentSub1Types_IncidentSub1TypeId",
                        column: x => x.IncidentSub1TypeId,
                        principalTable: "IncidentSub1Types",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSub1Types_IncidentSubCategoryId",
                table: "IncidentSub1Types",
                column: "IncidentSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSub2Types_IncidentSub1TypeId",
                table: "IncidentSub2Types",
                column: "IncidentSub1TypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IncidentSub2Types");

            migrationBuilder.DropTable(
                name: "IncidentSub1Types");
        }
    }
}
