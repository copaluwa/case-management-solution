﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class CaseIncidentForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IncidentCategoryId",
                table: "IncidentSubCategories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSubCategories_IncidentCategoryId",
                table: "IncidentSubCategories",
                column: "IncidentCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_IncidentSubCategories_IncidentCategories_IncidentCategoryId",
                table: "IncidentSubCategories",
                column: "IncidentCategoryId",
                principalTable: "IncidentCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IncidentSubCategories_IncidentCategories_IncidentCategoryId",
                table: "IncidentSubCategories");

            migrationBuilder.DropIndex(
                name: "IX_IncidentSubCategories_IncidentCategoryId",
                table: "IncidentSubCategories");

            migrationBuilder.DropColumn(
                name: "IncidentCategoryId",
                table: "IncidentSubCategories");
        }
    }
}
