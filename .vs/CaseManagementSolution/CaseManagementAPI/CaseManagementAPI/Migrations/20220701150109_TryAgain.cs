﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class TryAgain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "IncidentSubtype2");

            //migrationBuilder.DropTable(
            //    name: "IncidentSubtype1");

            //migrationBuilder.DropTable(
            //    name: "IncidentSubCategories");

            migrationBuilder.CreateTable(
                name: "Regions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Region_Name = table.Column<string>(nullable: true),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Affiliates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Affiliate_Name = table.Column<string>(nullable: true),
                    AffiliateCode = table.Column<string>(nullable: true),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false),
                    RegionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Affiliates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Affiliates_Regions_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Affiliates_RegionId",
                table: "Affiliates",
                column: "RegionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Affiliates");

            migrationBuilder.DropTable(
                name: "Regions");

            migrationBuilder.CreateTable(
                name: "IncidentSubCategories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AddedBy_Affiliate = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AddedBy_Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AddedBy_Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AddedBy_Role = table.Column<int>(type: "int", nullable: false),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateDeleted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy_Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Role = table.Column<int>(type: "int", nullable: false),
                    IncidentCategoryId = table.Column<int>(type: "int", nullable: true),
                    IncidentCatgory_Id = table.Column<int>(type: "int", nullable: false),
                    IncidentSubcategory_Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    ModifiedBy_Affiliate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedBy_Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedBy_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedBy_Role = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSubCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncidentSubCategories_IncidentCategories_IncidentCategoryId",
                        column: x => x.IncidentCategoryId,
                        principalTable: "IncidentCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IncidentSubtype1",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AddedBy_Affiliate = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AddedBy_Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AddedBy_Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    AddedBy_Role = table.Column<int>(type: "int", nullable: false),
                    DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateDeleted = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateModified = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy_Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Role = table.Column<int>(type: "int", nullable: false),
                    IncidentSubCategoryId = table.Column<int>(type: "int", nullable: true),
                    IncidentSubCatgory_Id = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    ModifiedBy_Affiliate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedBy_Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedBy_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedBy_Role = table.Column<int>(type: "int", nullable: false),
                    Subtype1_Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSubtype1", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncidentSubtype1_IncidentSubCategories_IncidentSubCategoryId",
                        column: x => x.IncidentSubCategoryId,
                        principalTable: "IncidentSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.CreateTable(
            //    name: "IncidentSubtype2",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(type: "int", nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        AddedBy_Affiliate = table.Column<string>(type: "nvarchar(max)", nullable: false),
            //        AddedBy_Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
            //        AddedBy_Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
            //        AddedBy_Role = table.Column<int>(type: "int", nullable: false),
            //        DateAdded = table.Column<DateTime>(type: "datetime2", nullable: false),
            //        DateDeleted = table.Column<DateTime>(type: "datetime2", nullable: false),
            //        DateModified = table.Column<DateTime>(type: "datetime2", nullable: false),
            //        DeletedBy_Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //        DeletedBy_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //        DeletedBy_Role = table.Column<int>(type: "int", nullable: false),
            //        IncidentSubCatgory_Id = table.Column<int>(type: "int", nullable: false),
            //        IncidentSubtype1Id = table.Column<int>(type: "int", nullable: true),
            //        IsDeleted = table.Column<bool>(type: "bit", nullable: false),
            //        ModifiedBy_Affiliate = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //        ModifiedBy_Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //        ModifiedBy_Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //        ModifiedBy_Role = table.Column<int>(type: "int", nullable: false),
            //        Subtype2_Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
            //    },
                //constraints: table =>
                //{
                //    table.PrimaryKey("PK_IncidentSubtype2", x => x.Id);
                //    table.ForeignKey(
                //        name: "FK_IncidentSubtype2_IncidentSubtype1_IncidentSubtype1Id",
                //        column: x => x.IncidentSubtype1Id,
                //        principalTable: "IncidentSubtype1",
                //        principalColumn: "Id",
                //        onDelete: ReferentialAction.Restrict);
                //});

            //migrationBuilder.CreateIndex(
            //    name: "IX_IncidentSubCategories_IncidentCategoryId",
            //    table: "IncidentSubCategories",
            //    column: "IncidentCategoryId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_IncidentSubtype1_IncidentSubCategoryId",
            //    table: "IncidentSubtype1",
            //    column: "IncidentSubCategoryId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_IncidentSubtype2_IncidentSubtype1Id",
            //    table: "IncidentSubtype2",
            //    column: "IncidentSubtype1Id");
        }
    }
}
