﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class IncidentCategory2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IncidentSubtypes1",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Subtype1_Name = table.Column<string>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false),
                    IncidentSubCategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSubtypes1", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncidentSubtypes1_IncidentSubCategories_IncidentSubCategoryId",
                        column: x => x.IncidentSubCategoryId,
                        principalTable: "IncidentSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IncidentSubtypes2",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IncidentSubCatgoryId = table.Column<int>(nullable: false),
                    Subtype2_Name = table.Column<string>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false),
                    IncidentSubCategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSubtypes2", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncidentSubtypes2_IncidentSubCategories_IncidentSubCategoryId",
                        column: x => x.IncidentSubCategoryId,
                        principalTable: "IncidentSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSubtypes1_IncidentSubCategoryId",
                table: "IncidentSubtypes1",
                column: "IncidentSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSubtypes2_IncidentSubCategoryId",
                table: "IncidentSubtypes2",
                column: "IncidentSubCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IncidentSubtypes1");

            migrationBuilder.DropTable(
                name: "IncidentSubtypes2");
        }
    }
}
