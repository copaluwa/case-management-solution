﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class Assignment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Affiliates_Regions_RegionId",
                table: "Affiliates");

            migrationBuilder.DropColumn(
                name: "AmountRefuted",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PreviouslyAssignedTo",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ReassignedBy",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "AffiliateId",
                table: "Affiliates");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateReassigned",
                table: "Cases",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "PreviouslyAssignedTo_Email",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReassignedBy_Affiliate",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReassignedBy_Email",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReassignedBy_Name",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReassignedBy_Region",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReassignedBy_Role",
                table: "Cases",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "RefutedOrSubstantiated",
                table: "Cases",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RegionId",
                table: "Affiliates",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Affiliates_Regions_RegionId",
                table: "Affiliates",
                column: "RegionId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Affiliates_Regions_RegionId",
                table: "Affiliates");

            migrationBuilder.DropColumn(
                name: "DateReassigned",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PreviouslyAssignedTo_Email",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ReassignedBy_Affiliate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ReassignedBy_Email",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ReassignedBy_Name",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ReassignedBy_Region",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ReassignedBy_Role",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "RefutedOrSubstantiated",
                table: "Cases");

            migrationBuilder.AddColumn<decimal>(
                name: "AmountRefuted",
                table: "Cases",
                type: "decimal(18,5)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "PreviouslyAssignedTo",
                table: "Cases",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReassignedBy",
                table: "Cases",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RegionId",
                table: "Affiliates",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "AffiliateId",
                table: "Affiliates",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Affiliates_Regions_RegionId",
                table: "Affiliates",
                column: "RegionId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
