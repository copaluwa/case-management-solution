﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class SourceDetails_DeletedBy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateDeleted",
                table: "CaseSources",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Email",
                table: "CaseSources",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Name",
                table: "CaseSources",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeletedBy_Role",
                table: "CaseSources",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateDeleted",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Email",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Name",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Role",
                table: "CaseSources");
        }
    }
}
