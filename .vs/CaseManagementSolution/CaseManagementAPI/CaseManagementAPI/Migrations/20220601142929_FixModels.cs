﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class FixModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Department",
                table: "UserAudits");

            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "UserAudits");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "CaseNotes");

            migrationBuilder.DropColumn(
                name: "ModifiededBy_Email",
                table: "CaseNotes");

            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "UserAudits",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "CaseNotes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy_Affiliate",
                table: "CaseNotes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy_Email",
                table: "CaseNotes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy_Name",
                table: "CaseNotes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "UserAudits");

            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "CaseNotes");

            migrationBuilder.DropColumn(
                name: "ModifiedBy_Affiliate",
                table: "CaseNotes");

            migrationBuilder.DropColumn(
                name: "ModifiedBy_Email",
                table: "CaseNotes");

            migrationBuilder.DropColumn(
                name: "ModifiedBy_Name",
                table: "CaseNotes");

            migrationBuilder.AddColumn<string>(
                name: "Department",
                table: "UserAudits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "UserAudits",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "CaseNotes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiededBy_Email",
                table: "CaseNotes",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
