﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class AddedIncidentCategoryID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IncidentSubCatgoryId",
                table: "IncidentSubtypes1",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IncidentCatgoryId",
                table: "IncidentSubCategories",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AffiliateId",
                table: "Affiliates",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IncidentSubCatgoryId",
                table: "IncidentSubtypes1");

            migrationBuilder.DropColumn(
                name: "IncidentCatgoryId",
                table: "IncidentSubCategories");

            migrationBuilder.DropColumn(
                name: "AffiliateId",
                table: "Affiliates");
        }
    }
}
