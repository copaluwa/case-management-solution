﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class SourceAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IncidentSubCategories");

            migrationBuilder.DropTable(
                name: "IncidentSubtypes2");

            migrationBuilder.DropTable(
                name: "IncidentCategories");

            migrationBuilder.DropTable(
                name: "IncidentSubtypes1");

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "UserProfile",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsReassigned",
                table: "Cases",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PreviouslyAssignedTo",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReassignedBy",
                table: "Cases",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CaseSources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Source = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseSources", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CaseSources");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "UserProfile");

            migrationBuilder.DropColumn(
                name: "IsReassigned",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "PreviouslyAssignedTo",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ReassignedBy",
                table: "Cases");

            migrationBuilder.CreateTable(
                name: "IncidentCategories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IncidentSubtypes1",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Subtype1_Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSubtypes1", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IncidentSubtypes2",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Subtype2_Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSubtypes2", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IncidentSubCategories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IncidentCategoryId = table.Column<int>(type: "int", nullable: true),
                    IncidentSubCategoryId = table.Column<int>(type: "int", nullable: true),
                    IncidentSubtype1Id = table.Column<int>(type: "int", nullable: true),
                    Subcategory_Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSubCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncidentSubCategories_IncidentCategories_IncidentCategoryId",
                        column: x => x.IncidentCategoryId,
                        principalTable: "IncidentCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_IncidentSubCategories_IncidentSubCategories_IncidentSubCategoryId",
                        column: x => x.IncidentSubCategoryId,
                        principalTable: "IncidentSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_IncidentSubCategories_IncidentSubtypes1_IncidentSubtype1Id",
                        column: x => x.IncidentSubtype1Id,
                        principalTable: "IncidentSubtypes1",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSubCategories_IncidentCategoryId",
                table: "IncidentSubCategories",
                column: "IncidentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSubCategories_IncidentSubCategoryId",
                table: "IncidentSubCategories",
                column: "IncidentSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSubCategories_IncidentSubtype1Id",
                table: "IncidentSubCategories",
                column: "IncidentSubtype1Id");
        }
    }
}
