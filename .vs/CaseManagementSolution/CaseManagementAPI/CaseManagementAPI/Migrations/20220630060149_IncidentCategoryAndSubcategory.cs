﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class IncidentCategoryAndSubcategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IncidentCategories",
                columns: table  => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IncidentCategoryName = table.Column<string>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IncidentSubCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IncidentSubcategory_Name = table.Column<string>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false),
                    IncidentCategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSubCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncidentSubCategories_IncidentCategories_IncidentCategoryId",
                        column: x => x.IncidentCategoryId,
                        principalTable: "IncidentCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IncidentSubtype1",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Subtype1_Name = table.Column<string>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false),
                    IncidentSubCategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSubtype1", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncidentSubtype1_IncidentSubCategories_IncidentSubCategoryId",
                        column: x => x.IncidentSubCategoryId,
                        principalTable: "IncidentSubCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IncidentSubtype2",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Subtype2_Name = table.Column<string>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    AddedBy_Name = table.Column<string>(nullable: false),
                    AddedBy_Affiliate = table.Column<string>(nullable: false),
                    AddedBy_Email = table.Column<string>(nullable: false),
                    AddedBy_Role = table.Column<int>(nullable: false),
                    ModifiedBy_Name = table.Column<string>(nullable: true),
                    ModifiedBy_Affiliate = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    ModifiedBy_Email = table.Column<string>(nullable: true),
                    ModifiedBy_Role = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedBy_Email = table.Column<string>(nullable: true),
                    DeletedBy_Name = table.Column<string>(nullable: true),
                    DeletedBy_Role = table.Column<int>(nullable: false),
                    DateDeleted = table.Column<DateTime>(nullable: false),
                    IncidentSubtype1Id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncidentSubtype2", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncidentSubtype2_IncidentSubtype1_IncidentSubtype1Id",
                        column: x => x.IncidentSubtype1Id,
                        principalTable: "IncidentSubtype1",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSubCategories_IncidentCategoryId",
                table: "IncidentSubCategories",
                column: "IncidentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSubtype1_IncidentSubCategoryId",
                table: "IncidentSubtype1",
                column: "IncidentSubCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_IncidentSubtype2_IncidentSubtype1Id",
                table: "IncidentSubtype2",
                column: "IncidentSubtype1Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IncidentSubtype2");

            migrationBuilder.DropTable(
                name: "IncidentSubtype1");

            migrationBuilder.DropTable(
                name: "IncidentSubCategories");

            migrationBuilder.DropTable(
                name: "IncidentCategories");
        }
    }
}
