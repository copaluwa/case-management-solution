﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class DetailsToAddNote : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddedBy",
                table: "CaseNotes");

            migrationBuilder.AlterColumn<string>(
                name: "Note",
                table: "CaseNotes",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CaseNo",
                table: "CaseNotes",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AddedBy_Email",
                table: "CaseNotes",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AddedBy_Affiliate",
                table: "CaseNotes",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AddedBy_Name",
                table: "CaseNotes",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AddedBy_Region",
                table: "CaseNotes",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddedBy_Affiliate",
                table: "CaseNotes");

            migrationBuilder.DropColumn(
                name: "AddedBy_Name",
                table: "CaseNotes");

            migrationBuilder.DropColumn(
                name: "AddedBy_Region",
                table: "CaseNotes");

            migrationBuilder.AlterColumn<string>(
                name: "Note",
                table: "CaseNotes",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "CaseNo",
                table: "CaseNotes",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "AddedBy_Email",
                table: "CaseNotes",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "AddedBy",
                table: "CaseNotes",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
