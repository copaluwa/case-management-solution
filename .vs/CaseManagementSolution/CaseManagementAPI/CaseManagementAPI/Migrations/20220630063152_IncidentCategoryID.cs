﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class IncidentCategoryID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IncidentSubCatgory_Id",
                table: "IncidentSubtype1",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IncidentCatgory_Id",
                table: "IncidentSubCategories",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IncidentSubCatgory_Id",
                table: "IncidentSubtype1");

            migrationBuilder.DropColumn(
                name: "IncidentCatgory_Id",
                table: "IncidentSubCategories");
        }
    }
}
