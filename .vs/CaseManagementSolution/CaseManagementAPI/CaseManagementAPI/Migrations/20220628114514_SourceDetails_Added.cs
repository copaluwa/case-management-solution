﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class SourceDetails_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Source",
                table: "CaseSources",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AddedBy_Affiliate",
                table: "CaseSources",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AddedBy_Email",
                table: "CaseSources",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AddedBy_Name",
                table: "CaseSources",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AddedBy_Region",
                table: "CaseSources",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "AddedBy_Role",
                table: "CaseSources",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateAdded",
                table: "CaseSources",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateModified",
                table: "CaseSources",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy_Affiliate",
                table: "CaseSources",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy_Email",
                table: "CaseSources",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy_Name",
                table: "CaseSources",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ModifiedBy_Role",
                table: "CaseSources",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddedBy_Affiliate",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "AddedBy_Email",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "AddedBy_Name",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "AddedBy_Region",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "AddedBy_Role",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "DateAdded",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "DateModified",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "ModifiedBy_Affiliate",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "ModifiedBy_Email",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "ModifiedBy_Name",
                table: "CaseSources");

            migrationBuilder.DropColumn(
                name: "ModifiedBy_Role",
                table: "CaseSources");

            migrationBuilder.AlterColumn<string>(
                name: "Source",
                table: "CaseSources",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
