﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class SubtypeForeignkey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IncidentSubCategories_IncidentCategories_IncidentCategoryId",
                table: "IncidentSubCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_IncidentSubtypes1_IncidentSubCategories_IncidentSubCategoryId",
                table: "IncidentSubtypes1");

            migrationBuilder.DropForeignKey(
                name: "FK_IncidentSubtypes2_IncidentSubCategories_IncidentSubCategoryId",
                table: "IncidentSubtypes2");

            //migrationBuilder.DropColumn(
            //    name: "IncidentSubCatgoryId",
            //    table: "IncidentSubtypes2");

            //migrationBuilder.DropColumn(
            //    name: "IncidentSubCatgoryId",
            //    table: "IncidentSubtypes1");

            migrationBuilder.DropColumn(
                name: "IncidentCatgoryId",
                table: "IncidentSubCategories");

            //migrationBuilder.AlterColumn<int>(
            //    name: "IncidentSubCategoryId",
            //    table: "IncidentSubtypes2",
            //    nullable: false,
            //    oldClrType: typeof(int),
            //    oldType: "int",
            //    oldNullable: true);

            //migrationBuilder.AlterColumn<int>(
            //    name: "IncidentSubCategoryId",
            //    table: "IncidentSubtypes1",
            //    nullable: false,
            //    oldClrType: typeof(int),
            //    oldType: "int",
            //    oldNullable: true);

            //migrationBuilder.AlterColumn<int>(
            //    name: "IncidentCategoryId",
            //    table: "IncidentSubCategories",
            //    nullable: false,
            //    oldClrType: typeof(int),
            //    oldType: "int",
            //    oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_IncidentSubCategories_IncidentCategories_IncidentCategoryId",
                table: "IncidentSubCategories",
                column: "IncidentCategoryId",
                principalTable: "IncidentCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_IncidentSubtypes1_IncidentSubCategories_IncidentSubCategoryId",
                table: "IncidentSubtypes1",
                column: "IncidentSubCategoryId",
                principalTable: "IncidentSubCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_IncidentSubtypes2_IncidentSubCategories_IncidentSubCategoryId",
                table: "IncidentSubtypes2",
                column: "IncidentSubCategoryId",
                principalTable: "IncidentSubCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IncidentSubCategories_IncidentCategories_IncidentCategoryId",
                table: "IncidentSubCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_IncidentSubtypes1_IncidentSubCategories_IncidentSubCategoryId",
                table: "IncidentSubtypes1");

            migrationBuilder.DropForeignKey(
                name: "FK_IncidentSubtypes2_IncidentSubCategories_IncidentSubCategoryId",
                table: "IncidentSubtypes2");

            migrationBuilder.AlterColumn<int>(
                name: "IncidentSubCategoryId",
                table: "IncidentSubtypes2",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "IncidentSubCatgoryId",
                table: "IncidentSubtypes2",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "IncidentSubCategoryId",
                table: "IncidentSubtypes1",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "IncidentSubCatgoryId",
                table: "IncidentSubtypes1",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "IncidentCategoryId",
                table: "IncidentSubCategories",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "IncidentCatgoryId",
                table: "IncidentSubCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_IncidentSubCategories_IncidentCategories_IncidentCategoryId",
                table: "IncidentSubCategories",
                column: "IncidentCategoryId",
                principalTable: "IncidentCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_IncidentSubtypes1_IncidentSubCategories_IncidentSubCategoryId",
                table: "IncidentSubtypes1",
                column: "IncidentSubCategoryId",
                principalTable: "IncidentSubCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_IncidentSubtypes2_IncidentSubCategories_IncidentSubCategoryId",
                table: "IncidentSubtypes2",
                column: "IncidentSubCategoryId",
                principalTable: "IncidentSubCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
