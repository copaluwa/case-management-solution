﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class CaseClosure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ClosedBy_Affiliate",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosedBy_Email",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosedBy_Name",
                table: "Cases",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ClosedBy_Region",
                table: "Cases",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClosedBy_Affiliate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ClosedBy_Email",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ClosedBy_Name",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ClosedBy_Region",
                table: "Cases");
        }
    }
}
