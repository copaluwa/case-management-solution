﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class RemovedIncidentSubcategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IncidentSubCatgory_Id",
                table: "IncidentSubtype1");

            migrationBuilder.DropColumn(
                name: "IncidentCatgory_Id",
                table: "IncidentSubCategories");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IncidentSubCatgory_Id",
                table: "IncidentSubtype1",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IncidentCatgory_Id",
                table: "IncidentSubCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
