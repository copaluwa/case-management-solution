﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaseManagementAPI.Migrations
{
    public partial class RemovedClosedby_Role : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignedTo",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "ClosedBy_Role",
                table: "Cases");

            migrationBuilder.AddColumn<string>(
                name: "AssignedTo_Affiliate",
                table: "Cases",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AssignedTo_Email",
                table: "Cases",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AssignedTo_Name",
                table: "Cases",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AssignedTo_Region",
                table: "Cases",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "AssignedTo_Role",
                table: "Cases",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignedTo_Affiliate",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "AssignedTo_Email",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "AssignedTo_Name",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "AssignedTo_Region",
                table: "Cases");

            migrationBuilder.DropColumn(
                name: "AssignedTo_Role",
                table: "Cases");

            migrationBuilder.AddColumn<string>(
                name: "AssignedTo",
                table: "Cases",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ClosedBy_Role",
                table: "Cases",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
