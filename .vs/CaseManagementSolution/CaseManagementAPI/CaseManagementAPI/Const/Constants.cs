﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Const
{
    public class Constants
    {
        public const string SYSTEM_NAME = "SYSTEM";
        public static string[] IGNORED_COLUMNS = new string[] { "Id", "CreatedBy", "CreatedDate" };
        public const string CONTEXT_USER_KEY = "Employee";
        public static readonly string COOKIE_KEY_FOR_GroupUser = "Group User";
        public static readonly string COOKIE_KEY_FOR_AffiliateUser = "Affiliate User";
        public static readonly string COOKIE_KEY_FOR_RegionalUser = "Group User";
    }
}
