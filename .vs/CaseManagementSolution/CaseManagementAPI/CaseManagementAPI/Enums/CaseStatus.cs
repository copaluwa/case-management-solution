﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Enums
{
    public enum CaseStatus
    {
        [Description("Open")]
        Open = 1,

        [Description("Closed")]
        Closed = 2
    }

    public enum StaffInvolvementEnum
    {
        [Description("negligence")]
        negligence = 1,

        [Description("dishonesty")]
        dishonesty,

        [Description("none")]
        none
    }
}
