﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Enums
{
    public enum Role
    {
        [Description("Unknown")]
        Unknown = 0,

        [Description("Group")]
        Group = 1,

        [Description("Regional")]
        Regional = 2,

        [Description("Affiliate")]
        Affiliate = 3,

        [Description("UserManager")]
        UserManager = 4
    }
    
}
