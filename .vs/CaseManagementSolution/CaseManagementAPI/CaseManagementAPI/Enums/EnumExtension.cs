﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace CaseManagementAPI.Enums
{
    public static class EnumExtension
    {
        public static string GetDescription(this Enum GenericEnum)
        {
            Type genericEnumType = GenericEnum.GetType();
            MemberInfo[] memberInfo = genericEnumType.GetMember(GenericEnum.ToString());
            if ((memberInfo != null && memberInfo.Length > 0))
            {
                var _Attribs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if ((_Attribs != null && _Attribs.Count() > 0))
                {
                    return ((DescriptionAttribute)_Attribs.ElementAt(0)).Description;
                }
            }
            return GenericEnum.ToString();
        }

        public static IEnumerable<string> GetStaffInvolvementEnumNames()
        {
            var response = new string[3];
            foreach (int i in Enum.GetValues(typeof(StaffInvolvementEnum)))
                response[i - 1] = ((StaffInvolvementEnum)i).GetDescription().ToLower();
            return response;
        }

        public static IEnumerable<string> GetCaseStatusEnumNames()
        {
            var response = new string[2];
            foreach (int i in Enum.GetValues(typeof(CaseStatus)))
                response[i - 1] = ((CaseStatus)i).GetDescription().ToLower();
            return response;
        }
    }
}

