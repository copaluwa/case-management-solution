﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Enums
{
    public enum ReportCategory
    {
        UNKNOWN = 0,
        [Display(Name = "Status")]
        Status = 1,
        [Display(Name = "Incident Category")]
        Incident_Category = 2,
        [Display(Name = "Incident Type")]
        Incident_Type = 3,
        [Display(Name = "Source")]
        Source = 4,
        [Display(Name = "Quarter")]
        Quarter = 5,
        [Display(Name = "Staff Involvement")]
        Staff_Involvement = 6,
        [Display(Name = "Gross Onslaught")]
        Gross_Onslaught = 7,
        [Display(Name = "Net Loss")]
        Net_Loss = 8,
        [Display(Name = "Cases Reported Per Affiliate")]
        All_Records = 9,
        [Display(Name = "Incident to Report days Per Affiliate")]
        Incident_To_Report_Days_Per_Affiliate = 10,
        [Display(Name = "Incident to Report days Per Source")]
        Incident_To_Report_Days_Per_Source = 11,
        [Display(Name = "Case_Outcome")]
        Case_Outcome = 12,
        //[Display(Name = "Net Loss Per Affiliate P")]
        //Case_Outcome = 12,
    }
}
