﻿using AutoMapper;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.CaseMapper
{
    public class CaseMappings : Profile
    {
        public CaseMappings()
        {
            CreateMap<Case, CaseDTO>().ReverseMap();
            CreateMap<Case, CreateCaseDTO>().ReverseMap();
            CreateMap<UserProfile, UserProfileDTO>().ReverseMap();
            CreateMap<UserProfile, GetAll_UserProfileDTO>().ReverseMap();
            CreateMap<CaseNote, CaseNoteDTO > ().ReverseMap();
            CreateMap<CaseSource, CaseSourceDTO > ().ReverseMap();
            CreateMap<IncidentCategory, CreateIncidentCategoryDTO> ().ReverseMap();
            CreateMap<IncidentCategory, GetIncidentCategoriesDTO> ().ReverseMap();
            CreateMap<Region, GetRegionsDTO > ().ReverseMap();
            CreateMap<Case, SearchCaseDto> ().ReverseMap();
        }
    }
}
