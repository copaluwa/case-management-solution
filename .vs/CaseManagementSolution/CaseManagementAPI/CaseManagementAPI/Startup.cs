using CaseManagementAPI.CaseMapper;
using CaseManagementAPI.Data;
using CaseManagementAPI.Extensions;
using CaseManagementAPI.Repository;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CaseManagementAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
          
            // Adds Microsoft Identity platform (Azure AD B2C) support to protect this Api
            services.AddCors();
            //services.AddControllers().AddJsonOptions(x =>
            //{
            //    x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            //});
            services.AddControllers();
            services.Configure<FormOptions>(options =>
            {
                options.MultipartBodyLengthLimit = Configuration.GetValue<long>("AppSettings:MaxUploadSize");
            });
            services.AddSwaggerService();
            //services.AddHttpContextAccessor();

            //services.AddHttpContextAccessor();
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("CaseManagementConnection"),
                          sqlServerOptionsAction: sqlOptions =>
                          {
                              sqlOptions.EnableRetryOnFailure();
                          });
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<ICaseRepo, CaseRepo>();
            services.AddScoped<IUserRepo, UserRepo>();
            services.AddScoped<ISourceRepo, SourceRepo>();
            services.AddScoped<IAffiliateRepo, AffiliateRepo>();
            services.AddScoped<IRegionRepo, RegionRepo>();
            services.AddScoped<IIncidentCategoryRepo, IncidentCategoryRepo>();
            services.AddScoped<IRepo, Repo>();
            services.AddScoped<IIncidentSubTypeRepo, IncidentSubTypeRepo>();
            services.AddScoped<IReportRepo, ReportRepo>();
            services.AddScoped<IAuthRepo, AuthRepo>();
            services.AddAutoMapper(typeof(CaseMappings));



            //var allowedOrigins = Configuration.GetSection("AppSettings:AllowedOrigins").GetChildren().ToArray().Select(c => c.Value).ToArray();
            //var allowedMethods = Configuration.GetSection("AppSettings:AllowedMethods").GetChildren().ToArray().Select(c => c.Value).ToArray();
            //services.AddCors(options =>
            //{
            //    options.AddDefaultPolicy(builder =>
            //        {
            //            builder.AllowAnyOrigin().AllowAnyHeader(); 
            //        });
            //});


            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //.AddMicrosoftIdentityWebApi(options =>
            //{
            //    Configuration.Bind("AzureAdB2C", options);
            //    options.TokenValidationParameters.NameClaimType = "name";
            //},
            //options => { Configuration.Bind("AzureAdB2C", options); });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddMicrosoftIdentityWebApi(Configuration.GetSection("AzureAdB2C"))
                .EnableTokenAcquisitionToCallDownstreamApi()
                .AddInMemoryTokenCaches();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            var path = Directory.GetCurrentDirectory();
            loggerFactory.AddFile($"{path}\\Logs\\Log.txt", isJson: true, outputTemplate: "=========================> {Timestamp:o} {RequestId,13} [{Level:u3}] <========================={NewLine} {Message} ({EventId:x8}){NewLine}{Exception} {NewLine}==========================================================================={NewLine}");


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwaggerService(Configuration);
            app.UseHttpsRedirection();
            app.UseRouting();
            //app.UseCors();

            app.UseCors(x => x.AllowAnyOrigin()
               .AllowAnyHeader()
               .AllowAnyMethod());
            app.UseAuthentication();

            // Shows UseCors with CorsPolicyBuilder.

            //app.UseCors(builder =>
            //{
            //    builder
            //    .AllowAnyOrigin()
            //    .AllowAnyMethod()
            //    .AllowAnyHeader();
            //});
            app.UseAuthorization();
            app.UseStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
           
        }
    }
}
