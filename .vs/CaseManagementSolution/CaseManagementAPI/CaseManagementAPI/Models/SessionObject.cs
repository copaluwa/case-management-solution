﻿using CaseManagementAPI.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Models
{
    public class SessionObject
    {
            public long Id { get; set; }
            public string Username { get; set; }
            public string UserId { get; set; }
            //public string FirstName { get; set; }
            //public string LastName { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
            public string Affiliate { get; set; }
            public Role UserRole { get; set; }

        public static SessionObject FromUserProfile(UserProfile userProfile)
        {
            return new SessionObject
            {
                Email = userProfile.EmailAddress,
                Username = userProfile.UserName,
                UserId = userProfile.UserId,
                //LastName = employee.LastName,
                //PersonNumber = employee.PersonNumber,
                //Username = userProfile.EmailAddress.Split("@")[0],
                Id = userProfile.Id,
                Affiliate = userProfile.Affiliate,
                //Roles = employee.EmployeeRoles.Select(er => RoleObject.FromRole(er.Role)).ToList(),
                //Roles = new List<RoleObject> { new RoleObject { Id = employee.Role.Id, Name = employee.Role.Name } }
            };
        }




    }
}
