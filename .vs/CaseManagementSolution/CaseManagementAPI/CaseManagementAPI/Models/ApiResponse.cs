﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Models
{
    public class ApiResponse
    {
        //public string AffiliateCode { get; set; }
        //public string RequestId { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        //public string SourceCode { get; set; }
        //public string IpAddress { get; set; }
    }
}
