﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Models
{
    public class HeaderRequest
    {
        public string SourceCode { get; set; }
        public string RequestId { get; set; }
        public string RequestToken { get; set; }
        public string RequestType { get; set; }
        public string AffiliateCode { get; set; }
        public string IpAddress { get; set; }
        public string SourceChannelId { get; set; }
        public bool IsValid { get; set; }
        //public string ErrorMessage { get; set; }
    }

        

}
