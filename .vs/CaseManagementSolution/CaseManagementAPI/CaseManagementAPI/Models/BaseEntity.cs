﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CaseManagementAPI.Models
{
    public class BaseEntity
    {
        [Key]
        public long Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; } = DateTimeOffset.Now;
        //public string UpdatedBy { get; set; }
        //public DateTimeOffset UpdatedDate { get; set; } = DateTimeOffset.Now;

        //Navigation Properties
        [JsonIgnore]
        [ForeignKey("CreatedBy")]
        [NotMapped]
        public virtual UserProfile CreatedByEmployee { get; set; }
        [JsonIgnore]
        [ForeignKey("UpdatedBy")]
        [NotMapped]
        public virtual UserProfile UpdatedByEmployee { get; set; }

        public T Clone<T>() where T : BaseEntity
        {
            return (T)this.MemberwiseClone();
        }
    }
}
