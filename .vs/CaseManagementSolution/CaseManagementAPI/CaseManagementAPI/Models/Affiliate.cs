﻿using CaseManagementAPI.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Models
{
    public class Affiliate
    {
        public int Id { get; set; }
        public string Affiliate_Name { get; set; }
        public int RegionId { get; set; }
        public string AffiliateCode { get; set; }
        [Required]
        public DateTime DateAdded { get; set; }
        [Required]
        public string AddedBy_Name { get; set; }
        [Required]
        public string AddedBy_Affiliate { get; set; }
        [Required]
        public string AddedBy_Email { get; set; }
        [Required]
        public Role AddedBy_Role { get; set; }
        public string ModifiedBy_Name { get; set; }
        public string ModifiedBy_Affiliate { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy_Email { get; set; }
        public Role ModifiedBy_Role { get; set; }
        public bool IsDeleted { get; set; }
        public string DeletedBy_Email { get; set; }
        public string DeletedBy_Name { get; set; }
        public Role DeletedBy_Role { get; set; }
        public DateTime DateDeleted { get; set; }
    }
}
