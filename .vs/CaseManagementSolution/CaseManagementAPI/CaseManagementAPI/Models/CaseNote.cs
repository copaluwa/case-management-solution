﻿using CaseManagementAPI.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Models
{
    public class CaseNote
    {

        public int Id { get; set; }
        [Required]
        public int CaseId { get; set; }
        [Required]
        public string CaseNo { get; set; }
        //public Case Case { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public DateTime DateAdded { get; set; }
        [Required]
        public string AddedBy_Name { get; set; }
        [Required]
        public string AddedBy_Affiliate { get; set; }
        [Required]
        public string AddedBy_Region { get; set; }
        [Required]
        public string AddedBy_Email { get; set; }
        [Required]
        public Role AddedBy_Role { get; set; }
        public string ModifiedBy_Name { get; set; }
        public string ModifiedBy_Affiliate { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy_Email { get; set; }
        public Role ModifiedBy_Role { get; set; }
    }
}
