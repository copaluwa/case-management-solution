﻿using CaseManagementAPI.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Models
{
    public class UserProfile
    {
        public int Id { get; set; }
        [Required]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Email is not valid")]
        public string EmailAddress { get; set; }
       
        public string UserName { get; set; }
        [Required]
        public Role UserRole { get; set; }
        [Required]
        public string Affiliate { get; set; }
        public string FullName { get; set; }
        //public string Department { get; set; }

        //[Required]
        //[MaxLength(4)]
        //public string AffiliateCode { get; set; }
        [Required]
        public string Region { get; set; }
        public string UserId { get; set; }
        [Required]
        public DateTime DateAdded { get; set; }
        [Required]
        public string AddedBy { get; set; }
        [Required]
        public string AddedBy_Email { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedBy_Email { get; set; }
        public bool IsDeleted { get; set; }
    }
}
