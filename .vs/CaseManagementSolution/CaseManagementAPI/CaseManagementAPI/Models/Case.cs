﻿using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Models
{
    public class Case
    {
        public int Id { get; set; }
        public string CaseNo { get; set; }
        public int? WhistleblowerNo { get; set; }

        [Required]
        public string Region { get; set; }
        public string Affiliate { get; set; }
        public string AffiliateCode { get; set; }
        [Required]
        public string Quarter { get; set; }
        [Required]
        public string Month { get; set; }
        [Required]
        public string Source { get; set; }
        [Required]
        public string Year { get; set; }
        [Required]
        public DateTime DateOfIncident { get; set; }
        [Required]
        public DateTime DateReported { get; set; }

        //public string DIR { get; set; }

        public DateTime DateFinalised { get; set; }
        //public string DRF { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,5)")]
        public decimal GrossOnSlaught { get; set; }

        public string RefutedOrSubstantiated { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountRecovered { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal NetLoss { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountPrevented { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountRefuted { get; set; }

        [Required]
        [MaxLength(10)]
        public string Currency { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal UsdEquivalent { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal UsdExchangeRate { get; set; }

        [Required]
        public string IncidentCategory { get; set; }
        [Required]
        public string IncidentSubCategory { get; set; }
        public string IncidentSub1Type { get; set; }
        public string IncidentSub2Type { get; set; }
        public string CaseOutcome { get; set; }
        [Required]
        public string StaffInvolvement { get; set; }
        [Required]
        public CaseStatus Status { get; set; }
        [Required]
        public string DescriptionOfIncident { get; set; }
        //public string Note { get; set; }
        [Required]
        public string AddedBy_Name { get; set; }
        
        [Required]
        public string AssignedTo_Name { get; set; }
        [Required]
        public string AssignedTo_Affiliate { get; set; }
        [Required]
        public string AssignedTo_Region { get; set; }
        [Required]
        public string AssignedTo_Email { get; set; }
        public Role AssignedTo_Role { get; set; }
        [Required]
        public string AddedBy_Affiliate { get; set; }
        [Required]
        public string AddedBy_Region { get; set; }
        [Required]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Email is not valid")]
        public string AddedBy_Email { get; set; }
        [Required]
        public Role AddedBy_Role { get; set; }
        [Required]
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy_Name { get; set; }
        public string ModifiedBy_Email { get; set; }
        public Role ModifiedBy_Role { get; set; }
        public DateTime DateClosed { get; set; }
        public string UserId { get; set; }
        public string CorrectiveAction { get; set; }
        public string ClosedBy_Name { get; set; }
        public string ClosedBy_Affiliate { get; set; }
        public string ClosedBy_Region{ get; set; }
        public string ClosedBy_Email{ get; set; }
        //public Role ClosedBy_Role{ get; set; }
        public ICollection<CaseNote> CaseNotes { get; set; }
        public ICollection<CaseFile> CaseFiles { get; set; }
        public bool IsReassigned { get; set; }
        public string ReassignedBy_Name { get; set; }
        public string ReassignedBy_Affiliate { get; set; }
        public string ReassignedBy_Region { get; set; }
        public string ReassignedBy_Email { get; set; }
        public Role ReassignedBy_Role { get; set; }
        public DateTime DateReassigned { get; set; }
        public string PreviouslyAssignedTo_Email { get; set; }
        public string AssignedUsers { get; set; }
        public string AssignedUsers_Name { get; set; }
        public string AssignedUsers_DateAdded { get; set; }
        public string AssignedUsers_AddedByName { get; set; }
        public string AssignedUsers_AddedByEmail { get; set; }
    }

    public class CaseFile
    {
        public int Id { get; set; }
        public int CaseId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
    }
}
