﻿using CaseManagementAPI.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Models
{
    public class UserAudit
    {
        public int Id { get; set; }
        [Required]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Email is not valid")]
        public string EmailAddress { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public Role UserRole { get; set; }
        [Required]
        public string FullName { get; set; }
        public string AffiliateCode { get; set; }
        [Required]
        [MaxLength(4)]
        public string Affiliate { get; set; }
        //public List<string> Groups { get; set; }
        [Required]
        public string Region { get; set; }
    }
}
