﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CaseManagementAPI.Models
{
    public class AuditLog
    {
        public int Id { get; set; }
        //public string ActionBy { get; set; }
        public string IpAddress { get; set; }
        public string PcName { get; set; }
        public string IpAddress2 { get; set; }
        //public string Event { get; set; }
        public DateTime? DateCreated { get; set; }
        public string ActionType { get; set; }
        public string Description { get; set; }
        public string ActionBy_Name { get; set; }
        public string ActionBy_EmailAddress { get; set; }
    }
}
