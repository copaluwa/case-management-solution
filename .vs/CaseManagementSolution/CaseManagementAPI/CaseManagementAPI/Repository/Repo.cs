﻿using CaseManagementAPI.Data;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository
{
    public class Repo : IRepo
    {
        private readonly ApplicationDbContext _db;
        public Repo(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<bool> Save()
        {
            return await _db.SaveChangesAsync() >= 0 ? true : false;
        }

        public async Task<bool> AuditLogger(UserProfile userProfile, ActivityActionType activityAction, string description)
        {
            string hostName = Dns.GetHostName(); // Retrive the Name of HOST
            // Get the IP
            string userIp = Dns.GetHostEntry(hostName).AddressList[1]?.ToString();
            var userIp2 =  Dns.GetHostEntry(hostName).AddressList[0]?.ToString();
            //var ip = Dns.GetHostByName(hostName).AddressList[0].ToString();
            var newAudit = new AuditLog();
            var dt = DateTime.Now;
            newAudit.ActionBy_Name = userProfile.FullName;
            newAudit.ActionBy_EmailAddress = userProfile.EmailAddress;
            newAudit.ActionType = activityAction.ToString();
            newAudit.DateCreated = (dt.AddSeconds(-dt.Second));
            newAudit.IpAddress = userIp;
            newAudit.IpAddress2 = userIp2;
            newAudit.Description = description;
            newAudit.PcName = hostName;
            _db.AuditLogs.Add(newAudit);

            return await Save();
        }


    }
}
