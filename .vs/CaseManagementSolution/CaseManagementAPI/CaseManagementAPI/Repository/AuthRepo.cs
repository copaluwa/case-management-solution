﻿using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository
{
    public class AuthRepo : IAuthRepo
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserRepo _userRepo;

        public AuthRepo(IHttpContextAccessor httpContextAccessor, IUserRepo userRepo)
        {
            _httpContextAccessor = httpContextAccessor;
            _userRepo = userRepo;
        }

        public HeaderRequest CheckRequestHeader()
        {
            var headerRequest = new HeaderRequest();
           
            headerRequest.SourceCode = _httpContextAccessor.HttpContext.Request.Headers["SourceCode"];
            headerRequest.RequestId = _httpContextAccessor.HttpContext.Request.Headers["RequestId"];
            headerRequest.RequestToken = _httpContextAccessor.HttpContext.Request.Headers["RequestToken"];
            headerRequest.RequestType = _httpContextAccessor.HttpContext.Request.Headers["RequestType"];
            headerRequest.IpAddress = _httpContextAccessor.HttpContext.Request.Headers["IpAddress"];
            headerRequest.SourceChannelId = _httpContextAccessor.HttpContext.Request.Headers["SourceChannelId"];
            headerRequest.AffiliateCode = _httpContextAccessor.HttpContext.Request.Headers["AffiliateCode"];

            bool isNotNull = headerRequest.GetType().GetProperties()
                            .All(p => p.GetValue(headerRequest) != null && (p.GetValue(headerRequest)).ToString() != "");
            if (!isNotNull)
            {
                //headerRequest.ErrorMessage = "One or more Header is Null";
                headerRequest.IsValid = false;
                return headerRequest;
            }
            var requestTokenCheck = headerRequest.SourceCode + headerRequest.RequestId;
            var checkHashValue = HashSh512(requestTokenCheck);

            if (checkHashValue != headerRequest.RequestToken)
            {
                //headerRequest.ErrorMessage = "Request Token does not match";
                headerRequest.IsValid = false;
                return headerRequest;
            }
            //headerRequest.ErrorMessage = "Valid";
            headerRequest.IsValid = true;
            return headerRequest;
        }
        public string HashSh512(string input)
        {
            using (SHA512Managed sha1 = new SHA512Managed())
            {
                var hashSh512 = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));

                // declare stringbuilder
                var sb = new StringBuilder(hashSh512.Length * 2);

                // computing hashSh1
                foreach (byte b in hashSh512)
                {
                    // "x2"
                    sb.Append(b.ToString("X2").ToLower());
                }
                return sb.ToString();
            }
        }

        public AuthDTO ValidateUserAuthorization(ClaimsPrincipal user)
        {
            //var headerRequestResult = CheckRequestHeader();
            //if (!headerRequestResult.IsValid)
            //{
            //    return new AuthDTO
            //    {
            //        Message = "Invalid Header Request",
            //        StatusCode = StatusCodes.Status400BadRequest,
            //        Success = false
            //    };
            //}

            var currentUserEmail = _userRepo.GetCurrentUserEmail(user);
            var userDetails = _userRepo.GetUserDetails(currentUserEmail, user);
            if (userDetails == null)
            {
                return new AuthDTO
                {
                    Message = "Unable to retrieve User Details!",
                    StatusCode = StatusCodes.Status404NotFound,
                    Success = false
                };
            }

            return new AuthDTO
            {
                Message = null,
                StatusCode = StatusCodes.Status200OK,
                Success = true,
                UserProfile = userDetails
            };
        }
    }
}
