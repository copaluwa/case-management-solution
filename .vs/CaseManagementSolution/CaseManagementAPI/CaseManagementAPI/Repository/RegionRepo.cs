﻿using CaseManagementAPI.Data;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository
{
    public class RegionRepo : IRegionRepo
    {
        private readonly ApplicationDbContext _db;
        
        public RegionRepo(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task<bool> CreateRegion(Region region)
        {
            _db.Regions.Add(region);
            return await Save();
        }
        public Region GetRegionById(int id)
        {
            return _db.Regions.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
        }
        public async Task<bool> UpdateRegion(UserProfile userDetails, Region entity, string region)
        {
            entity.Region_Name = region;
            entity.DateModified = DateTime.Now.Date;
            entity.ModifiedBy_Name = userDetails.FullName;
            entity.ModifiedBy_Email = userDetails.EmailAddress;
            entity.ModifiedBy_Role = userDetails.UserRole;
            entity.ModifiedBy_Affiliate = userDetails.Affiliate;


            _db.Regions.Update(entity);
            var value = Save();
            return await value;
        }
        public ICollection<Region> GetRegions(UserProfile userDetails)
        {
            switch (userDetails.UserRole)
            {
                case Role.Group:
                    return _db.Regions.Include("Affiliates").Where(a => a.IsDeleted == false).ToList();
                case Role.Regional:
                    return _db.Regions.Include("Affiliates").Where(a => a.Region_Name.ToLower() == userDetails.Region.ToLower() &&  a.IsDeleted == false).ToList();
                  
                case Role.Affiliate:
                    return _db.Regions.Include("Affiliates").Where(a => a.Region_Name.ToLower() == userDetails.Region.ToLower() & a.IsDeleted == false).ToList();
                case Role.UserManager:
                    return _db.Regions.Include("Affiliates").Where(a => a.IsDeleted == false).ToList();
                default:
                    return new List<Region>();
            }
            
            //var theRegion = new List<Region>();
            //var Lstpersons = (from p in _db.Regions let st = p.Affiliates from s in st where s.IsDeleted == false select p.).ToList();
            ////  var regions = _db.Regions.Include(x => x.Affiliates.Where(y => y.IsDeleted == false)).ToList();
            //return Lstpersons;
            //.Where(x => x.streetlivedin.Any(y => y.AddressType == "Home"));
            //var query = DB.Places.Where(p => p.CityId == CityId).Include(p => p.Images).ToList();
            //var affiliates = _db.Affiliates.Where(x => x.IsDeleted == false)
            //var theValue = from e in _db.Regions where e.IsDeleted == false 
            //               select new IEnumerable(Affiliate)
            //               {

            //               }
            //{
            //    request = e
            //}).FirstOrDefaultAsync()
            //return  _db.Regions.Where(x => x.IsDeleted == false).the(x => x.Affiliates.Where(x => x.IsDeleted == false).ToList()).ToList();
            //return _db.Regions.Include("Affiliates").Where(a => a.IsDeleted == false).ToList();

            // _db.Regions.Where(x => x.IsDeleted == false).Select(p => p.Affiliates.).Include(x => x.Affiliates.Where(p => p.IsDeleted == false)).ToList();
        }
        public bool RegionExists(string region)
        {
            bool value = _db.Regions.Any(a => a.Region_Name.ToLower().Trim() == region.ToLower().Trim() && a.IsDeleted == false);
            return value;
        }
        public async Task<bool> DeleteRegion(UserProfile userDetails, Region entity)
        {
            entity.DateDeleted = DateTime.Now.Date;
            entity.DeletedBy_Name = userDetails.FullName;
            entity.DeletedBy_Email = userDetails.EmailAddress;
            entity.DeletedBy_Role = userDetails.UserRole;
            entity.IsDeleted = true;

            _db.Regions.Update(entity);
            var value = Save();
            return await value;
        }

        public async Task<bool> Save()
        {
            return await _db.SaveChangesAsync() >= 0 ? true : false;
        }
    }
}
