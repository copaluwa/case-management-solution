﻿using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository.IRepository
{
    public interface IAuthRepo
    {
        HeaderRequest CheckRequestHeader();
        string HashSh512(string input);
        AuthDTO ValidateUserAuthorization(ClaimsPrincipal user);
    }
}
