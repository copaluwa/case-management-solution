﻿using CaseManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository.IRepository
{
   public interface IRegionRepo
    {
        bool RegionExists(string region);
        Task<bool> CreateRegion(Region region);
        Region GetRegionById(int id);
        Task<bool> UpdateRegion(UserProfile userDetails, Region entity, string region);
        Task<bool> DeleteRegion(UserProfile userDetails, Region entity);
        ICollection<Region> GetRegions(UserProfile userDetails);
      
        Task<bool> Save();
    }
}
