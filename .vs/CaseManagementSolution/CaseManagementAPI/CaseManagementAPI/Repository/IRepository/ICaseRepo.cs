﻿using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository.IRepository
{
    public interface ICaseRepo
    {
        bool CaseExists(int? whistleblowerNo);
        Task<bool> CreateCase(Case newCase);
        string GetCaseNumber();
        Task<bool> Save();
        Task<bool> CreateNote(CaseNote newNote, UserProfile userDetails);
        Task<bool> CreateNote(Case caseDetails, UserProfile userDetails, string note);
        Task<bool> UpdateNote(UserProfile userDetails, CaseNote entity, string newNote);
        Task<bool> UpdateCase(Case caseDetail);
        Case GetCaseById(int id);
        //bool AddNote(CaseNote newNote);
        bool CaseExistsByCaseIdAndCaseNo(int caseId, string caseNo);
        CaseNote GetCaseNoteById(int id);
        Case GetCaseByCaseNo(string caseNo, UserProfile userDetails);
        bool CaseExistsByCaseNo(string caseNo);
        ICollection<Case> GetAllCases(UserProfile userDetails);
        ICollection<Case> GetCasesByRegion(UserProfile userDetails, string region);
        ICollection<Case> GetCasesBySource(string source);
        ICollection<Case> GetCasesByAffiliate(string affiliate);
        Case GetCaseByIdWithoutCaseNote(UserProfile userDetails, int id);
        Task<bool> ReassignCase(UserProfile assignerDetails, Case entity, UserProfile reassignedToUserDetails);
        long GetCaseCountBySource(string source);
        long GetCaseCountByAffiliate(string affiliate);
        long GetCaseCountByIncidentSubCategory(string incidentSubcategory);
        long GetCaseCountByRegion(string region);
        long GetCaseCountByIncidentCategory(string incidentCategory);

        long GetCaseCountByIncidentSub1Type(string incidentSub1Type);

        long GetCaseCountByIncidentSub2Type(string incidentSub2Type);
        ICollection<Case> SearchAllCases(UserProfile userDetails, SearchCaseDTO searchCaseDTO);

        IQueryable<Case> GetQueryableCases(UserProfile userDetails);
        string GetCaseNumber(string region);
        //bool DeleteAffiliate(UserProfile userDetails, Affiliate entity);
        bool ValidateCaseFiles(IList<IFormFile> files);
        Task<IEnumerable<CaseFilePropsDTO>> CreateCaseFile(IList<IFormFile> files, string caseNumber, int caseId);
    }
}
