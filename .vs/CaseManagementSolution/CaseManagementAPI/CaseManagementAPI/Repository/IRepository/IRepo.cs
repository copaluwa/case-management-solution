﻿using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository.IRepository
{
    public interface IRepo
    {
        Task<bool> Save();
        Task<bool> AuditLogger(UserProfile userProfile, ActivityActionType activityAction, string description);


    }
}
