﻿using CaseManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository.IRepository
{
    public interface IAffiliateRepo
    {
        Task<bool> DeleteAffiliate(UserProfile userDetails, Affiliate entity);
        ICollection<Affiliate> GetAffiliatesByRegionId(int regionId);
        Affiliate GetAffiliatesById(int id);
        Task<bool> UpdateAffiliate(UserProfile userDetails, Affiliate entity, string affiliate);
        Task<bool> DeleteAffiliate(Affiliate entity);
        bool AffiliatesExists(string affiliate);
        Task<bool> CreateAffiliate(Affiliate affiliate);
        Task<bool> Save();
    }
}
