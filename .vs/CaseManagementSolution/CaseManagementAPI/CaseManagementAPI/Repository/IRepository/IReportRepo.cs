﻿using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository.IRepository
{
    public interface IReportRepo
    {
        Task<IEnumerable<object>> GetReportBySource(ReportRequestDTO request);
        Task<IEnumerable<object>> GetReportByIncidentCategories(ReportRequestDTO request);
        Task<IEnumerable<object>> GetReportByIncidentTypes(ReportRequestDTO request);
        Task<IEnumerable<object>> GetReportByStaffInvolvement(ReportRequestDTO request);
        Task<IEnumerable<object>> GetReportByAffiliates(ReportRequestDTO request);
        Task<object> GetGrossOnslaughtReport(ReportRequestDTO request);
        Task<IEnumerable<object>> SumGrossOnslaughtPerAffiliateReport(ReportRequestDTO request);
        Task<object> GetNetLossReport(ReportRequestDTO request);
        Task<IEnumerable<object>> SumNetLossPerAffiliateReport(ReportRequestDTO request);
        Task<IEnumerable<object>> SumNetLossPerIncidentCategoryReport(ReportRequestDTO request);
        Task<IEnumerable<object>> SumNetLossPerStaffInvolvementReport(ReportRequestDTO request);
        Task<IEnumerable<object>> GetAverageDaysPerAffiliate(ReportRequestDTO request);
        Task<IEnumerable<object>> GetAverageDaysPerSource(ReportRequestDTO request);
        Task<object> GetFinalizedCases(ReportRequestDTO request);
        Task<IEnumerable<object>> GetReportPerQuarter(ReportRequestDTO request);
    }
}
