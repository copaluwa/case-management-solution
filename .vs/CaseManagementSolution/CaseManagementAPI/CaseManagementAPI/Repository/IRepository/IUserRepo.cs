﻿using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository.IRepository
{
    public interface IUserRepo
    {
        Role GetUserRoleByEmail(string email);
        bool UserExists(string emailAddress);
        bool CreateUser(UserProfile newUser);
        string GetCurrentUserEmail(ClaimsPrincipal user);
        string GetCurrentUserFullName(ClaimsPrincipal user);
        UserProfile GetUserDetails(string emailAddress, ClaimsPrincipal user);
        string GetCurrentUserId(ClaimsPrincipal user);
        ICollection<UserProfile> GetAllUsers();
        UserProfile GetUserById(int id);
        bool UpdateUserProfile(UserProfile userDetails, UserProfile entity, UserProfileDTO userProfileDTO);
        bool UserExists(int id);
        bool DeleteUser(UserProfile user);
       
        List<UserProfile> GetUsersToReassign(UserProfile userDetails, string userName);
        UserProfile GetUserByEmailAddress(string email);
    }
}
