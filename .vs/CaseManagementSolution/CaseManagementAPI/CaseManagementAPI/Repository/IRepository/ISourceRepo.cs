﻿using CaseManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository.IRepository
{
    public interface ISourceRepo
    {

        bool SourceExists(string source);
        Task<bool> CreateSource(CaseSource newSource);
        ICollection<CaseSource> GetSources();
        CaseSource GetSourceById(int id);
        Task<bool> UpdateSource(UserProfile userDetails, CaseSource entity, string source);
        Task<bool> DeleteSource(UserProfile userDetails, CaseSource entity);

    }
}
