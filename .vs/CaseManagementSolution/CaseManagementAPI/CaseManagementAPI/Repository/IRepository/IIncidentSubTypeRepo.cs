﻿using CaseManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository.IRepository
{
    public interface IIncidentSubTypeRepo
    {
        bool IncidentSubType1Exists(string incidentSub1Type, int incidentSubCategoryID);

        bool IncidentSubType1Exists(string incidentSub1Type);

        Task<bool> CreateSub1Type(IncidentSub1Type incidentSub1Type);

        bool IncidentSubType2Exists(string incidentSub2Type, int incidentSubCategoryID);

        Task<bool> CreateIncidentSub2Type(IncidentSub2Type incidentSub2Type);

        ICollection<IncidentSub1Type> GetIncidentSub1Types();

        IncidentSub1Type GetIncidentSub1TypesById(int id);

        Task<bool> UpdateIncidentSub1Type(UserProfile userDetails, IncidentSub1Type entity, string incidentSub1Type);

        Task<bool> DeleteIncidentSub1Type(UserProfile userDetails, IncidentSub1Type entity);

        ICollection<IncidentSub2Type> GetIncidentSub2TypesByIncidentSub1TypeId(int incidentSub1TypeId);

        Task<bool> DeleteIncidentSub2Type(UserProfile userDetails, IncidentSub2Type entity);

        IncidentSub2Type GetIncidentSub2TypeById(int id, int incidentSub1TypeId);

        Task<bool> UpdateIncidentSub2Type(UserProfile userDetails, IncidentSub2Type entity, string incidentSub2Type);

        IncidentSub2Type GetIncidentSub2TypeById(int id);

        Task<bool> DeleteIncidentSub2Type(IncidentSub2Type entity);

        bool IncidentSub2TypeExists(string incidentSub2Type, int incidentSub1TypeId);

        Task<bool> Save();
        

        //Task<bool> DeleteIncidentSubCategory(IncidentSubCategory entity);
        //IncidentSubCategory GetIncidentSubCategoryById(int id);
        //Task<bool> CreateIncidentCategory(IncidentCategory incidentCategory);
        //bool IncidentCategoryExists(string incidentCategory);

        //bool IncidentSubCategoryExists(string subcategoryCategory, int incidentCategoryID);
        //ICollection<IncidentCategory> GetIncidentCategories();
        //Task<bool> UpdateIncidentCategory(UserProfile userDetails, IncidentCategory entity, string incidentCatgeory);
        //IncidentCategory GetIncidentCategoryById(int id);
        //Task<bool> DeleteIncidentCategory(UserProfile userDetails, IncidentCategory entity);
        //IncidentSubCategory GetIncidentSubCategoryById(int id, int incidentCategoryId);
        //Task<bool> UpdateSubIncidentCategory(UserProfile userDetails, IncidentSubCategory entity, string incidentSubCategory);
        //ICollection<IncidentSubCategory> GetIncidentSubCategoriesByIncidentCategoryId(int incidentCategoryId);
        //Task<bool> DeleteIncidentSubCategory(UserProfile userDetails, IncidentSubCategory entity);


    }
}
