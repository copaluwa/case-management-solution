﻿using CaseManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository.IRepository
{
    public interface IIncidentCategoryRepo
    {
        Task<bool> DeleteIncidentSubCategory(IncidentSubCategory entity);
        IncidentSubCategory GetIncidentSubCategoryById(int id);
        Task<bool> CreateIncidentCategory(IncidentCategory incidentCategory);
        bool IncidentCategoryExists(string incidentCategory);
        Task<bool> CreateSubcategory(IncidentSubCategory incidentSubCategory);
        bool IncidentSubCategoryExists(string subcategoryCategory, int incidentCategoryID);
        ICollection<IncidentCategory> GetIncidentCategories();
        Task<bool> UpdateIncidentCategory(UserProfile userDetails, IncidentCategory entity, string incidentCatgeory);
        IncidentCategory GetIncidentCategoryById(int id);
        Task<bool> DeleteIncidentCategory(UserProfile userDetails, IncidentCategory entity);
        IncidentSubCategory GetIncidentSubCategoryById(int id, int incidentCategoryId);
        Task<bool> UpdateSubIncidentCategory(UserProfile userDetails, IncidentSubCategory entity, string incidentSubCategory);
        ICollection<IncidentSubCategory> GetIncidentSubCategoriesByIncidentCategoryId(int incidentCategoryId);
        Task<bool> DeleteIncidentSubCategory(UserProfile userDetails, IncidentSubCategory entity);
        Task<bool> Save();
    }
}
