﻿using Azure;
using Azure.Core;
using CaseManagementAPI.Data;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository
{
    public class ReportRepo : IReportRepo
    {
        private readonly ApplicationDbContext _db;

        public ReportRepo(ApplicationDbContext db) => _db = db;

        private DateTime? GetStartEndDate(DateTime? startDate, DateTime? requestEndDate, out DateTime? endDate)
        {
            if (startDate.HasValue)
                startDate = new DateTime(startDate.Value.Year, startDate.Value.Month, startDate.Value.Day, 0, 0, 0);
            if (requestEndDate.HasValue)
                requestEndDate = new DateTime(requestEndDate.Value.Year, requestEndDate.Value.Month, requestEndDate.Value.Day, 23, 59, 59);

            endDate = requestEndDate;
            return startDate;
        }

        public async Task<IEnumerable<object>> GetReportBySource(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var response = await (from source in _db.CaseSources
                                  where !source.IsDeleted
                                  let cases = (
                                      from _case in _db.Cases
                                      where (source.Source.ToLower().Equals(_case.Source.ToLower())
                                          && (request.Role == Role.Regional ? _case.Region.ToLower().Equals(request.Region.ToLower()) :
                                            (request.Role == Role.Affiliate ? _case.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                                            : null != _case.Region/* && null != _case.Affiliate*/))
                                          && (null == request.Status || request.Status.Value == _case.Status)
                                          && (null == startDate || _case.DateAdded >= startDate.Value)
                                          && (null == endDate || _case.DateAdded <= endDate.Value))
                                      select _case)
                                  select new { Source = source.Source, Count = cases.Count() })
                                  .AsNoTracking().ToListAsync();
            return response;
        }

        public async Task<IEnumerable<object>> GetReportByIncidentCategories(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var response = await (from category in _db.IncidentCategories
                                  where !category.IsDeleted
                                  let cases = (
                                      from _case in _db.Cases
                                      where (category.IncidentCategoryName.ToLower().Equals(_case.IncidentCategory.ToLower())
                                        && (request.Role == Role.Regional ? _case.Region.ToLower().Equals(request.Region.ToLower()) :
                                            (request.Role == Role.Affiliate ? _case.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                                            : null != _case.Region/* && null != _case.Affiliate*/))
                                          && (null == request.Status || request.Status.Value == _case.Status)
                                          && (null == startDate || _case.DateAdded >= startDate.Value)
                                          && (null == endDate || _case.DateAdded <= endDate.Value))
                                      select _case)
                                  select new { IncidentCategory = category.IncidentCategoryName, Count = cases.Count() })
                                  .AsNoTracking().ToListAsync();
            return response;
        }

        public async Task<IEnumerable<object>> GetReportByIncidentTypes(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var openCasesCount = await _db.Cases.AsNoTracking().CountAsync(x =>
                (null == startDate || x.DateAdded >= startDate.Value)
                && (null == endDate || x.DateAdded <= endDate.Value)
                && (request.Role == Role.Regional ? x.Region.ToLower().Equals(request.Region.ToLower()) :
                    (request.Role == Role.Affiliate ? x.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                    : null != x.Region/* && null != _case.Affiliate*/))
                && x.Status == CaseStatus.Open);

            var closedCasesCount = await _db.Cases.AsNoTracking().CountAsync(x =>
                (null == startDate || x.DateAdded >= startDate.Value)
                && (null == endDate || x.DateAdded <= endDate.Value)
                && (request.Role == Role.Regional ? x.Region.ToLower().Equals(request.Region.ToLower()) :
                    (request.Role == Role.Affiliate ? x.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                    : null != x.Region/* && null != _case.Affiliate*/))
                && x.Status == CaseStatus.Closed);

            return new List<object>
            {
                new { Type = CaseStatus.Open.GetDescription(), Count = openCasesCount },
                new { Type = CaseStatus.Closed.GetDescription(), Count = closedCasesCount }
            };
        }

        public async Task<IEnumerable<object>> GetReportByStaffInvolvement(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var response = await _db.Cases.AsNoTracking().Where(x =>
                (null == request.Status || request.Status.Value == x.Status)
                    && (request.Role == Role.Regional ? x.Region.ToLower().Equals(request.Region.ToLower()) :
                        (request.Role == Role.Affiliate ? x.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                        : null != x.Region/* && null != _case.Affiliate*/))
                    && (null == startDate || x.DateAdded >= startDate.Value)
                    && (null == endDate || x.DateAdded <= endDate.Value))
                .GroupBy(x => x.StaffInvolvement).Select(x => new
                {
                    StaffInvolvementType = x.Key,
                    Count = x.Count()   
                }).ToListAsync();

            var staffInvolvementValues = EnumExtension.GetStaffInvolvementEnumNames();
            var matchedStaffIv = from staffIv in staffInvolvementValues where response.Any(x => x.StaffInvolvementType.ToLower().Equals($"staff involvement - {staffIv}")) select staffIv;
            staffInvolvementValues = staffInvolvementValues.Except(matchedStaffIv);
            if (staffInvolvementValues.Any())
            {
                foreach (var os in staffInvolvementValues)
                    response.Add(new { StaffInvolvementType = os, Count = 0 });
            }

            return response;
        }

        public async Task<IEnumerable<object>> GetReportByAffiliates(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var response = await (from affiliate in _db.Affiliates
                                  where !affiliate.IsDeleted
                                  let cases = (
                                      from _case in _db.Cases
                                      where (affiliate.Affiliate_Name.ToLower().Equals(_case.Affiliate.ToLower())
                                          && (request.Role == Role.Regional ? _case.Region.ToLower().Equals(request.Region.ToLower()) :
                                            (request.Role == Role.Affiliate ? _case.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                                            : null != _case.Region/* && null != _case.Affiliate*/))
                                          && (null == request.Status || request.Status.Value == _case.Status)
                                          && (null == startDate || _case.DateAdded >= startDate.Value)
                                          && (null == endDate || _case.DateAdded <= endDate.Value))
                                      select _case)
                                  select new AffiliateReportResponse { Affiliate = affiliate.Affiliate_Name, Count = cases.Count() })
                                  .AsNoTracking().ToListAsync();
            
            if (request.Role == Role.Affiliate)
                response = response.Where(x => x.Affiliate.ToLower().Equals(request.Affiliate.ToLower())).ToList();
            else if (request.Role == Role.Regional)
            {
                var affliatesInUserRegion = await (from _region in _db.Regions
                                       join affiliate in _db.Affiliates on _region.Id equals affiliate.RegionId
                                into affiliates
                                       from _affiliate in affiliates.DefaultIfEmpty()
                                       where _region.Region_Name.ToLower().Equals(request.Region.ToLower())
                                       select _affiliate.Affiliate_Name).ToListAsync();

                response = (from res in response where affliatesInUserRegion.Contains(res.Affiliate) select res).ToList();
            }
            return response;
        }

        public async Task<object> GetGrossOnslaughtReport(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var grossSum = await _db.Cases.AsNoTracking().Where(x =>
                (null == request.Status || request.Status.Value == x.Status)
                    && (request.Role == Role.Regional ? x.Region.ToLower().Equals(request.Region.ToLower()) :
                        (request.Role == Role.Affiliate ? x.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                        : null != x.Region/* && null != _case.Affiliate*/))
                    && (null == startDate || x.DateAdded >= startDate.Value)
                    && (null == endDate || x.DateAdded <= endDate.Value))
                .SumAsync(x => x.GrossOnSlaught);

            return new { GrossOnSlaught = grossSum };
        }

        public async Task<IEnumerable<object>> SumGrossOnslaughtPerAffiliateReport(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var response = await (from affiliate in _db.Affiliates
                                  where !affiliate.IsDeleted
                                  let cases = (
                                      from _case in _db.Cases
                                      where (affiliate.Affiliate_Name.ToLower().Equals(_case.Affiliate.ToLower())
                                          && (request.Role == Role.Regional ? _case.Region.ToLower().Equals(request.Region.ToLower()) :
                                            (request.Role == Role.Affiliate ? _case.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                                            : null != _case.Region/* && null != _case.Affiliate*/))
                                          && (null == request.Status || request.Status.Value == _case.Status)
                                          && (null == startDate || _case.DateAdded >= startDate.Value)
                                          && (null == endDate || _case.DateAdded <= endDate.Value))
                                      select _case)
                                  select new { Affiliate = affiliate.Affiliate_Name, Sum = cases.Sum(x => x.GrossOnSlaught) })
                                  .AsNoTracking().ToListAsync();
            return response;
        }

        public async Task<object> GetNetLossReport(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var netSum = await _db.Cases.AsNoTracking().Where(x =>
                (null == request.Status || request.Status.Value == x.Status)
                    && (request.Role == Role.Regional ? x.Region.ToLower().Equals(request.Region.ToLower()) :
                        (request.Role == Role.Affiliate ? x.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                        : null != x.Region/* && null != _case.Affiliate*/))
                    && (null == startDate || x.DateAdded >= startDate.Value)
                    && (null == endDate || x.DateAdded <= endDate.Value))
                .SumAsync(x => x.NetLoss);

            return new { NetLoss = netSum };
        }

        public async Task<IEnumerable<object>> SumNetLossPerAffiliateReport(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var response = await (from affiliate in _db.Affiliates
                                  where !affiliate.IsDeleted
                                  let cases = (
                                      from _case in _db.Cases
                                      where (affiliate.Affiliate_Name.ToLower().Equals(_case.Affiliate.ToLower())
                                          && (request.Role == Role.Regional ? _case.Region.ToLower().Equals(request.Region.ToLower()) :
                                            (request.Role == Role.Affiliate ? _case.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                                            : null != _case.Region/* && null != _case.Affiliate*/))
                                          && (null == request.Status || request.Status.Value == _case.Status)
                                          && (null == startDate || _case.DateAdded >= startDate.Value)
                                          && (null == endDate || _case.DateAdded <= endDate.Value))
                                      select _case)
                                  select new { Affiliate = affiliate.Affiliate_Name, Sum = cases.Sum(x => x.NetLoss) })
                                  .AsNoTracking().ToListAsync();
            return response;
        }

        public async Task<IEnumerable<object>> SumNetLossPerIncidentCategoryReport(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var response = await (from category in _db.IncidentCategories
                                  where !category.IsDeleted
                                  let cases = (
                                      from _case in _db.Cases
                                      where (category.IncidentCategoryName.ToLower().Equals(_case.IncidentCategory.ToLower())
                                          && (request.Role == Role.Regional ? _case.Region.ToLower().Equals(request.Region.ToLower()) :
                                            (request.Role == Role.Affiliate ? _case.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                                            : null != _case.Region/* && null != _case.Affiliate*/))
                                          && (null == request.Status || request.Status.Value == _case.Status)
                                          && (null == startDate || _case.DateAdded >= startDate.Value)
                                          && (null == endDate || _case.DateAdded <= endDate.Value))
                                      select _case)
                                  select new { Category = category.IncidentCategoryName, Sum = cases.Sum(x => x.NetLoss) })
                                  .AsNoTracking().ToListAsync();
            return response;
        }

        public async Task<IEnumerable<object>> SumNetLossPerStaffInvolvementReport(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var response = await _db.Cases.AsNoTracking().Where(x =>
                (null == request.Status || request.Status.Value == x.Status)
                    && (request.Role == Role.Regional ? x.Region.ToLower().Equals(request.Region.ToLower()) :
                        (request.Role == Role.Affiliate ? x.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                        : null != x.Region/* && null != _case.Affiliate*/))
                    && (null == startDate || x.DateAdded >= startDate.Value)
                    && (null == endDate || x.DateAdded <= endDate.Value))
                .GroupBy(x => x.StaffInvolvement).Select(x => new
                {
                    StaffInvolvementType = x.Key,
                    Sum = x.Sum(x => x.NetLoss)
                }).ToListAsync();

            var staffInvolvementValues = EnumExtension.GetStaffInvolvementEnumNames();
            var matchedStaffIv = from staffIv in staffInvolvementValues where response.Any(x => x.StaffInvolvementType.ToLower().Equals($"staff involvement - {staffIv}")) select staffIv;
            staffInvolvementValues = staffInvolvementValues.Except(matchedStaffIv);
            if (staffInvolvementValues.Any())
            {
                foreach (var os in staffInvolvementValues)
                    response.Add(new { StaffInvolvementType = os, Sum = 0m });
            }

            var totalSum = response.Sum(x => x.Sum);
            return response.Select(x => new
            {
                x.StaffInvolvementType,
                x.Sum,
                Percentage = $"{((x.Sum / totalSum) * 100).ToString("F")}%"
            });
        }

        public async Task<IEnumerable<object>> GetAverageDaysPerAffiliate(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var affiliateCases = await (from affiliate in _db.Affiliates
                                  where !affiliate.IsDeleted
                                  let cases = (
                                      from _case in _db.Cases
                                      where (affiliate.Affiliate_Name.ToLower().Equals(_case.Affiliate.ToLower())
                                          && (request.Role == Role.Regional ? _case.Region.ToLower().Equals(request.Region.ToLower()) :
                                            (request.Role == Role.Affiliate ? _case.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                                            : null != _case.Region/* && null != _case.Affiliate*/))
                                          && (null == request.Status || request.Status.Value == _case.Status)
                                          && (null == startDate || _case.DateAdded >= startDate.Value)
                                          && (null == endDate || _case.DateAdded <= endDate.Value))
                                      select _case)
                                  select new
                                  {
                                      Affiliate = affiliate.Affiliate_Name,
                                      Cases = cases.ToList()
                                  })
                                  .AsNoTracking().ToListAsync();
            var response = affiliateCases.Select(x => new
            {
                Affiliate = x.Affiliate,
                AverageDays = x.Cases.Any() ? Convert.ToInt32(x.Cases.Average(x => (x.DateReported.Subtract(x.DateOfIncident)).Days)) : 0
            }).ToList();
            return response;
        }

        public async Task<IEnumerable<object>> GetAverageDaysPerSource(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var sourceCases = await (from source in _db.CaseSources
                                        where !source.IsDeleted
                                        let cases = (
                                            from _case in _db.Cases
                                            where (source.Source.ToLower().Equals(_case.Source.ToLower())
                                                && (request.Role == Role.Regional ? _case.Region.ToLower().Equals(request.Region.ToLower()) :
                                                  (request.Role == Role.Affiliate ? _case.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                                                  : null != _case.Region/* && null != _case.Affiliate*/))
                                                && (null == request.Status || request.Status.Value == _case.Status)
                                                && (null == startDate || _case.DateAdded >= startDate.Value)
                                                && (null == endDate || _case.DateAdded <= endDate.Value))
                                            select _case)
                                        select new
                                        {
                                            Source = source.Source,
                                            Cases = cases.ToList()
                                        })
                                  .AsNoTracking().ToListAsync();
            var response = sourceCases.Select(x => new
            {
                Source = x.Source,
                AverageDays = x.Cases.Any() ? Convert.ToInt32(x.Cases.Average(x => (x.DateReported.Subtract(x.DateOfIncident)).Days)) : 0
            }).ToList();
            return response;
        }

        public async Task<object> GetFinalizedCases(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var casesCounts = await _db.Cases.AsNoTracking().Where(x =>
                    (request.Role == Role.Regional ? x.Region.ToLower().Equals(request.Region.ToLower()) :
                        (request.Role == Role.Affiliate ? x.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                        : null != x.Region/* && null != _case.Affiliate*/))
                    && (null == startDate || x.DateAdded >= startDate.Value)
                    && (null == endDate || x.DateAdded <= endDate.Value))
                .GroupBy(x => x.Status).Select(x => new
                {
                    Status = x.Key.GetDescription(),
                    Count = x.Count()
                }).ToListAsync();

            var statusValues = EnumExtension.GetCaseStatusEnumNames();
            var otherStatuses = from status in statusValues where casesCounts.Any(x => !(x.Status.ToLower().Equals(status.ToLower()))) select status;
            if (otherStatuses.Any())
            {
                foreach (var os in otherStatuses)
                    casesCounts.Add(new { Status = os, Count = 0 });
            }

            var closedCases = casesCounts.FirstOrDefault(x => x.Status.ToLower().Equals(CaseStatus.Closed.GetDescription().ToLower()));
            var openCases = casesCounts.FirstOrDefault(x => x.Status.ToLower().Equals(CaseStatus.Open.GetDescription().ToLower()));

            int totalCasesCount = closedCases.Count + openCases.Count;
            var response = new
            {
                TotalCasesReceived = totalCasesCount,
                TotalCasesFinalized = closedCases.Count,
                TotalCasesUnderInvestigation = openCases.Count,
                PercentageOpenCases = $"{(((float)openCases.Count / totalCasesCount) * 100).ToString("F")}%",
                PercentageFinalizedCases = $"{(((float)closedCases.Count / totalCasesCount) * 100).ToString("F")}%"
            };
            return response;
        }

        public async Task<IEnumerable<object>> GetReportPerQuarter(ReportRequestDTO request)
        {
            var startDate = GetStartEndDate(request.StartDate, request.EndDate, out DateTime? endDate);

            var response = await _db.Cases.AsNoTracking().Where(x =>
                    (null == request.Status || request.Status.Value == x.Status)
                    && (request.Role == Role.Regional ? x.Region.ToLower().Equals(request.Region.ToLower()) :
                        (request.Role == Role.Affiliate ? x.Affiliate.ToLower().Equals(request.Affiliate.ToLower())
                        : null != x.Region/* && null != _case.Affiliate*/))
                    && (null == startDate || x.DateAdded >= startDate.Value)
                    && (null == endDate || x.DateAdded <= endDate.Value))
                .GroupBy(x => x.Quarter).Select(x => new
                {
                    Quarter = x.Key,
                    Count = x.Count()
                }).ToListAsync();

            var quarters = new string[4] { "Q1", "Q2", "Q3", "Q4" };
            var matchedQuarters = from quarter in quarters where response.Any(x => x.Quarter.ToLower().Equals(quarter.ToLower())) select quarter;
            quarters = quarters.Except(matchedQuarters).ToArray();
            if (quarters.Any())
            {
                foreach (var os in quarters)
                    response.Add(new { Quarter = os, Count = 0 });
            }

            return response;
        }
    }
}
