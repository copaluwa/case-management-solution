﻿using CaseManagementAPI.Data;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository
{
    public class SourceRepo : ISourceRepo
    {
        private readonly ApplicationDbContext _db;
       
        public SourceRepo(ApplicationDbContext db)
        {
            _db = db;
        }

        public bool SourceExists(string source)
        {
            bool value =  _db.CaseSources.Any(a => a.Source.ToLower().Trim() == source.ToLower().Trim() && a.IsDeleted == false);
            return  value;
        }
        public async Task<bool> CreateSource(CaseSource newSource)
        {
            _db.CaseSources.Add(newSource);
            return await Save();
        }
        public ICollection<CaseSource> GetSources()
        {
            return  _db.CaseSources.Where(a => a.IsDeleted == false).ToList();
        }

        public async Task<bool> UpdateSource(UserProfile userDetails, CaseSource entity, string source)
        {
            entity.Source = source;
            entity.DateModified = DateTime.Now.Date;
            entity.ModifiedBy_Name = userDetails.FullName;
            entity.ModifiedBy_Email = userDetails.EmailAddress;
            entity.ModifiedBy_Role = userDetails.UserRole;
            entity.ModifiedBy_Affiliate = userDetails.Affiliate;


            _db.CaseSources.Update(entity);
            var value = Save();
            return await value;
        }

        public async Task<bool> DeleteSource(UserProfile userDetails, CaseSource entity)
        {
            entity.DateDeleted = DateTime.Now.Date;
            entity.DeletedBy_Name = userDetails.FullName;
            entity.DeletedBy_Email = userDetails.EmailAddress;
            entity.DeletedBy_Role = userDetails.UserRole;
            entity.IsDeleted = true;

            _db.CaseSources.Update(entity);
            var value = Save();
            return await value;
        }

        public CaseSource GetSourceById(int id)
        {
            return _db.CaseSources.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
        }
        public async Task<bool> Save()
        {
            return await _db.SaveChangesAsync() >= 0 ? true : false;
        }
    }
}
