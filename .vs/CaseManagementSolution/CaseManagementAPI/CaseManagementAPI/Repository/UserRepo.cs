﻿using CaseManagementAPI.Const;
using CaseManagementAPI.Data;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository
{
    public class UserRepo : IUserRepo
    {

        private readonly ApplicationDbContext _db;
     
        public UserRepo(ApplicationDbContext db, IHttpContextAccessor contextAccessor)
        {
            _db = db;
        
        }



        //Get user by Role
        public Role GetUserRoleByEmail(string email)
        {
            var userDetails = _db.UserProfile.Where(q => q.EmailAddress == email).FirstOrDefault();
            if (userDetails == null)
            {
                return Role.Unknown;
            }
            return userDetails.UserRole;
        }
        public bool CreateUser(UserProfile newUser)
        {
            _db.UserProfile.Add(newUser);
            return Save();
        }
        public bool UserExists(string emailAddress)
        {
            bool value = _db.UserProfile.Any(a => a.EmailAddress.ToLower().Trim() == emailAddress.ToLower().Trim());
            return value;
        }
        public bool UserExists(int id)
        {
            bool value = _db.UserProfile.Any(a => a.Id == id);
            return value;
        }
        public bool Save()
        {
            return _db.SaveChanges() >= 0 ? true : false;
        }

        public string GetCurrentUserEmail(ClaimsPrincipal user)
        {
            var email = user.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
            // var fullName = user.Identity.Name;
            return email;
        }

        public string GetCurrentUserFullName(ClaimsPrincipal user)
        {
            var fullName = user.Identity.Name.ToString();
            return fullName;
        }
        public string GetCurrentUserId(ClaimsPrincipal user)
        {
            string userId = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            return userId;
        }
        public UserProfile GetUserDetails(string emailAddress, ClaimsPrincipal user)
        {
            var userDetails = _db.UserProfile.Where(a => a.EmailAddress.ToLower().Trim() == emailAddress.ToLower().Trim()).FirstOrDefault();
            if (userDetails == null)
            {
                return userDetails;
            }
            else if (string.IsNullOrEmpty(userDetails.FullName) || string.IsNullOrEmpty(userDetails.UserId))
            {
                userDetails.FullName = GetCurrentUserFullName(user);
                userDetails.UserId = GetCurrentUserId(user);
                var value = Save();
                if (!value)
                {
                    return new UserProfile();
                }
            }
            return userDetails;

        }

        public ICollection<UserProfile> GetAllUsers()
        {
            return _db.UserProfile.OrderBy(a => a.Id).ToList();
        }

        public UserProfile GetUserById(int id)
        {
            //var avaka = _db.Cases.Find(id);
            return _db.UserProfile.Where(x => x.Id == id).FirstOrDefault();
        }

        public UserProfile GetUserByEmailAddress(string email)
        {
            return _db.UserProfile.Where(x => x.EmailAddress == email).FirstOrDefault();
        }
        public bool UpdateUserProfile(UserProfile userDetails, UserProfile entity, UserProfileDTO userProfileDTO)
        {
            entity.FullName = userProfileDTO.EmailAddress.Split('@')[0];
            entity.UserName = userProfileDTO.EmailAddress.Split('@')[0];
            entity.EmailAddress = userProfileDTO.EmailAddress;
            entity.Affiliate = userProfileDTO.Affiliate;
            entity.Region = userProfileDTO.Region;
            entity.UserRole = userProfileDTO.UserRole;
            entity.DateModified = DateTime.Now.Date;
            entity.ModifiedBy = userDetails.FullName;
            entity.ModifiedBy_Email = userDetails.EmailAddress;

            _db.UserProfile.Update(entity);
            var value = Save();
            return value;
        }
        public bool DeleteUser(UserProfile user)
        {
            _db.UserProfile.Remove(user);
            return Save();
        }
        public List<UserProfile> GetUsersToReassign(UserProfile userDetails, string userName)
        {
            var assignees = new UserProfile();
            userName = userName.Trim().ToLower();

            if (userDetails.UserRole == Role.Regional)
            {
               //return _db.UserProfile.Where(x => x.EmailAddress.ToLower().Contains(userName) && (x.Region.ToLower() == userDetails.Region.ToLower()) && (x.EmailAddress != userDetails.EmailAddress)).ToList();
               return _db.UserProfile.Where(x => x.EmailAddress.ToLower().Contains(userName) && (x.Region.ToLower() == userDetails.Region.ToLower())).ToList();
            }
            else if (userDetails.UserRole == Role.Affiliate)
            {
                return  _db.UserProfile.Where(x => x.EmailAddress.ToLower().Contains(userName) && (x.Affiliate.ToLower() == userDetails.Affiliate.ToLower())).ToList();
               
            }
            else if (userDetails.UserRole == Role.Group)
            {
                return _db.UserProfile.Where(x => x.EmailAddress.ToLower().Contains(userName)).ToList();
            }
            return new List<UserProfile>();
            //else if (userDetails.UserRole == Role.Group)
            //{
            //    var userProfile = _db.UserProfile.Where(x => x.Affiliate.ToLower() == userDetails.Affiliate.ToLower() && x.EmailAddress != userDetails.EmailAddress).ToList();
            //    return usersPerAffiliate;
            //}
            //    var assigneeDetails = _db.UserProfile.Select(x => new
            //      {
            //       // FullName= x.FullName,
            //        EmailAddress = x.EmailAddress
            //      }).ToList();

        }

        
    }
}
