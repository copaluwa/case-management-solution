﻿using CaseManagementAPI.Data;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository
{
    public class IncidentSubTypeRepo : IIncidentSubTypeRepo
    {
        private readonly ApplicationDbContext _db;

        public IncidentSubTypeRepo(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<bool> CreateSub1Type(IncidentSub1Type incidentSub1Type)
        {
            _db.IncidentSub1Types.Add(incidentSub1Type);
            return await Save();
        }
        public bool IncidentSubType1Exists(string incidentSub1Type, int incidentSubCategoryID)
        {
            bool value = _db.IncidentSub1Types.Any(a => a.IncidentSub1Type_Name.ToLower().Trim() == incidentSub1Type.ToLower().Trim() && a.IsDeleted == false && a.IncidentSubCategoryId == incidentSubCategoryID);
            return value;
        }
        public bool IncidentSubType1Exists(string incidentSub1Type)
        {
            bool value = _db.IncidentSub1Types.Any(a => a.IncidentSub1Type_Name.ToLower().Trim() == incidentSub1Type.ToLower().Trim() && a.IsDeleted == false);
            return value;
        }
        public bool IncidentSubType2Exists(string incidentSub2Type, int incidentSub1TypeId)
        {
            bool value = _db.IncidentSub2Types.Any(a => a.IncidentSub2Type_Name.Trim() == incidentSub2Type.ToLower().Trim() && a.IsDeleted == false && a.IncidentSub1TypeId == incidentSub1TypeId);
            return value;
        }
        public async Task<bool> CreateIncidentSub2Type(IncidentSub2Type incidentSub2Type)
        {
            _db.IncidentSub2Types.Add(incidentSub2Type);
            return await Save();
        }

        public async Task<bool> Save()
        {
            return await _db.SaveChangesAsync() >= 0 ? true : false;
        }

        public ICollection<IncidentSub1Type> GetIncidentSub1Types()//IncidentSub2Types
        {
            return _db.IncidentSub1Types.Include("IncidentSub2Types").Where(a => a.IsDeleted == false).ToList();
        }
        public IncidentSub1Type GetIncidentSub1TypesById(int id)
        {
            return _db.IncidentSub1Types.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
        }

        public async Task<bool> UpdateIncidentSub1Type(UserProfile userDetails, IncidentSub1Type entity, string incidentSub1Type)
        {
            entity.IncidentSub1Type_Name = incidentSub1Type;
            entity.DateModified = DateTime.Now.Date;
            entity.ModifiedBy_Name = userDetails.FullName;
            entity.ModifiedBy_Email = userDetails.EmailAddress;
            entity.ModifiedBy_Role = userDetails.UserRole;
            entity.ModifiedBy_Affiliate = userDetails.Affiliate;


            _db.IncidentSub1Types.Update(entity);
            var value = Save();
            return await value;
        }

        public async Task<bool> DeleteIncidentSub1Type(UserProfile userDetails, IncidentSub1Type entity)
        {
            entity.DateDeleted = DateTime.Now.Date;
            entity.DeletedBy_Name = userDetails.FullName;
            entity.DeletedBy_Email = userDetails.EmailAddress;
            entity.DeletedBy_Role = userDetails.UserRole;
            entity.IsDeleted = true;

            _db.IncidentSub1Types.Update(entity);
            var value = await Save();
            return value;
        }

        public ICollection<IncidentSub2Type> GetIncidentSub2TypesByIncidentSub1TypeId(int incidentSub1TypeId)
        {
            return _db.IncidentSub2Types.Where(a => a.IncidentSub1TypeId == incidentSub1TypeId && a.IsDeleted == false).ToList();
        }

        public async Task<bool> DeleteIncidentSub2Type(UserProfile userDetails, IncidentSub2Type entity)
        {
            entity.DateDeleted = DateTime.Now.Date;
            entity.DeletedBy_Name = userDetails.FullName;
            entity.DeletedBy_Email = userDetails.EmailAddress;
            entity.DeletedBy_Role = userDetails.UserRole;
            entity.IsDeleted = true;

            _db.IncidentSub2Types.Update(entity);
            var value = Save();
            return await value;
        }

        public IncidentSub2Type GetIncidentSub2TypeById(int id, int incidentSub1TypeId)
        {
            return _db.IncidentSub2Types.Where(x => x.Id == id && x.IsDeleted == false && x.IncidentSub1TypeId == incidentSub1TypeId).FirstOrDefault();
        }

        //public IncidentSub2Type GetIncidentSub2TypeById(int id, int incidentSub1TypeId)
        //{
        //    return _db.IncidentSub2Types.Where(x => x.Id == id && x.IsDeleted == false && x.IncidentSub1TypeId == incidentSub1TypeId).FirstOrDefault();
        //}

        public async Task<bool> UpdateIncidentSub2Type(UserProfile userDetails, IncidentSub2Type entity, string incidentSub2Type)
        {
            entity.IncidentSub2Type_Name = incidentSub2Type;
            entity.DateModified = DateTime.Now.Date;
            entity.ModifiedBy_Name = userDetails.FullName;
            entity.ModifiedBy_Email = userDetails.EmailAddress;
            entity.ModifiedBy_Role = userDetails.UserRole;
            entity.ModifiedBy_Affiliate = userDetails.Affiliate;


            _db.IncidentSub2Types.Update(entity);
            var value = Save();
            return await value;
        }

        public IncidentSub2Type GetIncidentSub2TypeById(int id)
        {
            return _db.IncidentSub2Types.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
        }

        public async Task<bool> DeleteIncidentSub2Type(IncidentSub2Type entity)
        {
            _db.IncidentSub2Types.Remove(entity);
            return await Save();
        }

        public bool IncidentSub2TypeExists(string incidentSub2Type, int incidentSub1TypeId)
        {
            bool value = _db.IncidentSub2Types.Any(a => a.IncidentSub2Type_Name.ToLower().Trim() == incidentSub2Type.ToLower().Trim() && a.IsDeleted == false && a.IncidentSub1TypeId == incidentSub1TypeId);
            return value;
        }

        //public async Task<bool> CreateIncidentSub2Type(IncidentSub2Type incidentSub2Type)
        //{
        //    _db.IncidentSub2Types.Add(incidentSub2Type);
        //    return await Save();
        //}

    }
}
