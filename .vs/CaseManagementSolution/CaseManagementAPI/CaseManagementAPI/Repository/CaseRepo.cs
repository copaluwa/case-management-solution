﻿using CaseManagementAPI.Data;
using CaseManagementAPI.Data.Dtos;
using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository
{
    public class CaseRepo : ICaseRepo
    {
        private readonly ApplicationDbContext _db;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IConfiguration _config;
        //private readonly IHostEnvironment _hostEnv;
        //private readonly IWebHostEnvironment _webHostEnv;
        //private readonly ILogger<AssignedOfficerFunction> _logger;

        public CaseRepo(
            ApplicationDbContext db,
            IWebHostEnvironment webHostEnvironment,
            IConfiguration config
        )
        {
            _db = db;
            _webHostEnvironment = webHostEnvironment;
            _config = config;
            // _context = context;
            //_dashboard = dashboard;
        }


        public bool CaseExists(int? whistleblowerNo)
        {
            if (whistleblowerNo == 0 || whistleblowerNo == null)
            {
                return false;
            }
            bool value = _db.Cases.Any(a => a.WhistleblowerNo == whistleblowerNo);
            return value;
        }
       
        
        public bool CaseExistsByCaseIdAndCaseNo(int caseId, string caseNo)
        {
            bool value = _db.Cases.Any((a => a.CaseNo.ToLower().Trim() == caseNo.ToLower().Trim() && a.Id == caseId));
            return value;
        }
        public bool CaseExistsByCaseNo(string caseNo)
        {
            bool value = _db.Cases.Any(a => a.CaseNo.ToLower().Trim() == caseNo.ToLower().Trim());
            return value;
        }

        public async Task<bool> CreateCase(Case newCase)
        {
            _db.Cases.Add(newCase);
            return await Save();
        }

        //public async Task<bool> Save()
        //{
        //    return await _db.SaveChangesAsync() >= 0 ? true : false;
        //}
        public async Task<bool> Save()
        {
            return await _db.SaveChangesAsync() >= 0 ? true : false;
        }

        public ICollection<Case> GetCases()
        {
            return _db.Cases.OrderBy(a => a.CaseNo).ToList();
        }
        //public ICollection<Case> GetAllCases()
        //{
        //    return _db.Cases.OrderBy(a => a.CaseNo).ToList();
        //}
        //For the Role User --- Get by Role
        //public ICollection<Case> GetCasesByRole(Role role)
        //{
        //    return _db.Cases.Where(x => x.AddedBy_Role == role).OrderBy(a => a.CaseNo).ToList();
        //}
        //For the Affiliate user --- Get by Affiliate
       

        public ICollection<Case> GetCasesBySource(string source)
        {
            return _db.Cases.Where(x => x.Source == source).OrderBy(a => a.CaseNo).ToList();
        }
      
        public ICollection<Case> GetCasesByQuarter(string quarter)
        {
            return _db.Cases.Where(x => x.Quarter == quarter).OrderBy(a => a.CaseNo).ToList();
        }

        public ICollection<Case> GetCasesByStatus(CaseStatus status)
        {
            return _db.Cases.Where(x => x.Status == status).OrderBy(a => a.CaseNo).ToList();
        }

        public ICollection<Case> GetCasesByYear(string year)
        {
            return _db.Cases.Where(x => x.Year == year).OrderBy(a => a.CaseNo).ToList();
        }
        public ICollection<Case> GetCasesByMonth(string month)
        {
            return _db.Cases.Where(x => x.Month == month).OrderBy(a => a.CaseNo).ToList();
        }
        public Case GetCaseByCaseNo(string caseNo)
        {
            return _db.Cases.FirstOrDefault(a => a.CaseNo == caseNo);
        }
        public Case GetCaseById(int id)
        {
            //var avaka = _db.Cases.Find(id);
            return _db.Cases.Include(x => x.CaseNotes).Include(x => x.CaseFiles).Where(x => x.Id == id).FirstOrDefault();
        }
        //public Case GetCaseByIdWithoutCaseNote(UserProfile userDetails, int id)
        //{
        //    //var avaka = _db.Cases.Find(id);
        //    switch (userDetails.UserRole)
        //    {
        //        case Role.Group:
        //            return _db.Cases.Where(x => x.Id == id && x.AssignedTo_Email != userDetails.EmailAddress).FirstOrDefault();

        //        case Role.Regional:
        //            return _db.Cases.Where(x => x.Id == id && x.Region == userDetails.Region && x.AssignedTo_Email != userDetails.EmailAddress).FirstOrDefault();

        //        case Role.Affiliate:
        //            return _db.Cases.Where(x => x.Id == id && x.AssignedTo_Email != userDetails.EmailAddress).FirstOrDefault();

        //        case Role.UserManager:
        //            return new Case();
        //        default:
        //            return new Case();
        //    }
        //    //return _db.Cases.Where(x => x.Id == id).FirstOrDefault();
        //}
        public Case GetCaseByIdWithoutCaseNote(UserProfile userDetails, int id)
        {
            //var avaka = _db.Cases.Find(id);
            switch (userDetails.UserRole)
            {
                case Role.Group:
                    return _db.Cases.Where(x => x.Id == id).FirstOrDefault();

                case Role.Regional:
                    return _db.Cases.Where(x => x.Id == id && x.Region == userDetails.Region
                    && x.Region == userDetails.Region).FirstOrDefault();
                // || x.AssignedTo_Email.ToLower() == userDetails.EmailAddress.ToLower()
                //|| x.AssignedUsers.ToLower().Contains(userDetails.EmailAddress.ToLower())).FirstOrDefault();

                case Role.Affiliate:
                    return _db.Cases.Where(x => x.Id == id
                   && x.AssignedTo_Email == userDetails.EmailAddress).FirstOrDefault();
                //|| x.AssignedUsers.ToLower().Contains(userDetails.EmailAddress.ToLower())).FirstOrDefault();


                default:
                    return new Case();
            }
            //return _db.Cases.Where(x => x.Id == id).FirstOrDefault();
        }
        public ICollection<Case> GetCasesByAffiliate(string affiliate)
        {
            return _db.Cases.Where(x => x.Source == affiliate).OrderBy(a => a.CaseNo).ToList();
        }
        public Case GetCaseByCaseNo(string caseNo, UserProfile userDetails)
        {
            switch (userDetails.UserRole)
            {
                case Role.Group:
                    return _db.Cases.Include(x => x.CaseNotes).Include(x => x.CaseFiles).Where(x => x.CaseNo == caseNo).AsNoTracking().FirstOrDefault();

                case Role.Regional:
                    return _db.Cases.Include(x => x.CaseNotes).Include(x => x.CaseFiles).Where(x => x.CaseNo == caseNo
                    && x.Region == userDetails.Region
                    || x.AssignedUsers.ToLower().Contains(userDetails.EmailAddress.ToLower())).AsNoTracking().FirstOrDefault();

                case Role.Affiliate:
                    return _db.Cases.Include(x => x.CaseNotes).Include(x => x.CaseFiles).Where(x => x.CaseNo == caseNo 
                    && x.AssignedTo_Email == userDetails.EmailAddress 
                    || x.AssignedUsers.ToLower().Contains(userDetails.EmailAddress.ToLower())).AsNoTracking().FirstOrDefault();

                case Role.UserManager:
                    return new Case();
                default:
                    return new Case();
            }
        }

        public ICollection<Case> GetCasesByRegion(UserProfile userDetails, string region)
        {
            region = region.ToLower();
            switch (userDetails.UserRole)
            {
                case Role.Group:
                    return _db.Cases.Where(x => x.Region.ToLower() == region).OrderBy(a => a.CaseNo).ToList();

                case Role.Regional:
                    return _db.Cases.Where(x => x.Region.ToLower() == userDetails.Region.ToLower() && (x.Region.ToLower() == region)).OrderBy(a => a.CaseNo).ToList();

                case Role.Affiliate:
                    return _db.Cases.Where(x => x.Region.ToLower() == userDetails.Region.ToLower() && (x.Region.ToLower() == region)).OrderBy(a => a.CaseNo).ToList();

                case Role.UserManager:
                    return new List<Case>();
                default:
                    return new List<Case>();
            }
        }

        public ICollection<Case> GetAllCases(UserProfile userDetails)
        {

            switch (userDetails.UserRole)
            {
                case Role.Group:
                    return _db.Cases.Include("CaseNotes").OrderByDescending(a => a.Id).ToList();

                case Role.Regional:
                    return _db.Cases.Include("CaseNotes").Where(x => x.Region.ToLower() == userDetails.Region.ToLower() || x.AssignedUsers.ToLower().Contains(userDetails.EmailAddress.ToLower())).OrderByDescending(a => a.Id).ToList();

                case Role.Affiliate:
                    return _db.Cases.Include("CaseNotes").Where(x => x.AssignedTo_Email.ToLower() == userDetails.EmailAddress.ToLower() || x.AssignedUsers.ToLower().Contains(userDetails.EmailAddress.ToLower())).OrderByDescending(a => a.Id).ToList();

                case Role.UserManager:
                    return new List<Case>();
                default:
                    return new List<Case>();
            }
        }

        public IQueryable<Case> GetQueryableCases(UserProfile userDetails)
        {
            switch (userDetails.UserRole)
            {
                case Role.Group:
                    return _db.Cases.Include("CaseNotes").AsQueryable();

                case Role.Regional:
                    return _db.Cases.Include("CaseNotes").Where(x => x.Region.ToLower() == userDetails.Region.ToLower() || x.AssignedUsers.ToLower().Contains(userDetails.EmailAddress.ToLower())).OrderBy(a => a.CaseNo).AsQueryable();

                case Role.Affiliate:
                    return _db.Cases.Include("CaseNotes").Where(x => x.AssignedTo_Email.ToLower() == userDetails.EmailAddress.ToLower() || x.AssignedUsers.ToLower().Contains(userDetails.EmailAddress.ToLower())).OrderBy(a => a.CaseNo).AsQueryable();
                default:
                    return null;
            }
        }

        public ICollection<Case> SearchAllCases(UserProfile userDetails, SearchCaseDTO searchCaseDTO)
        {
            var cases = GetQueryableCases(userDetails);

            if (!string.IsNullOrWhiteSpace(searchCaseDTO.Region))
            {
                cases = cases.Where(x => x.Region.Trim().ToLower() == searchCaseDTO.Region.Trim().ToLower()).AsQueryable();
            }
            if (!string.IsNullOrWhiteSpace(searchCaseDTO.Source))
            {
                cases = cases.Where(x => x.Source.Trim().ToLower() == searchCaseDTO.Source.Trim().ToLower()).AsQueryable();
            }
            if (!string.IsNullOrWhiteSpace(searchCaseDTO.Affiliate))
            {
                cases = cases.Where(x => x.Affiliate.Trim().ToLower() == searchCaseDTO.Affiliate.Trim().ToLower()).AsQueryable();
            }
            if (!string.IsNullOrWhiteSpace(searchCaseDTO.Quarter))
            {
                cases = cases.Where(x => x.Quarter.Trim().ToLower() == searchCaseDTO.Quarter.Trim().ToLower()).AsQueryable();
            }
            if (searchCaseDTO.Status != 0)
            {
                cases = cases.Where(x => x.Status == searchCaseDTO.Status).AsQueryable();
            }
            if (cases.Count() == 0)
            {
                return new List<Case>();
            }
            return cases.OrderBy(a => a.Id).ToList();
        }

        public string GetCaseNumber()
        {
            var caseNumber = "EVT001";
            var lastCaseDetails = _db.Cases.OrderByDescending(c => c.Id).FirstOrDefault();

            if (lastCaseDetails == null)
            {
                return caseNumber;
            }
            else
            {
                var numberValue = lastCaseDetails.CaseNo.Split("T")[1];
                numberValue.Replace("T", "");
                int i = Convert.ToInt32(numberValue) + 2;
                string s = i.ToString().PadLeft(3, '0');
                caseNumber = "EVT" + s;
                return caseNumber;
            }

        }

        public string GetCaseNumber(string region)
        {
            //generating the case number based on the last added 
            if (string.IsNullOrWhiteSpace(region))
            {
                return "";
            }
            region = region.Trim();
            var casePrefix = region.Length > 3 ? (region.Substring(0,3)).ToUpper() : region.ToUpper();
            var lastCaseNumber = _db.Cases.AsNoTracking().Where(x => x.Region.ToLower().Equals(region.ToLower())).Count();
            var newNumber = lastCaseNumber + 1;
            var caseNumber = casePrefix + "_" + newNumber.ToString();
            return caseNumber;
        }
        //public bool AddNote(CaseNote newNote)
        //{
        //    _db.CaseNotes.Add(newNote);
        //    return Save();
        //}
        public async Task<bool> CreateNote(CaseNote newNote, UserProfile userDetails)
        {
            newNote.DateAdded = DateTime.Now.Date;
            newNote.AddedBy_Affiliate = userDetails.Affiliate;
            newNote.AddedBy_Name = userDetails.FullName;
            newNote.AddedBy_Email = userDetails.EmailAddress;
            newNote.AddedBy_Region = userDetails.Region;
            newNote.AddedBy_Role = userDetails.UserRole;

            _db.CaseNotes.Add(newNote);
            var value = Save();
            return await value;
        }
        public async Task<bool> CreateNote(Case caseDetails, UserProfile userDetails, string note)
        {

            var caseNote = new CaseNote()
            {
                CaseNo = caseDetails.CaseNo,
                CaseId = caseDetails.Id,
                Note = note,
                DateAdded = DateTime.Now.Date,
                AddedBy_Affiliate = userDetails.Affiliate,
                AddedBy_Name = userDetails.FullName,
                AddedBy_Email = userDetails.EmailAddress,
                AddedBy_Region = userDetails.Region,
                AddedBy_Role = userDetails.UserRole
            };
            _db.CaseNotes.Add(caseNote);
            var value = Save();
            return await value;
        }
        public async Task<bool> UpdateNote(UserProfile userDetails, CaseNote entity, string newNote)
        {
            entity.Note = newNote;
            entity.DateModified = DateTime.Now.Date;
            entity.ModifiedBy_Name = userDetails.FullName;
            entity.ModifiedBy_Email = userDetails.EmailAddress;
            entity.ModifiedBy_Role = userDetails.UserRole;

            _db.CaseNotes.Update(entity);
            var value = Save();
            return await value;
        }
       
        public async Task<bool> ReassignCase(UserProfile assignerDetails, Case entity, UserProfile reassignedToUserDetails)
        {
            var previouslyAssignedTo = entity.AssignedTo_Email == null ? "No Name" : entity.AssignedTo_Email;
            entity.PreviouslyAssignedTo_Email = previouslyAssignedTo;
            entity.AssignedTo_Affiliate = reassignedToUserDetails.Affiliate;
            entity.AssignedTo_Email = reassignedToUserDetails.EmailAddress;
            entity.AssignedTo_Name = reassignedToUserDetails.FullName;
            entity.AssignedTo_Region = reassignedToUserDetails.Region;
            entity.AssignedTo_Role = reassignedToUserDetails.UserRole;
            entity.IsReassigned = true;
            entity.ReassignedBy_Email = assignerDetails.EmailAddress;
            entity.ReassignedBy_Name = assignerDetails.FullName;
            entity.ReassignedBy_Affiliate = assignerDetails.Affiliate;
            entity.ReassignedBy_Region = assignerDetails.Region;
            entity.DateReassigned = DateTime.Now.Date;
            entity.ReassignedBy_Role = assignerDetails.UserRole;

            _db.Cases.Update(entity);
            var value = Save();
            return await value;
        }
       
        public async Task<bool> DeleteUser(UserProfile user)
        {
            _db.UserProfile.Remove(user);
            return await Save();
        }
        public async Task<bool> UpdateCase(Case caseDetail)
        {
            _db.Cases.Update(caseDetail);
            return await Save();
        }
        public CaseNote GetCaseNoteById(int id)
        {
            //var avaka = _db.Cases.Find(id);
            return _db.CaseNotes.Where(x => x.Id == id).FirstOrDefault();
        }
        public long GetCaseCountBySource(string source)
        {
            var count = _db.Cases.Count(p => p.Source.ToLower() == source.ToLower());
            return count;
        }
        public long GetCaseCountByAffiliate(string affiliate)
        {
            var count = _db.Cases.Count(p => p.Affiliate.ToLower() == affiliate.ToLower());
            return count;
        }
        public long GetCaseCountByIncidentSubCategory(string incidentSubcategory)
        {
            var count = _db.Cases.Count(p => p.IncidentSubCategory.ToLower() == incidentSubcategory.ToLower());
            return count;
        }
        public long GetCaseCountByRegion(string region)
        {
            var count = _db.Cases.Count(p => p.Region.ToLower() == region.ToLower());
            return count;
        }
        public long GetCaseCountByIncidentCategory(string incidentCategory)
        {
            var count = _db.Cases.Count(p => p.IncidentCategory.ToLower() == incidentCategory.ToLower());
            return count;
        }

        public long GetCaseCountByIncidentSub1Type(string incidentSub1Type)
        {
            var count = _db.Cases.Count(p => p.IncidentSub1Type.ToLower() == incidentSub1Type.ToLower());
            return count;
        }

        public long GetCaseCountByIncidentSub2Type(string incidentSub2Type)
        {
            var count = _db.Cases.Count(p => p.IncidentSub2Type.ToLower() == incidentSub2Type.ToLower());
            return count;
        }

        public bool ValidateCaseFiles(IList<IFormFile> files)
        {
            var supportedTypes = _config.GetSection("AppSettings:FileTypes").Get<string[]>();
            var unsupportedFiles = files.Where(file => !supportedTypes.Any(type => type.Equals(Path.GetExtension(file.FileName.ToLower())[1..])));
            return !unsupportedFiles.Any(); //unsupportedFiles.Count() == 0; // empty passes, not empty fails
        }

        private async Task<IEnumerable<CaseFilePropsDTO>> UploadCaseFiles(IList<IFormFile> files, string caseNumber)
        {
            var uploadedFileNames = new List<CaseFilePropsDTO>();

            for (int i = 0; i < files.Count; i++)
            {
                var file = files[i];
                string fileExtension = Path.GetExtension(file.FileName);
                string fileName = $"{Guid.NewGuid().ToString().Replace("-", "")}{fileExtension}";

                string path = Path.Combine(_webHostEnvironment.WebRootPath, "CaseFiles", caseNumber);
                string filePath = Path.Combine(path, fileName);

                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    await file.CopyToAsync(stream);
                    await stream.FlushAsync();
                }

                uploadedFileNames.Add(new CaseFilePropsDTO
                {
                    FileName = file.FileName,
                    FilePath = $"CaseFiles/{caseNumber}/{fileName}"
                });
            }
            return uploadedFileNames;
        }

        public async Task<IEnumerable<CaseFilePropsDTO>> CreateCaseFile(IList<IFormFile> files, string caseNumber, int caseId)
        {
            var caseFileProps = await UploadCaseFiles(files, caseNumber);

            if (!caseFileProps.Any()) return new List<CaseFilePropsDTO>();

            var caseFiles = new List<CaseFile>();
            foreach (var file in caseFileProps)
            {
                caseFiles.Add(new CaseFile
                {
                    CaseId = caseId,
                    DateAdded = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileName = file.FileName,
                    FilePath = file.FilePath
                });
            }

            await _db.CaseFiles.AddRangeAsync(caseFiles);
            await _db.SaveChangesAsync();

            return caseFiles.Select(x => new CaseFilePropsDTO
            {
                FileId = x.Id,
                FileName = x.FileName,
                FilePath = $"{_config.GetSection("AppSettings:BaseUrl").Value.ToString()}/{x.FilePath}"
            });
        }
    }
}
