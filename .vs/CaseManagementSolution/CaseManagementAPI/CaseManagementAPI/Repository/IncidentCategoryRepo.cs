﻿using CaseManagementAPI.Data;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository
{
    public class IncidentCategoryRepo : IIncidentCategoryRepo
    {
        private readonly ApplicationDbContext _db;

        public IncidentCategoryRepo(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<bool> UpdateSubIncidentCategory(UserProfile userDetails, IncidentSubCategory entity, string incidentSubCategory)
        {
            entity.IncidentSubcategory_Name = incidentSubCategory;
            entity.DateModified = DateTime.Now.Date;
            entity.ModifiedBy_Name = userDetails.FullName;
            entity.ModifiedBy_Email = userDetails.EmailAddress;
            entity.ModifiedBy_Role = userDetails.UserRole;
            entity.ModifiedBy_Affiliate = userDetails.Affiliate;


            _db.IncidentSubCategories.Update(entity);
            var value = Save();
            return await value;
        }
        public async Task<bool> DeleteIncidentCategory(UserProfile userDetails, IncidentCategory entity)
        {
            entity.DateDeleted = DateTime.Now.Date;
            entity.DeletedBy_Name = userDetails.FullName;
            entity.DeletedBy_Email = userDetails.EmailAddress;
            entity.DeletedBy_Role = userDetails.UserRole;
            entity.IsDeleted = true;

            _db.IncidentCategories.Update(entity);
            var value = await Save();
            return value;
        }
        public async Task<bool> DeleteIncidentSubCategory(IncidentSubCategory entity)
        {
            _db.IncidentSubCategories.Remove(entity);
            return await Save();
        }
        public async Task<bool> DeleteIncidentSubCategory(UserProfile userDetails, IncidentSubCategory entity)
        {
            entity.DateDeleted = DateTime.Now.Date;
            entity.DeletedBy_Name = userDetails.FullName;
            entity.DeletedBy_Email = userDetails.EmailAddress;
            entity.DeletedBy_Role = userDetails.UserRole;
            entity.IsDeleted = true;

            _db.IncidentSubCategories.Update(entity);
            var value = Save();
            return await value;
        }
        public bool IncidentCategoryExists(string incidentCategory)
        {
            bool value = _db.IncidentCategories.Any(a => a.IncidentCategoryName.ToLower().Trim() == incidentCategory.ToLower().Trim() && a.IsDeleted == false);
            return value;
        }
        public bool IncidentSubCategoryExists(string subcategoryCategory, int incidentCategoryID)
        {
            bool value = _db.IncidentSubCategories.Any(a => a.IncidentSubcategory_Name.ToLower().Trim() == subcategoryCategory.ToLower().Trim() && a.IsDeleted == false && a.IncidentCategoryId == incidentCategoryID);
            return value;
        }
        public async Task<bool> CreateSubcategory(IncidentSubCategory incidentSubCategory)
        {
            _db.IncidentSubCategories.Add(incidentSubCategory);
            return await Save();
        }
        public ICollection<IncidentCategory> GetIncidentCategories()
        {
            return _db.IncidentCategories.Include("IncidentSubCategories").Where(a => a.IsDeleted == false).ToList();
        }
        public ICollection<IncidentSubCategory> GetIncidentSubCategoriesByIncidentCategoryId(int incidentCategoryId)
        {
            return _db.IncidentSubCategories.Where(a => a.IncidentCategoryId == incidentCategoryId && a.IsDeleted == false).ToList();
        }
        public async Task<bool> CreateIncidentCategory(IncidentCategory incidentCategory)
        {
            _db.IncidentCategories.Add(incidentCategory);
            return await Save();
        }
        public async Task<bool> UpdateIncidentCategory(UserProfile userDetails, IncidentCategory entity, string incidentCatgeory)
        {
            entity.IncidentCategoryName = incidentCatgeory;
            entity.DateModified = DateTime.Now.Date;
            entity.ModifiedBy_Name = userDetails.FullName;
            entity.ModifiedBy_Email = userDetails.EmailAddress;
            entity.ModifiedBy_Role = userDetails.UserRole;
            entity.ModifiedBy_Affiliate = userDetails.Affiliate;
            _db.IncidentCategories.Update(entity);
            var value = Save();
            return await value;
        }
        public IncidentCategory GetIncidentCategoryById(int id)
        {
            return _db.IncidentCategories.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
        }

        public IncidentSubCategory GetIncidentSubCategoryById(int id, int incidentCategoryId)
        {
            return _db.IncidentSubCategories.Where(x => x.Id == id && x.IsDeleted == false && x.IncidentCategoryId == incidentCategoryId).FirstOrDefault();
        }
        public IncidentSubCategory GetIncidentSubCategoryById(int id)
        {
            return _db.IncidentSubCategories.Where(x => x.Id == id && x.IsDeleted == false).FirstOrDefault();
        }

        public async Task<bool> Save()
        {
            return await _db.SaveChangesAsync() >= 0 ? true : false;
        }
    }
}
