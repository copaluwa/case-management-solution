﻿using CaseManagementAPI.Data;
using CaseManagementAPI.Models;
using CaseManagementAPI.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Repository
{
    public class AffiliateRepo : IAffiliateRepo 
    {
        private readonly ApplicationDbContext _db;
      
        public AffiliateRepo(ApplicationDbContext db)
        {
            _db = db;
        }


        public async Task<bool> DeleteAffiliate(UserProfile userDetails, Affiliate entity)
        {
            entity.DateDeleted = DateTime.Now.Date;
            entity.DeletedBy_Name = userDetails.FullName;
            entity.DeletedBy_Email = userDetails.EmailAddress;
            entity.DeletedBy_Role = userDetails.UserRole;
            entity.IsDeleted = true;

            _db.Affiliates.Update(entity);
            var value = Save();
            return await value;
        }
        public async Task<bool> DeleteAffiliate(Affiliate entity)
        {
            _db.Affiliates.Remove(entity);
            return await Save();

        }
        public bool AffiliatesExists(string affiliate)
        {
            bool value = _db.Affiliates.Any(a => a.Affiliate_Name.ToLower().Trim() == affiliate.ToLower().Trim() && a.IsDeleted == false);
            return value;
        }
        public async Task<bool> CreateAffiliate(Affiliate affiliate)
        {
            _db.Affiliates.Add(affiliate);
            return await Save();
        }
        public async Task<bool> Save()
        {
            return await _db.SaveChangesAsync() >= 0 ? true : false;
        }
        public ICollection<Affiliate> GetAffiliatesByRegionId(int regionId)
        {
            return _db.Affiliates.Where(a => a.RegionId == regionId && a.IsDeleted == false).ToList();
        }
        public Affiliate GetAffiliatesById(int id)
        {
            return _db.Affiliates.Where(a => a.Id == id && a.IsDeleted == false).FirstOrDefault();
        }
        public async Task<bool> UpdateAffiliate(UserProfile userDetails, Affiliate entity, string affiliate)
        {
            entity.Affiliate_Name = affiliate;
            entity.DateModified = DateTime.Now.Date;
            entity.ModifiedBy_Name = userDetails.FullName;
            entity.ModifiedBy_Email = userDetails.EmailAddress;
            entity.ModifiedBy_Role = userDetails.UserRole;
            entity.ModifiedBy_Affiliate = userDetails.Affiliate;


            _db.Affiliates.Update(entity);
            var value = Save();
            return await value;
        }

    }
}
