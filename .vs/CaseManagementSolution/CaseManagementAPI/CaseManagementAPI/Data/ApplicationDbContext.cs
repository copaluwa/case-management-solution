﻿using CaseManagementAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Case> Cases { get; set; }
        public DbSet<CaseNote> CaseNotes { get; set; }
        public DbSet<CaseFile> CaseFiles { get; set; }
        public DbSet<UserProfile> UserProfile { get; set; }
        public DbSet<CaseSource> CaseSources { get; set; }
        public DbSet<IncidentCategory> IncidentCategories { get; set; }
        public DbSet<IncidentSubCategory> IncidentSubCategories { get; set; }
        public DbSet<IncidentSub1Type> IncidentSub1Types { get; set; }
        public DbSet<IncidentSub2Type> IncidentSub2Types { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Affiliate> Affiliates { get; set; }
        //public DbSet<ActivityLog> ActivityLogs { get; set; }
        public DbSet<AuditLog> AuditLogs { get; set; }
        //public DbSet<AuditLogChange> AuditLogChanges { get; set; }
    }
}
