﻿using CaseManagementAPI.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Data.Dtos
{
    public class SearchCaseDto
    {
        public string CaseNo { get; set; }

        public int WhistleblowerNo { get; set; }

        [Required]
        public string Region { get; set; }

        [Required]
        public string Affiliate { get; set; }
     
        [Required]
        public string Quarter { get; set; }

        [Required]
        public string Month { get; set; }

        [Required]
        public string Year { get; set; }

        [Required]
        public DateTime DateOfIncident { get; set; }

        [Required]
        public DateTime DateReported { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,5)")]
        public decimal GrossOnSlaught { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountRefuted { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountRecovered { get; set; }

        public decimal AmountPrevented { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal NetLoss { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal UsdExchangeRate { get; set; }

        public string Source { get; set; }
        
        [Required]
        [MaxLength(10)]
        public string Currency { get; set; }

        //[Column(TypeName = "decimal(18,5)")]
        //public decimal UsdEquivalent { get; set; }

        [Required]
        public string IncidentCategory { get; set; }

        [Required]
        public string IncidentSubCategory { get; set; }

        public string IncidentSub1Type { get; set; }

        public string IncidentSub2Type { get; set; }

        [Required]
        public string StaffInvolvement { get; set; }

        [Required]
        public string CaseStatus { get; set; }

        [Required]
        public string DescriptionOfIncident { get; set; }

        public DateTime DateFinalised { get; set; }

        public string RefutedOrSubstantiated { get; set; }

        public string CorrectiveAction { get; set; }

        public string CaseOutcome { get; set; }

        public string AssignedTo_Name { get; set; }
        
        public string AssignedTo_Affiliate { get; set; }

        public string AssignedTo_Region { get; set; }

        public string AssignedTo_Email { get; set; }

        public string AssignedUsers { get; set; }

        public string AssignedUsers_Name { get; set; }

        public string AssignedUsers_DateAdded { get; set; }

        public string AssignedUsers_AddedByName { get; set; }

        public string AssignedUsers_AddedByEmail { get; set; }

        public string ClosedBy_Name { get; set; }

        public string ClosedBy_Affiliate { get; set; }

        public string ClosedBy_Region { get; set; }

        public string ClosedBy_Email { get; set; }
        
        public string ReassignedBy_Name { get; set; }

        public string ReassignedBy_Affiliate { get; set; }

        public string ReassignedBy_Region { get; set; }

        public string ReassignedBy_Email { get; set; }

        public DateTime DateReassigned { get; set; }

        public string PreviouslyAssignedTo_Email { get; set; }
    }
}
