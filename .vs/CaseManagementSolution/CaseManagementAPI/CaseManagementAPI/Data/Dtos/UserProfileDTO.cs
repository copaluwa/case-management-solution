﻿using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Data.Dtos
{
    public class UserProfileDTO
    {
        public int Id { get; set; }
        [Required]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Email is not valid")]
        public string EmailAddress { get; set; }
        [Required]
        public Role UserRole { get; set; }
        [Required]
        public string Affiliate { get; set; }
        [Required]
        public string Region { get; set; }

    }
    public class GetAll_UserProfileDTO
    {
        public int Id { get; set; }
        [Required]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Email is not valid")]
        public string EmailAddress { get; set; }
        [Required]
        public Role UserRole { get; set; }
        [Required]
        public string Affiliate { get; set; }
        public string Region { get; set; }
        public string AddedBy_Email { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedBy_Email { get; set; }
        public string FullName { get; set; }
    }
    public class UserRoleDTO
    {
        public string UserRole { get; set; }
    }
    public class AssigneeDto
    {
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
    }
    public class GetAssigneesDTO
    {
        public string Username { get; set; }
    }

    public class AuthDTO
    {
        public int StatusCode { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}


