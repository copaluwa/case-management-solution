﻿using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Data.Dtos
{
    public class CaseDTO
    {
        public int Id { get; set; }
        public string CaseNo { get; set; }
        public int? WhistleblowerNo { get; set; }

        [Required]
        public string Region { get; set; }
        [Required]
        public string Affiliate { get; set; }

        //public string AffiliateCode { get; set; }
        [Required]
        public string Quarter { get; set; }
        [Required]
        public string Month { get; set; }
        [Required]
        public string Year { get; set; }
        [Required]
        public DateTime DateOfIncident { get; set; }
        [Required]
        public DateTime DateReported { get; set; }

        //public DateTime DateFinalised { get; set; }
       // public string DRF { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,5)")]
        public decimal GrossOnSlaught { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountRefuted { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountRecovered { get; set; }

        public decimal AmountPrevented { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal NetLoss { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal UsdExchangeRate { get; set; }
        public string Source { get; set; }
        //public string CaseOutcome { get; set; }

        [Required]
        [MaxLength(10)]
        public string Currency { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal UsdEquivalent { get; set; }

        [Required]
        public string IncidentCategory { get; set; }
        [Required]
        public string IncidentSubCategory { get; set; }
        public string IncidentSub1Type { get; set; }
        public string IncidentSub2Type { get; set; }
        [Required]
        public string StaffInvolvement { get; set; }
        [Required]
        public CaseStatus Status { get; set; }
        public string StatusText { get; set; }
        [Required]
        public string DescriptionOfIncident { get; set; }
        //[Required]
        public string Note { get; set; }
        public string AssignedTo_Name { get; set; }
        //public string CorrectiveAction { get; set; }
        //public string CaseOutcome { get; set; }
        //public string DRF { get; set; }
        //public DateTime DateFinalised { get; set; }
        public ICollection<CaseNote> CaseNotes { get; set; }
        //[NotMapped]
        public ICollection<CaseFile> CaseFiles { get; set; }

        //adjustments made


        public string AssignedTo_Affiliate { get; set; }
       
        public string AssignedTo_Region { get; set; }
       
        public string AssignedTo_Email { get; set; }
        public Role AssignedTo_Role { get; set; }
        public string AssignedUsers { get; set; }
        public string CorrectiveAction { get; set; }
        public string CaseOutcome { get; set; }
        public string DRF { get; set; }
        public DateTime DateFinalised { get; set; }
        public string RefutedOrSubstantiated { get; set; }
        public string AssignedUsers_Name { get; set; }
        public string AssignedUsers_DateAdded { get; set; }
        public string AssignedUsers_AddedByName { get; set; }
        public string AssignedUsers_AddedByEmail { get; set; }

    }
    
    public class CreateCaseDTO
    {
        public int? WhistleblowerNo { get; set; }

        [Required]
        public string Region { get; set; }

        [Required]
        public string Affiliate { get; set; }
        
        [Required]
        public string Quarter { get; set; }

        [Required]
        public string Month { get; set; }

        [Required]
        public string Year { get; set; }

        [Required]
        public DateTime DateOfIncident { get; set; }

        [Required]
        public DateTime DateReported { get; set; }

        //public DateTime DateFinalised { get; set; }
        // public string DRF { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,5)")]
        public decimal GrossOnSlaught { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountRefuted { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountRecovered { get; set; }

        public decimal AmountPrevented { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,5)")]
        public decimal NetLoss { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal UsdExchangeRate { get; set; }

        [Required]
        public string Source { get; set; }

        [Required]
        [MaxLength(10)]
        public string Currency { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal UsdEquivalent { get; set; }

        [Required]
        public string IncidentCategory { get; set; }

        [Required]
        public string IncidentSubCategory { get; set; }

        public string IncidentSub1Type { get; set; }

        public string IncidentSub2Type { get; set; }

        [Required]
        public string StaffInvolvement { get; set; }
      
        [Required]
        public string DescriptionOfIncident { get; set; }
        //[Required]
        public string Note { get; set; }
        public IList<IFormFile>? Files { get; set; }
    }

    public class CaseResultDTO
    {
        public int Code { get; set; }
        public object Value { get; set; }
    }

    public class CaseNoteDTO
    {
        [Required]
        public int CaseId { get; set; }
        [Required]
        public string CaseNo { get; set; }
        //public Case Case { get; set; }
        [Required]
        public string Note { get; set; }
       
    }
    public class UpdateCaseNoteDTO
    {
        [Required]
        public string Note { get; set; }
    }

    public class UpdateCaseDTO
    {
      
        [Column(TypeName = "decimal(18,5)")]
        public decimal GrossOnSlaught { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountRefuted { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountRecovered { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal AmountPrevented { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal NetLoss { get; set; }
        //public decimal UsdEquivalent { get; set; }
        public CaseStatus Status { get; set; }
        public string DescriptionOfIncident { get; set; }
        public string CorrectiveAction { get; set; }
        public string CaseOutcome { get; set; }
        public string DRF { get; set; }
        public DateTime DateFinalised { get; set; }
       
        public string IncidentCategory { get; set; }
  
        public string IncidentSubCategory { get; set; }
        public string IncidentSub1Type { get; set; }
        public string IncidentSub2Type { get; set; }
        public string RefutedOrSubstantiated { get; set; }
    }



    public class CloseCaseDTO
    {
        [Required]
        public CaseStatus Status { get; set; }
       
        [Required]
        public string CorrectiveAction { get; set; }
        [Required]
        public string CaseOutcome { get; set; }
        [Required]
        public DateTime DateFinalised { get; set; }
        public string RefutedOrSubstantiated { get; set; }
        public IList<IFormFile>? Files { get; set; }
    }

    public class CaseSourceDTO
    {
        public int Id { get; set; }

        [Required]
        public string Source { get; set; }
    }
    public class CreateIncidentCategoryDTO
    {
        public int Id { get; set; }
        [Required]
        public string IncidentCategory { get; set; }
        public string IncidentSubCategories { get; set; }
       
    }
    public class CreateIncidentSubTypeDTO
    {
        [Required]
        public int IncidentSubCategoryId { get; set; }
        [Required]
        public string IncidentSub1Type { get; set; }

        public string IncidentSub2Type { get; set; }
    }

    public class GetIncidentSubType1DTO
    {
        public int Id { get; set; }
        public string IncidentSubType1Name { get; set; }
        public List<IncidentSub2Type> IncidentSu2Types { get; set; }
    }

    public class UpdateIncidentCategoryDTO
    {
        //public int Id { get; set; }
        [Required]
        public string IncidentCategory { get; set; }
    }
    public class UpdateIncidentSub1TypeDTO
    {
        //public int Id { get; set; }
        [Required]
        public string IncidentSub1Type { get; set; }
    }
    public class UpdateIncidentSubCategoryDTO
    {
        [Required]
        public int IncidentCategoryId { get; set; }
        [Required]
        public string IncidentSubCategory { get; set; }
    }
    public class UpdateIncidentSub2TypeDTO
    {
        //[Required]
        //public int IncidentSub1TypeId { get; set; }
        [Required]
        public string IncidentSub2Type { get; set; }
    }

    public class DeleteIncidentSubCategoryDTO
    {
        [Required]
        public int IncidentCategoryId { get; set; }
      
    }

    public class GetIncidentCategoriesDTO
    {
        public int Id { get; set; }
        public string IncidentCategoryName { get; set; }
        public List<IncidentSubCategory> IncidentSubCategories { get; set; }
    }
    public class GetRegionsDTO
    {
        public int Id { get; set; }
        public string Region_Name { get; set; }
        public List<Affiliate> Affiliates { get; set; }
    }
    public class CreateIncidentSubCategoryDTO
    {
        //public int Id { get; set; }
        [Required]
        public string IncidentSubCategory { get; set; }
        [Required]
        public int IncidentCategoryId { get; set; }
    }

    public class CreateIncidentSub2TypeDTO
    {
        //public int Id { get; set; }
        [Required]
        public string IncidentSub2Type { get; set; }
        [Required]
        public int IncidentSub1TypeId { get; set; }
    }

    public class CreateAffiliateDTO
    {
        public int Id { get; set; }
        [Required]
        public string Affiliate { get; set; }
        public int RegionId { get; set; }
    }
    public class CreateRegionDTO
    {
        public int Id { get; set; }
        [Required]
        public string Region { get; set; }
        public string Affiliates { get; set; }
    }
    public class UpdateRegionDTO
    {
        //public int Id { get; set; }
        [Required]
        public string Region { get; set; }
    }
    public class UpdateAndDeleteAffiliateDTO
    { 
        [Required]
        public string Affiliate { get; set; }
    }
    public class AssignmentDTO
    {
        public string AssignToEmail { get; set; }
    }

    public class SearchCaseDTO
    {
        public string Region { get; set; }
        public string Affiliate { get; set; }
        public string Source { get; set; }
        public string Quarter { get; set; }
        public CaseStatus Status { get; set; }
    }

    public class CaseFilePropsDTO
    {
        public int FileId { get;set;}
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
