﻿using CaseManagementAPI.Enums;
using CaseManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaseManagementAPI.Data.Dtos
{
    public class ReportDto
    {
        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }
    }

    public class StatusReportDto : ReportDto
    {
        [Required]
        public CaseStatus CaseStatus { get; set; }
    }

    public class ReportRequest
    {
        public CaseStatus? Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class ReportRequestDTO : ReportRequest
    {
        public Role Role { get; set; }
        public string Affiliate { get; set; }
        public string Region { get; set; }
    }

    public class AffiliateReportResponse
    {
        public string Affiliate { get; set; }
        public int Count { get; set; }
    }

    public class SourceReportDto : ReportDto
    {
        [Required]
        public CaseSource CaseSource { get; set; }
    }

    public class GrossOnSlaughtReportDto : StatusReportDto
    {
        [Required]
        public string Region { get; set; }
        public string Affiliate { get; set; }
    }

    public class IncidentCategoryReportDto : ReportDto
    {
        [Required]
        public IncidentCategory IncidentCategory { get; set; }

        [Required]
        public IncidentCategory IncidentSubCategory { get; set; }
    }
}
